#include <iostream>
#include <string>

using namespace std;

class WithCC { // with copy constructor
    public:
        WithCC() {}
        WithCC(const WithCC&) {
            cout << "WithCC(WithCC&)" << endl;
        }
};

class WoCC { // without copy constructor
    private:
        string id;
    public:
        WoCC(const string& name = "") : id(name) {}
        void print(const string& msg = "") const {
            if(msg.size() != 0) cout << msg << ": ";
            cout << id << endl;
        }
};

class Composite {
    private:
        WithCC withcc;
        WoCC wocc;
    public:
        Composite() : wocc("Composite()") {}
        void print(const string& msg = "") const {
            wocc.print(msg);
        }
};

int main(){
    Composite c;
    c.print("Contents of c");
    cout << "Calling compostie copy-constructor" << endl;
    Composite c2 = c;
    c2.print("Contents of c2");
    return 0;
}
