void f(int&) {}
void g(const int&) {}

int main(){
//    f(1); // invalid initialization of non-const reference of type ‘int&’ from an rvalue of type ‘int’
    g(1);
    const int y = 20;
//    f(y); // invalid initialization of non-const reference of type ‘int&’ from an rvalue of type ‘int’
    g(y);
    return 0;
}
