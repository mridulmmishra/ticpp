class NoCC {
    private:
        int i;
        NoCC(const NoCC&);
    public:
        NoCC(int ii = 0) : i(ii) {}
};

void f(NoCC);

int main() {
    NoCC n;
//    f(n); //Error
//    NoCC n2 = n; // Error
//    NoCC n3(n); // Error
    return 0;
}
