#include <iostream>

using namespace std;

int* f(int* x) {
    (*x)++;
    return x; // Safe as x is outside the scope
}

int& g(int& x) {
    x++;       // Same as in f()
    return x;  // Safe as outside the scope
}

int& h() {
    int q;
    return q; // reference to local variable ‘q’ returned
    static int x;
    return x; // Safe -- x is otside the scope
}

int main() {
    int a = 0;
    cout << *(f(&a)) << endl;  // Explicit
    cout << g(a) << endl;   // Hidden
    return 0;
}
