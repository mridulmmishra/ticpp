#include <fstream>
#include <string>

using namespace std;

ofstream out("HowMany.out");

class HowMany {
    private:
        static int objCount;
    public:
        HowMany() { objCount++; }
        static void print(const string& msg = "") {
            if(msg.size() != 0) out << msg << ": ";
            out << "objCount = " << objCount << endl;
        }
        ~HowMany() {
            objCount--;
            print("~HowMany()");
        }
};

int HowMany::objCount = 0;

HowMany f(HowMany x) {
    x.print("x argument inside f()");
    return x;
}

int main() {
    HowMany h;
    HowMany::print("After construction of h");
    HowMany h2 = f(h);
    HowMany::print("After call to f()");
    return 0;
}
