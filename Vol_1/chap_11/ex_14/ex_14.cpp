#include <iostream>

using namespace std;

int func(int i) {
    if(i <= 0)
        return 0;
    func(--i);
}

int main() {
    func(20);
}
