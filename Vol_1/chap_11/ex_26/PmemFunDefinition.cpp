#include <iostream>

using namespace std;

class Simple2 {
    public:
        int f(float) const { return 1; }
        int f(float, char) const { cout <<"Overloaded f()" << endl; return 1; }
};

int (Simple2::*fp)(float) const;
int (Simple2::*fp2)(float) const = &Simple2::f;

int main(){
    fp = &Simple2::f;
    Simple2 s;
    int (Simple2::*fp3)(float, char)const = &Simple2::f;
    (s.*fp3)(3.2, 'M');
    return 0;
}
