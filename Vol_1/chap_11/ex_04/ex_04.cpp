#include <iostream>

using namespace std;

int i = 10;

int& func(int* p) {
    int j = 20;
    p = &i;
    //p = &j;
    return *(p);
}

int main() {
    int k = 50;
    int l = 60;
    int *ptr = &k;
    cout << "ptr " << ptr << " : " << *ptr << endl;
    cout << "k   " << &k  << " : " << k << endl;
    cout << "l   " << &l  << " : " << l << endl;
    cout << "i   " << &i  << " : " << i << endl;
    l = func(ptr);   // becomes a copy operation, returned reference(a temporary object) refers to i which is copied to l
    int& m = func(ptr);  // Becomes an initialization, m is intialized by the returned reference(a temporary object)
    cout << "ptr " << ptr << " : " << *ptr << endl;
    cout << "k   " << &k  << " : " << k << endl;
    cout << "l   " << &l  << " : " << l << endl;
    cout << "i   " << &i  << " : " << i << endl;
    cout << "m   " << &m  << " : " << m << endl;
}
