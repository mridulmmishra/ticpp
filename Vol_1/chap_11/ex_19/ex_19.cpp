#include <iostream>

using namespace std;

class X1 {
    private:
    /* data */

    public:
    X1 () {};
    X1(const X1& x1) {
        cout << "X1::X1(X1&) " << endl;
    }
    virtual ~X1 (){};
};


class X2 {
    private:
    /* data */
        X1 x;
    public:
    X2 () {};
    virtual ~X2 (){};
};

int main(){
    X2 x2;
    cout << "Creating a copy of x2" << endl;
    X2 y = x2;
}
