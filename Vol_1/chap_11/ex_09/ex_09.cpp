#include <iostream>

using namespace std;

class X {
    private:
    /* data */
        int i;
    public:
    X (int ii = 0): i(ii) {};
    virtual ~X ();
    int value() const {
        return i;
    }
    void set(int j) {
        i = j;
    }
};


void func1(X y) {
    y.value();
    y.set(3);
}

void func2(X& y) {
    y.value();
    y.set(3);
}

void func3(const X& y) {
    y.value();
    y.set(3);
}

int main(int argc, char* argv[]) {
    X x(1);
    func1(x);
    func2(x);
    func3(x);
    return 0;
}
