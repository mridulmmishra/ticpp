#include <iostream>

using namespace std;

class X {
    private:
    /* data */

    public:
    X() {};
    virtual ~X (){};
    X(const X&) {
        cout << "X::X(X&) " << endl;
    }
};

void func(X x) {
    int i = 0;
}

X func1(){
    X* x = new X;
    return *x;
}

int main(){
    X* x1 = new X();
    cout << "calling func(X)" << endl;
    func(*x1);
    cout << "calling func1()" << endl;
    X x2 = func1();
}
