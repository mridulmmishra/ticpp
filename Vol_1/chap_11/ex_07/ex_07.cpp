#include <iostream>

using namespace std;

int j = 39;
int* ptr1 = &j;
int** ptr2 = &ptr1;

void f(int**& i) {
    i = ptr2;
}

int main(int argc, char* argv[]) {
    int i = 47;
    int* ip = &i;
    int** ip2 = &ip;
    cout << "Global variable ptr2: ";
    cout << **ptr2 << ", " 
        << *ptr2  << ", "
        << ptr2   << endl;
    cout << "ip2 before calling f(): ";
    cout << **ip2 << ", " 
        << *ip2  << ", "
        << ip2   << endl;
    f(ip2);
    cout << "ip2 after calling f(): ";
    cout << **ip2 << ", " 
        << *ip2  << ", "
        << ip2   << endl;
    return 0;
}
