#include <iostream>

using namespace std;

void func(char& c){
    c = 'M';
}

int main(){
    char ch = 'A';
    cout << "ch before func(): " << ch << endl;
    func(ch);
    cout << "ch  after func(): " << ch << endl;
}
