#include <iostream>
#include <vector>

using namespace std;

// A macro to define dummy functions:
#define DF(N) void N() { \
   cout << "function " #N " called..." << endl; }

DF(a); DF(b); DF(c); DF(d); DF(e); DF(f); DF(g);

void (*func_table[])() = { a, b, c, d, e, f, g };

typedef void(*fptr)();
class X {
    private:
    /* data */
        vector<fptr> fv;
    public:
    X (){};
    virtual ~X (){};
    void add(fptr fp){
        fv.push_back(fp);
    }
    void remove(){
        fv.pop_back();
    }
    void run() {
        for(vector<fptr>::iterator f1 = fv.begin(); f1 != fv.end(); f1++)
            (*f1)();
    }
};

int main() {
    X x;
  while(1) {
    cout << "press a key from 'a' to 'g' "
      "or q to quit" << endl;
    char c, cr;
    cin.get(c); cin.get(cr); // second one for CR
    if ( c == 'q' ) 
      break; // ... out of while(1)
    if ( c < 'a' || c > 'g' ) 
      continue;
    //(*func_table[c - 'a'])();
    x.add(*func_table[c - 'a']);
  }
  x.run();
}
