#include <iostream>

using namespace std;

int main(){
    int x = 10;
    int y = 20;
    // int& ref1; // declared as reference but not initialized
    int& ref2 = x; // Initializes ref2 to refer to x
    cout << "ref2 " << &ref2 << " : " << ref2 << endl;
    cout << "x    " << &x    << " : " << x << endl;
    cout << "y    " << &y    << " : " << y << endl;
    ref2 = y; // Does not make ref2 to refer 2 y, rather copies value of y to ref2 thus x also.
    cout << "ref2 " << &ref2 << " : " << ref2 << endl;
    cout << "x    " << &x    << " : " << x << endl;
    cout << "y    " << &y    << " : " << y << endl;
    int& ref3 = *((int *)NULL);
    return 0;
}
