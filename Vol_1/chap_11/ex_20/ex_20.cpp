#include <iostream>

using namespace std;

class X {
    private:
    /* data */

    public:
    X (){};
    virtual ~X (){};
};

X func1(){
    X* x = new X;
    return *x;
}

void func2(X& x){
    int i = 0;
}

void func3(const X& x){
    int i = 0;
}

int main(){
    //func2(func1()); // Doesn't work
    func3(func1());  // works
}
