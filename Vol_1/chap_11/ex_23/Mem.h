#ifndef GUARD_MEM_H
#define GUARD_MEM_H 
typedef unsigned char byte;

class Mem {
    private:
        byte* mem;
        int size;
        void ensureMinSize(int minSize);
        Mem(const Mem&);
    public:
        Mem();
        Mem(int sz);
        ~Mem();
        int msize();
        byte* pointer();
        byte* pointer(int minSize);
}; 
#endif /* MEM_H */
