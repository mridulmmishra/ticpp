#include <iostream>

using namespace std;

class X {
    private:
        /* data */
        X(const X& x); // Private copy constructor so can not pass by value

    public:
        X (){};
        virtual ~X (){};
        X* clone() const{
            cout << "Creating a clone of object" << endl;
            X* tmp = new X;
            return tmp;
        }
};

void func(const X& x){
    X* y = x.clone();
    int i = 0;
}

int main(){
    X x;
    func(x);  // works
}
