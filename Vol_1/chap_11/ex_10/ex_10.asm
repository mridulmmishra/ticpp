GAS LISTING /tmp/ccLn5Wva.s 			page 1


   1              		.file	"ex_10.cpp"
   2              		.text
   3              	.Ltext0:
   4              		.local	_ZStL8__ioinit
   5              		.comm	_ZStL8__ioinit,1,1
   6              		.globl	_Z4funci
   7              		.type	_Z4funci, @function
   8              	_Z4funci:
   9              	.LFB1020:
  10              		.file 1 "ex_10.cpp"
   1:ex_10.cpp     **** #include <iostream>
   2:ex_10.cpp     **** 
   3:ex_10.cpp     **** using namespace std;
   4:ex_10.cpp     **** 
   5:ex_10.cpp     **** int func(int x){
  11              		.loc 1 5 0
  12              		.cfi_startproc
  13 0000 55       		pushq	%rbp
  14              		.cfi_def_cfa_offset 16
  15              		.cfi_offset 6, -16
  16 0001 4889E5   		movq	%rsp, %rbp
  17              		.cfi_def_cfa_register 6
  18 0004 897DFC   		movl	%edi, -4(%rbp)
   6:ex_10.cpp     ****     return ++x;
  19              		.loc 1 6 0
  20 0007 8345FC01 		addl	$1, -4(%rbp)
  21 000b 8B45FC   		movl	-4(%rbp), %eax
   7:ex_10.cpp     **** }
  22              		.loc 1 7 0
  23 000e 5D       		popq	%rbp
  24              		.cfi_def_cfa 7, 8
  25 000f C3       		ret
  26              		.cfi_endproc
  27              	.LFE1020:
  28              		.size	_Z4funci, .-_Z4funci
  29              		.globl	main
  30              		.type	main, @function
  31              	main:
  32              	.LFB1021:
   8:ex_10.cpp     **** 
   9:ex_10.cpp     **** int main(){
  33              		.loc 1 9 0
  34              		.cfi_startproc
  35 0010 55       		pushq	%rbp
  36              		.cfi_def_cfa_offset 16
  37              		.cfi_offset 6, -16
  38 0011 4889E5   		movq	%rsp, %rbp
  39              		.cfi_def_cfa_register 6
  40 0014 4883EC10 		subq	$16, %rsp
  41              	.LBB2:
  10:ex_10.cpp     ****     int i = 3;
  42              		.loc 1 10 0
  43 0018 C745FC03 		movl	$3, -4(%rbp)
  43      000000
  11:ex_10.cpp     ****     i = func(i);
  44              		.loc 1 11 0
  45 001f 8B45FC   		movl	-4(%rbp), %eax
GAS LISTING /tmp/ccLn5Wva.s 			page 2


  46 0022 89C7     		movl	%eax, %edi
  47 0024 E8000000 		call	_Z4funci
  47      00
  48 0029 8945FC   		movl	%eax, -4(%rbp)
  49              	.LBE2:
  12:ex_10.cpp     **** }
  50              		.loc 1 12 0
  51 002c B8000000 		movl	$0, %eax
  51      00
  52 0031 C9       		leave
  53              		.cfi_def_cfa 7, 8
  54 0032 C3       		ret
  55              		.cfi_endproc
  56              	.LFE1021:
  57              		.size	main, .-main
  58              		.type	_Z41__static_initialization_and_destruction_0ii, @function
  59              	_Z41__static_initialization_and_destruction_0ii:
  60              	.LFB1022:
  61              		.loc 1 12 0
  62              		.cfi_startproc
  63 0033 55       		pushq	%rbp
  64              		.cfi_def_cfa_offset 16
  65              		.cfi_offset 6, -16
  66 0034 4889E5   		movq	%rsp, %rbp
  67              		.cfi_def_cfa_register 6
  68 0037 4883EC10 		subq	$16, %rsp
  69 003b 897DFC   		movl	%edi, -4(%rbp)
  70 003e 8975F8   		movl	%esi, -8(%rbp)
  71              		.loc 1 12 0
  72 0041 837DFC01 		cmpl	$1, -4(%rbp)
  73 0045 7527     		jne	.L5
  74              		.loc 1 12 0 is_stmt 0 discriminator 1
  75 0047 817DF8FF 		cmpl	$65535, -8(%rbp)
  75      FF0000
  76 004e 751E     		jne	.L5
  77              		.file 2 "/usr/include/c++/4.9.2/iostream"
   1:/usr/include/c++/4.9.2/iostream **** // Standard iostream objects -*- C++ -*-
   2:/usr/include/c++/4.9.2/iostream **** 
   3:/usr/include/c++/4.9.2/iostream **** // Copyright (C) 1997-2014 Free Software Foundation, Inc.
   4:/usr/include/c++/4.9.2/iostream **** //
   5:/usr/include/c++/4.9.2/iostream **** // This file is part of the GNU ISO C++ Library.  This library is free
   6:/usr/include/c++/4.9.2/iostream **** // software; you can redistribute it and/or modify it under the
   7:/usr/include/c++/4.9.2/iostream **** // terms of the GNU General Public License as published by the
   8:/usr/include/c++/4.9.2/iostream **** // Free Software Foundation; either version 3, or (at your option)
   9:/usr/include/c++/4.9.2/iostream **** // any later version.
  10:/usr/include/c++/4.9.2/iostream **** 
  11:/usr/include/c++/4.9.2/iostream **** // This library is distributed in the hope that it will be useful,
  12:/usr/include/c++/4.9.2/iostream **** // but WITHOUT ANY WARRANTY; without even the implied warranty of
  13:/usr/include/c++/4.9.2/iostream **** // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  14:/usr/include/c++/4.9.2/iostream **** // GNU General Public License for more details.
  15:/usr/include/c++/4.9.2/iostream **** 
  16:/usr/include/c++/4.9.2/iostream **** // Under Section 7 of GPL version 3, you are granted additional
  17:/usr/include/c++/4.9.2/iostream **** // permissions described in the GCC Runtime Library Exception, version
  18:/usr/include/c++/4.9.2/iostream **** // 3.1, as published by the Free Software Foundation.
  19:/usr/include/c++/4.9.2/iostream **** 
  20:/usr/include/c++/4.9.2/iostream **** // You should have received a copy of the GNU General Public License and
  21:/usr/include/c++/4.9.2/iostream **** // a copy of the GCC Runtime Library Exception along with this program;
GAS LISTING /tmp/ccLn5Wva.s 			page 3


  22:/usr/include/c++/4.9.2/iostream **** // see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
  23:/usr/include/c++/4.9.2/iostream **** // <http://www.gnu.org/licenses/>.
  24:/usr/include/c++/4.9.2/iostream **** 
  25:/usr/include/c++/4.9.2/iostream **** /** @file include/iostream
  26:/usr/include/c++/4.9.2/iostream ****  *  This is a Standard C++ Library header.
  27:/usr/include/c++/4.9.2/iostream ****  */
  28:/usr/include/c++/4.9.2/iostream **** 
  29:/usr/include/c++/4.9.2/iostream **** //
  30:/usr/include/c++/4.9.2/iostream **** // ISO C++ 14882: 27.3  Standard iostream objects
  31:/usr/include/c++/4.9.2/iostream **** //
  32:/usr/include/c++/4.9.2/iostream **** 
  33:/usr/include/c++/4.9.2/iostream **** #ifndef _GLIBCXX_IOSTREAM
  34:/usr/include/c++/4.9.2/iostream **** #define _GLIBCXX_IOSTREAM 1
  35:/usr/include/c++/4.9.2/iostream **** 
  36:/usr/include/c++/4.9.2/iostream **** #pragma GCC system_header
  37:/usr/include/c++/4.9.2/iostream **** 
  38:/usr/include/c++/4.9.2/iostream **** #include <bits/c++config.h>
  39:/usr/include/c++/4.9.2/iostream **** #include <ostream>
  40:/usr/include/c++/4.9.2/iostream **** #include <istream>
  41:/usr/include/c++/4.9.2/iostream **** 
  42:/usr/include/c++/4.9.2/iostream **** namespace std _GLIBCXX_VISIBILITY(default)
  43:/usr/include/c++/4.9.2/iostream **** {
  44:/usr/include/c++/4.9.2/iostream **** _GLIBCXX_BEGIN_NAMESPACE_VERSION
  45:/usr/include/c++/4.9.2/iostream **** 
  46:/usr/include/c++/4.9.2/iostream ****   /**
  47:/usr/include/c++/4.9.2/iostream ****    *  @name Standard Stream Objects
  48:/usr/include/c++/4.9.2/iostream ****    *
  49:/usr/include/c++/4.9.2/iostream ****    *  The &lt;iostream&gt; header declares the eight <em>standard stream
  50:/usr/include/c++/4.9.2/iostream ****    *  objects</em>.  For other declarations, see
  51:/usr/include/c++/4.9.2/iostream ****    *  http://gcc.gnu.org/onlinedocs/libstdc++/manual/io.html
  52:/usr/include/c++/4.9.2/iostream ****    *  and the @link iosfwd I/O forward declarations @endlink
  53:/usr/include/c++/4.9.2/iostream ****    *
  54:/usr/include/c++/4.9.2/iostream ****    *  They are required by default to cooperate with the global C
  55:/usr/include/c++/4.9.2/iostream ****    *  library's @c FILE streams, and to be available during program
  56:/usr/include/c++/4.9.2/iostream ****    *  startup and termination. For more information, see the section of the
  57:/usr/include/c++/4.9.2/iostream ****    *  manual linked to above.
  58:/usr/include/c++/4.9.2/iostream ****   */
  59:/usr/include/c++/4.9.2/iostream ****   //@{
  60:/usr/include/c++/4.9.2/iostream ****   extern istream cin;		/// Linked to standard input
  61:/usr/include/c++/4.9.2/iostream ****   extern ostream cout;		/// Linked to standard output
  62:/usr/include/c++/4.9.2/iostream ****   extern ostream cerr;		/// Linked to standard error (unbuffered)
  63:/usr/include/c++/4.9.2/iostream ****   extern ostream clog;		/// Linked to standard error (buffered)
  64:/usr/include/c++/4.9.2/iostream **** 
  65:/usr/include/c++/4.9.2/iostream **** #ifdef _GLIBCXX_USE_WCHAR_T
  66:/usr/include/c++/4.9.2/iostream ****   extern wistream wcin;		/// Linked to standard input
  67:/usr/include/c++/4.9.2/iostream ****   extern wostream wcout;	/// Linked to standard output
  68:/usr/include/c++/4.9.2/iostream ****   extern wostream wcerr;	/// Linked to standard error (unbuffered)
  69:/usr/include/c++/4.9.2/iostream ****   extern wostream wclog;	/// Linked to standard error (buffered)
  70:/usr/include/c++/4.9.2/iostream **** #endif
  71:/usr/include/c++/4.9.2/iostream ****   //@}
  72:/usr/include/c++/4.9.2/iostream **** 
  73:/usr/include/c++/4.9.2/iostream ****   // For construction of filebuffers for cout, cin, cerr, clog et. al.
  74:/usr/include/c++/4.9.2/iostream ****   static ios_base::Init __ioinit;
  78              		.loc 2 74 0 is_stmt 1
  79 0050 BF000000 		movl	$_ZStL8__ioinit, %edi
  79      00
  80 0055 E8000000 		call	_ZNSt8ios_base4InitC1Ev
GAS LISTING /tmp/ccLn5Wva.s 			page 4


  80      00
  81 005a BA000000 		movl	$__dso_handle, %edx
  81      00
  82 005f BE000000 		movl	$_ZStL8__ioinit, %esi
  82      00
  83 0064 BF000000 		movl	$_ZNSt8ios_base4InitD1Ev, %edi
  83      00
  84 0069 E8000000 		call	__cxa_atexit
  84      00
  85              	.L5:
  86              		.loc 1 12 0
  87 006e C9       		leave
  88              		.cfi_def_cfa 7, 8
  89 006f C3       		ret
  90              		.cfi_endproc
  91              	.LFE1022:
  92              		.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destructi
  93              		.type	_GLOBAL__sub_I__Z4funci, @function
  94              	_GLOBAL__sub_I__Z4funci:
  95              	.LFB1023:
  96              		.loc 1 12 0
  97              		.cfi_startproc
  98 0070 55       		pushq	%rbp
  99              		.cfi_def_cfa_offset 16
 100              		.cfi_offset 6, -16
 101 0071 4889E5   		movq	%rsp, %rbp
 102              		.cfi_def_cfa_register 6
 103              		.loc 1 12 0
 104 0074 BEFFFF00 		movl	$65535, %esi
 104      00
 105 0079 BF010000 		movl	$1, %edi
 105      00
 106 007e E8B0FFFF 		call	_Z41__static_initialization_and_destruction_0ii
 106      FF
 107 0083 5D       		popq	%rbp
 108              		.cfi_def_cfa 7, 8
 109 0084 C3       		ret
 110              		.cfi_endproc
 111              	.LFE1023:
 112              		.size	_GLOBAL__sub_I__Z4funci, .-_GLOBAL__sub_I__Z4funci
 113              		.section	.init_array,"aw"
 114              		.align 8
 115 0000 00000000 		.quad	_GLOBAL__sub_I__Z4funci
 115      00000000 
 116              		.text
 117              	.Letext0:
 118              		.file 3 "<built-in>"
 119              		.file 4 "/usr/include/stdio.h"
 120              		.file 5 "/usr/lib/gcc/x86_64-unknown-linux-gnu/4.9.2/include/stddef.h"
 121              		.file 6 "/usr/include/wchar.h"
 122              		.file 7 "/usr/include/c++/4.9.2/cwchar"
 123              		.file 8 "/usr/include/c++/4.9.2/x86_64-unknown-linux-gnu/bits/c++config.h"
 124              		.file 9 "/usr/include/c++/4.9.2/clocale"
 125              		.file 10 "/usr/include/c++/4.9.2/bits/ios_base.h"
 126              		.file 11 "/usr/include/c++/4.9.2/cwctype"
 127              		.file 12 "/usr/include/time.h"
 128              		.file 13 "/usr/include/c++/4.9.2/debug/debug.h"
GAS LISTING /tmp/ccLn5Wva.s 			page 5


 129              		.file 14 "/usr/include/c++/4.9.2/bits/predefined_ops.h"
 130              		.file 15 "/usr/include/c++/4.9.2/ext/new_allocator.h"
 131              		.file 16 "/usr/include/c++/4.9.2/ext/numeric_traits.h"
 132              		.file 17 "/usr/include/locale.h"
 133              		.file 18 "/usr/include/bits/types.h"
 134              		.file 19 "/usr/include/c++/4.9.2/x86_64-unknown-linux-gnu/bits/atomic_word.h"
 135              		.file 20 "/usr/include/wctype.h"
 136              		.section	.debug_info,"",@progbits
 137              	.Ldebug_info0:
 138 0000 8F130000 		.long	0x138f
 139 0004 0400     		.value	0x4
 140 0006 00000000 		.long	.Ldebug_abbrev0
 141 000a 08       		.byte	0x8
 142 000b 01       		.uleb128 0x1
 143 000c 00000000 		.long	.LASF229
 144 0010 04       		.byte	0x4
 145 0011 00000000 		.long	.LASF230
 146 0015 00000000 		.long	.LASF231
 147 0019 00000000 		.quad	.Ltext0
 147      00000000 
 148 0021 85000000 		.quad	.Letext0-.Ltext0
 148      00000000 
 149 0029 00000000 		.long	.Ldebug_line0
 150 002d 02       		.uleb128 0x2
 151 002e 00000000 		.long	.LASF232
 152 0032 03       		.uleb128 0x3
 153 0033 00000000 		.long	.LASF6
 154 0037 04       		.byte	0x4
 155 0038 40       		.byte	0x40
 156 0039 2D000000 		.long	0x2d
 157 003d 04       		.uleb128 0x4
 158 003e 08       		.byte	0x8
 159 003f 07       		.byte	0x7
 160 0040 00000000 		.long	.LASF4
 161 0044 05       		.uleb128 0x5
 162 0045 00000000 		.long	.LASF118
 163 0049 18       		.byte	0x18
 164 004a 03       		.byte	0x3
 165 004b 00       		.byte	0
 166 004c 81000000 		.long	0x81
 167 0050 06       		.uleb128 0x6
 168 0051 00000000 		.long	.LASF0
 169 0055 03       		.byte	0x3
 170 0056 00       		.byte	0
 171 0057 81000000 		.long	0x81
 172 005b 00       		.byte	0
 173 005c 06       		.uleb128 0x6
 174 005d 00000000 		.long	.LASF1
 175 0061 03       		.byte	0x3
 176 0062 00       		.byte	0
 177 0063 81000000 		.long	0x81
 178 0067 04       		.byte	0x4
 179 0068 06       		.uleb128 0x6
 180 0069 00000000 		.long	.LASF2
 181 006d 03       		.byte	0x3
 182 006e 00       		.byte	0
 183 006f 88000000 		.long	0x88
GAS LISTING /tmp/ccLn5Wva.s 			page 6


 184 0073 08       		.byte	0x8
 185 0074 06       		.uleb128 0x6
 186 0075 00000000 		.long	.LASF3
 187 0079 03       		.byte	0x3
 188 007a 00       		.byte	0
 189 007b 88000000 		.long	0x88
 190 007f 10       		.byte	0x10
 191 0080 00       		.byte	0
 192 0081 04       		.uleb128 0x4
 193 0082 04       		.byte	0x4
 194 0083 07       		.byte	0x7
 195 0084 00000000 		.long	.LASF5
 196 0088 07       		.uleb128 0x7
 197 0089 08       		.byte	0x8
 198 008a 03       		.uleb128 0x3
 199 008b 00000000 		.long	.LASF7
 200 008f 05       		.byte	0x5
 201 0090 D4       		.byte	0xd4
 202 0091 95000000 		.long	0x95
 203 0095 04       		.uleb128 0x4
 204 0096 08       		.byte	0x8
 205 0097 07       		.byte	0x7
 206 0098 00000000 		.long	.LASF8
 207 009c 08       		.uleb128 0x8
 208 009d 00000000 		.long	.LASF9
 209 00a1 05       		.byte	0x5
 210 00a2 6101     		.value	0x161
 211 00a4 81000000 		.long	0x81
 212 00a8 09       		.uleb128 0x9
 213 00a9 08       		.byte	0x8
 214 00aa 06       		.byte	0x6
 215 00ab 53       		.byte	0x53
 216 00ac 00000000 		.long	.LASF233
 217 00b0 EC000000 		.long	0xec
 218 00b4 0A       		.uleb128 0xa
 219 00b5 04       		.byte	0x4
 220 00b6 06       		.byte	0x6
 221 00b7 56       		.byte	0x56
 222 00b8 D3000000 		.long	0xd3
 223 00bc 0B       		.uleb128 0xb
 224 00bd 00000000 		.long	.LASF10
 225 00c1 06       		.byte	0x6
 226 00c2 58       		.byte	0x58
 227 00c3 81000000 		.long	0x81
 228 00c7 0B       		.uleb128 0xb
 229 00c8 00000000 		.long	.LASF11
 230 00cc 06       		.byte	0x6
 231 00cd 5C       		.byte	0x5c
 232 00ce EC000000 		.long	0xec
 233 00d2 00       		.byte	0
 234 00d3 06       		.uleb128 0x6
 235 00d4 00000000 		.long	.LASF12
 236 00d8 06       		.byte	0x6
 237 00d9 54       		.byte	0x54
 238 00da 03010000 		.long	0x103
 239 00de 00       		.byte	0
 240 00df 06       		.uleb128 0x6
GAS LISTING /tmp/ccLn5Wva.s 			page 7


 241 00e0 00000000 		.long	.LASF13
 242 00e4 06       		.byte	0x6
 243 00e5 5D       		.byte	0x5d
 244 00e6 B4000000 		.long	0xb4
 245 00ea 04       		.byte	0x4
 246 00eb 00       		.byte	0
 247 00ec 0C       		.uleb128 0xc
 248 00ed FC000000 		.long	0xfc
 249 00f1 FC000000 		.long	0xfc
 250 00f5 0D       		.uleb128 0xd
 251 00f6 3D000000 		.long	0x3d
 252 00fa 03       		.byte	0x3
 253 00fb 00       		.byte	0
 254 00fc 04       		.uleb128 0x4
 255 00fd 01       		.byte	0x1
 256 00fe 06       		.byte	0x6
 257 00ff 00000000 		.long	.LASF14
 258 0103 0E       		.uleb128 0xe
 259 0104 04       		.byte	0x4
 260 0105 05       		.byte	0x5
 261 0106 696E7400 		.string	"int"
 262 010a 03       		.uleb128 0x3
 263 010b 00000000 		.long	.LASF15
 264 010f 06       		.byte	0x6
 265 0110 5E       		.byte	0x5e
 266 0111 A8000000 		.long	0xa8
 267 0115 03       		.uleb128 0x3
 268 0116 00000000 		.long	.LASF16
 269 011a 06       		.byte	0x6
 270 011b 6A       		.byte	0x6a
 271 011c 0A010000 		.long	0x10a
 272 0120 04       		.uleb128 0x4
 273 0121 02       		.byte	0x2
 274 0122 07       		.byte	0x7
 275 0123 00000000 		.long	.LASF17
 276 0127 0F       		.uleb128 0xf
 277 0128 03010000 		.long	0x103
 278 012c 10       		.uleb128 0x10
 279 012d 08       		.byte	0x8
 280 012e 32010000 		.long	0x132
 281 0132 0F       		.uleb128 0xf
 282 0133 FC000000 		.long	0xfc
 283 0137 11       		.uleb128 0x11
 284 0138 73746400 		.string	"std"
 285 013c 03       		.byte	0x3
 286 013d 00       		.byte	0
 287 013e DB060000 		.long	0x6db
 288 0142 12       		.uleb128 0x12
 289 0143 07       		.byte	0x7
 290 0144 40       		.byte	0x40
 291 0145 15010000 		.long	0x115
 292 0149 12       		.uleb128 0x12
 293 014a 07       		.byte	0x7
 294 014b 8B       		.byte	0x8b
 295 014c 9C000000 		.long	0x9c
 296 0150 12       		.uleb128 0x12
 297 0151 07       		.byte	0x7
GAS LISTING /tmp/ccLn5Wva.s 			page 8


 298 0152 8D       		.byte	0x8d
 299 0153 DB060000 		.long	0x6db
 300 0157 12       		.uleb128 0x12
 301 0158 07       		.byte	0x7
 302 0159 8E       		.byte	0x8e
 303 015a F1060000 		.long	0x6f1
 304 015e 12       		.uleb128 0x12
 305 015f 07       		.byte	0x7
 306 0160 8F       		.byte	0x8f
 307 0161 0D070000 		.long	0x70d
 308 0165 12       		.uleb128 0x12
 309 0166 07       		.byte	0x7
 310 0167 90       		.byte	0x90
 311 0168 3A070000 		.long	0x73a
 312 016c 12       		.uleb128 0x12
 313 016d 07       		.byte	0x7
 314 016e 91       		.byte	0x91
 315 016f 55070000 		.long	0x755
 316 0173 12       		.uleb128 0x12
 317 0174 07       		.byte	0x7
 318 0175 92       		.byte	0x92
 319 0176 7B070000 		.long	0x77b
 320 017a 12       		.uleb128 0x12
 321 017b 07       		.byte	0x7
 322 017c 93       		.byte	0x93
 323 017d 96070000 		.long	0x796
 324 0181 12       		.uleb128 0x12
 325 0182 07       		.byte	0x7
 326 0183 94       		.byte	0x94
 327 0184 B2070000 		.long	0x7b2
 328 0188 12       		.uleb128 0x12
 329 0189 07       		.byte	0x7
 330 018a 95       		.byte	0x95
 331 018b CE070000 		.long	0x7ce
 332 018f 12       		.uleb128 0x12
 333 0190 07       		.byte	0x7
 334 0191 96       		.byte	0x96
 335 0192 E4070000 		.long	0x7e4
 336 0196 12       		.uleb128 0x12
 337 0197 07       		.byte	0x7
 338 0198 97       		.byte	0x97
 339 0199 F0070000 		.long	0x7f0
 340 019d 12       		.uleb128 0x12
 341 019e 07       		.byte	0x7
 342 019f 98       		.byte	0x98
 343 01a0 16080000 		.long	0x816
 344 01a4 12       		.uleb128 0x12
 345 01a5 07       		.byte	0x7
 346 01a6 99       		.byte	0x99
 347 01a7 3B080000 		.long	0x83b
 348 01ab 12       		.uleb128 0x12
 349 01ac 07       		.byte	0x7
 350 01ad 9A       		.byte	0x9a
 351 01ae 5C080000 		.long	0x85c
 352 01b2 12       		.uleb128 0x12
 353 01b3 07       		.byte	0x7
 354 01b4 9B       		.byte	0x9b
GAS LISTING /tmp/ccLn5Wva.s 			page 9


 355 01b5 87080000 		.long	0x887
 356 01b9 12       		.uleb128 0x12
 357 01ba 07       		.byte	0x7
 358 01bb 9C       		.byte	0x9c
 359 01bc A2080000 		.long	0x8a2
 360 01c0 12       		.uleb128 0x12
 361 01c1 07       		.byte	0x7
 362 01c2 9E       		.byte	0x9e
 363 01c3 B8080000 		.long	0x8b8
 364 01c7 12       		.uleb128 0x12
 365 01c8 07       		.byte	0x7
 366 01c9 A0       		.byte	0xa0
 367 01ca D9080000 		.long	0x8d9
 368 01ce 12       		.uleb128 0x12
 369 01cf 07       		.byte	0x7
 370 01d0 A1       		.byte	0xa1
 371 01d1 F5080000 		.long	0x8f5
 372 01d5 12       		.uleb128 0x12
 373 01d6 07       		.byte	0x7
 374 01d7 A2       		.byte	0xa2
 375 01d8 10090000 		.long	0x910
 376 01dc 12       		.uleb128 0x12
 377 01dd 07       		.byte	0x7
 378 01de A4       		.byte	0xa4
 379 01df 36090000 		.long	0x936
 380 01e3 12       		.uleb128 0x12
 381 01e4 07       		.byte	0x7
 382 01e5 A7       		.byte	0xa7
 383 01e6 56090000 		.long	0x956
 384 01ea 12       		.uleb128 0x12
 385 01eb 07       		.byte	0x7
 386 01ec AA       		.byte	0xaa
 387 01ed 7B090000 		.long	0x97b
 388 01f1 12       		.uleb128 0x12
 389 01f2 07       		.byte	0x7
 390 01f3 AC       		.byte	0xac
 391 01f4 9B090000 		.long	0x99b
 392 01f8 12       		.uleb128 0x12
 393 01f9 07       		.byte	0x7
 394 01fa AE       		.byte	0xae
 395 01fb B6090000 		.long	0x9b6
 396 01ff 12       		.uleb128 0x12
 397 0200 07       		.byte	0x7
 398 0201 B0       		.byte	0xb0
 399 0202 D1090000 		.long	0x9d1
 400 0206 12       		.uleb128 0x12
 401 0207 07       		.byte	0x7
 402 0208 B1       		.byte	0xb1
 403 0209 F7090000 		.long	0x9f7
 404 020d 12       		.uleb128 0x12
 405 020e 07       		.byte	0x7
 406 020f B2       		.byte	0xb2
 407 0210 110A0000 		.long	0xa11
 408 0214 12       		.uleb128 0x12
 409 0215 07       		.byte	0x7
 410 0216 B3       		.byte	0xb3
 411 0217 2B0A0000 		.long	0xa2b
GAS LISTING /tmp/ccLn5Wva.s 			page 10


 412 021b 12       		.uleb128 0x12
 413 021c 07       		.byte	0x7
 414 021d B4       		.byte	0xb4
 415 021e 450A0000 		.long	0xa45
 416 0222 12       		.uleb128 0x12
 417 0223 07       		.byte	0x7
 418 0224 B5       		.byte	0xb5
 419 0225 5F0A0000 		.long	0xa5f
 420 0229 12       		.uleb128 0x12
 421 022a 07       		.byte	0x7
 422 022b B6       		.byte	0xb6
 423 022c 790A0000 		.long	0xa79
 424 0230 12       		.uleb128 0x12
 425 0231 07       		.byte	0x7
 426 0232 B7       		.byte	0xb7
 427 0233 390B0000 		.long	0xb39
 428 0237 12       		.uleb128 0x12
 429 0238 07       		.byte	0x7
 430 0239 B8       		.byte	0xb8
 431 023a 4F0B0000 		.long	0xb4f
 432 023e 12       		.uleb128 0x12
 433 023f 07       		.byte	0x7
 434 0240 B9       		.byte	0xb9
 435 0241 6E0B0000 		.long	0xb6e
 436 0245 12       		.uleb128 0x12
 437 0246 07       		.byte	0x7
 438 0247 BA       		.byte	0xba
 439 0248 8D0B0000 		.long	0xb8d
 440 024c 12       		.uleb128 0x12
 441 024d 07       		.byte	0x7
 442 024e BB       		.byte	0xbb
 443 024f AC0B0000 		.long	0xbac
 444 0253 12       		.uleb128 0x12
 445 0254 07       		.byte	0x7
 446 0255 BC       		.byte	0xbc
 447 0256 D70B0000 		.long	0xbd7
 448 025a 12       		.uleb128 0x12
 449 025b 07       		.byte	0x7
 450 025c BD       		.byte	0xbd
 451 025d F20B0000 		.long	0xbf2
 452 0261 12       		.uleb128 0x12
 453 0262 07       		.byte	0x7
 454 0263 BF       		.byte	0xbf
 455 0264 1A0C0000 		.long	0xc1a
 456 0268 12       		.uleb128 0x12
 457 0269 07       		.byte	0x7
 458 026a C1       		.byte	0xc1
 459 026b 3C0C0000 		.long	0xc3c
 460 026f 12       		.uleb128 0x12
 461 0270 07       		.byte	0x7
 462 0271 C2       		.byte	0xc2
 463 0272 5C0C0000 		.long	0xc5c
 464 0276 12       		.uleb128 0x12
 465 0277 07       		.byte	0x7
 466 0278 C3       		.byte	0xc3
 467 0279 830C0000 		.long	0xc83
 468 027d 12       		.uleb128 0x12
GAS LISTING /tmp/ccLn5Wva.s 			page 11


 469 027e 07       		.byte	0x7
 470 027f C4       		.byte	0xc4
 471 0280 A30C0000 		.long	0xca3
 472 0284 12       		.uleb128 0x12
 473 0285 07       		.byte	0x7
 474 0286 C5       		.byte	0xc5
 475 0287 C20C0000 		.long	0xcc2
 476 028b 12       		.uleb128 0x12
 477 028c 07       		.byte	0x7
 478 028d C6       		.byte	0xc6
 479 028e D80C0000 		.long	0xcd8
 480 0292 12       		.uleb128 0x12
 481 0293 07       		.byte	0x7
 482 0294 C7       		.byte	0xc7
 483 0295 F80C0000 		.long	0xcf8
 484 0299 12       		.uleb128 0x12
 485 029a 07       		.byte	0x7
 486 029b C8       		.byte	0xc8
 487 029c 180D0000 		.long	0xd18
 488 02a0 12       		.uleb128 0x12
 489 02a1 07       		.byte	0x7
 490 02a2 C9       		.byte	0xc9
 491 02a3 380D0000 		.long	0xd38
 492 02a7 12       		.uleb128 0x12
 493 02a8 07       		.byte	0x7
 494 02a9 CA       		.byte	0xca
 495 02aa 580D0000 		.long	0xd58
 496 02ae 12       		.uleb128 0x12
 497 02af 07       		.byte	0x7
 498 02b0 CB       		.byte	0xcb
 499 02b1 6F0D0000 		.long	0xd6f
 500 02b5 12       		.uleb128 0x12
 501 02b6 07       		.byte	0x7
 502 02b7 CC       		.byte	0xcc
 503 02b8 860D0000 		.long	0xd86
 504 02bc 12       		.uleb128 0x12
 505 02bd 07       		.byte	0x7
 506 02be CD       		.byte	0xcd
 507 02bf A40D0000 		.long	0xda4
 508 02c3 12       		.uleb128 0x12
 509 02c4 07       		.byte	0x7
 510 02c5 CE       		.byte	0xce
 511 02c6 C30D0000 		.long	0xdc3
 512 02ca 12       		.uleb128 0x12
 513 02cb 07       		.byte	0x7
 514 02cc CF       		.byte	0xcf
 515 02cd E10D0000 		.long	0xde1
 516 02d1 12       		.uleb128 0x12
 517 02d2 07       		.byte	0x7
 518 02d3 D0       		.byte	0xd0
 519 02d4 000E0000 		.long	0xe00
 520 02d8 13       		.uleb128 0x13
 521 02d9 07       		.byte	0x7
 522 02da 0801     		.value	0x108
 523 02dc A20F0000 		.long	0xfa2
 524 02e0 13       		.uleb128 0x13
 525 02e1 07       		.byte	0x7
GAS LISTING /tmp/ccLn5Wva.s 			page 12


 526 02e2 0901     		.value	0x109
 527 02e4 C40F0000 		.long	0xfc4
 528 02e8 13       		.uleb128 0x13
 529 02e9 07       		.byte	0x7
 530 02ea 0A01     		.value	0x10a
 531 02ec EB0F0000 		.long	0xfeb
 532 02f0 14       		.uleb128 0x14
 533 02f1 00000000 		.long	.LASF159
 534 02f5 0D       		.byte	0xd
 535 02f6 30       		.byte	0x30
 536 02f7 03       		.uleb128 0x3
 537 02f8 00000000 		.long	.LASF7
 538 02fc 08       		.byte	0x8
 539 02fd BC       		.byte	0xbc
 540 02fe 95000000 		.long	0x95
 541 0302 12       		.uleb128 0x12
 542 0303 09       		.byte	0x9
 543 0304 35       		.byte	0x35
 544 0305 41100000 		.long	0x1041
 545 0309 12       		.uleb128 0x12
 546 030a 09       		.byte	0x9
 547 030b 36       		.byte	0x36
 548 030c 6E110000 		.long	0x116e
 549 0310 12       		.uleb128 0x12
 550 0311 09       		.byte	0x9
 551 0312 37       		.byte	0x37
 552 0313 88110000 		.long	0x1188
 553 0317 03       		.uleb128 0x3
 554 0318 00000000 		.long	.LASF18
 555 031c 08       		.byte	0x8
 556 031d BD       		.byte	0xbd
 557 031e 7C0C0000 		.long	0xc7c
 558 0322 15       		.uleb128 0x15
 559 0323 00000000 		.long	.LASF38
 560 0327 04       		.byte	0x4
 561 0328 0A       		.byte	0xa
 562 0329 33       		.byte	0x33
 563 032a B1030000 		.long	0x3b1
 564 032e 16       		.uleb128 0x16
 565 032f 00000000 		.long	.LASF19
 566 0333 01       		.sleb128 1
 567 0334 16       		.uleb128 0x16
 568 0335 00000000 		.long	.LASF20
 569 0339 02       		.sleb128 2
 570 033a 16       		.uleb128 0x16
 571 033b 00000000 		.long	.LASF21
 572 033f 04       		.sleb128 4
 573 0340 16       		.uleb128 0x16
 574 0341 00000000 		.long	.LASF22
 575 0345 08       		.sleb128 8
 576 0346 16       		.uleb128 0x16
 577 0347 00000000 		.long	.LASF23
 578 034b 10       		.sleb128 16
 579 034c 16       		.uleb128 0x16
 580 034d 00000000 		.long	.LASF24
 581 0351 20       		.sleb128 32
 582 0352 16       		.uleb128 0x16
GAS LISTING /tmp/ccLn5Wva.s 			page 13


 583 0353 00000000 		.long	.LASF25
 584 0357 C000     		.sleb128 64
 585 0359 16       		.uleb128 0x16
 586 035a 00000000 		.long	.LASF26
 587 035e 8001     		.sleb128 128
 588 0360 16       		.uleb128 0x16
 589 0361 00000000 		.long	.LASF27
 590 0365 8002     		.sleb128 256
 591 0367 16       		.uleb128 0x16
 592 0368 00000000 		.long	.LASF28
 593 036c 8004     		.sleb128 512
 594 036e 16       		.uleb128 0x16
 595 036f 00000000 		.long	.LASF29
 596 0373 8008     		.sleb128 1024
 597 0375 16       		.uleb128 0x16
 598 0376 00000000 		.long	.LASF30
 599 037a 8010     		.sleb128 2048
 600 037c 16       		.uleb128 0x16
 601 037d 00000000 		.long	.LASF31
 602 0381 8020     		.sleb128 4096
 603 0383 16       		.uleb128 0x16
 604 0384 00000000 		.long	.LASF32
 605 0388 80C000   		.sleb128 8192
 606 038b 16       		.uleb128 0x16
 607 038c 00000000 		.long	.LASF33
 608 0390 808001   		.sleb128 16384
 609 0393 16       		.uleb128 0x16
 610 0394 00000000 		.long	.LASF34
 611 0398 B001     		.sleb128 176
 612 039a 16       		.uleb128 0x16
 613 039b 00000000 		.long	.LASF35
 614 039f CA00     		.sleb128 74
 615 03a1 16       		.uleb128 0x16
 616 03a2 00000000 		.long	.LASF36
 617 03a6 8402     		.sleb128 260
 618 03a8 16       		.uleb128 0x16
 619 03a9 00000000 		.long	.LASF37
 620 03ad 808004   		.sleb128 65536
 621 03b0 00       		.byte	0
 622 03b1 15       		.uleb128 0x15
 623 03b2 00000000 		.long	.LASF39
 624 03b6 04       		.byte	0x4
 625 03b7 0A       		.byte	0xa
 626 03b8 67       		.byte	0x67
 627 03b9 EA030000 		.long	0x3ea
 628 03bd 16       		.uleb128 0x16
 629 03be 00000000 		.long	.LASF40
 630 03c2 01       		.sleb128 1
 631 03c3 16       		.uleb128 0x16
 632 03c4 00000000 		.long	.LASF41
 633 03c8 02       		.sleb128 2
 634 03c9 16       		.uleb128 0x16
 635 03ca 00000000 		.long	.LASF42
 636 03ce 04       		.sleb128 4
 637 03cf 16       		.uleb128 0x16
 638 03d0 00000000 		.long	.LASF43
 639 03d4 08       		.sleb128 8
GAS LISTING /tmp/ccLn5Wva.s 			page 14


 640 03d5 16       		.uleb128 0x16
 641 03d6 00000000 		.long	.LASF44
 642 03da 10       		.sleb128 16
 643 03db 16       		.uleb128 0x16
 644 03dc 00000000 		.long	.LASF45
 645 03e0 20       		.sleb128 32
 646 03e1 16       		.uleb128 0x16
 647 03e2 00000000 		.long	.LASF46
 648 03e6 808004   		.sleb128 65536
 649 03e9 00       		.byte	0
 650 03ea 15       		.uleb128 0x15
 651 03eb 00000000 		.long	.LASF47
 652 03ef 04       		.byte	0x4
 653 03f0 0A       		.byte	0xa
 654 03f1 8F       		.byte	0x8f
 655 03f2 17040000 		.long	0x417
 656 03f6 16       		.uleb128 0x16
 657 03f7 00000000 		.long	.LASF48
 658 03fb 00       		.sleb128 0
 659 03fc 16       		.uleb128 0x16
 660 03fd 00000000 		.long	.LASF49
 661 0401 01       		.sleb128 1
 662 0402 16       		.uleb128 0x16
 663 0403 00000000 		.long	.LASF50
 664 0407 02       		.sleb128 2
 665 0408 16       		.uleb128 0x16
 666 0409 00000000 		.long	.LASF51
 667 040d 04       		.sleb128 4
 668 040e 16       		.uleb128 0x16
 669 040f 00000000 		.long	.LASF52
 670 0413 808004   		.sleb128 65536
 671 0416 00       		.byte	0
 672 0417 15       		.uleb128 0x15
 673 0418 00000000 		.long	.LASF53
 674 041c 04       		.byte	0x4
 675 041d 0A       		.byte	0xa
 676 041e B5       		.byte	0xb5
 677 041f 3E040000 		.long	0x43e
 678 0423 16       		.uleb128 0x16
 679 0424 00000000 		.long	.LASF54
 680 0428 00       		.sleb128 0
 681 0429 16       		.uleb128 0x16
 682 042a 00000000 		.long	.LASF55
 683 042e 01       		.sleb128 1
 684 042f 16       		.uleb128 0x16
 685 0430 00000000 		.long	.LASF56
 686 0434 02       		.sleb128 2
 687 0435 16       		.uleb128 0x16
 688 0436 00000000 		.long	.LASF57
 689 043a 808004   		.sleb128 65536
 690 043d 00       		.byte	0
 691 043e 17       		.uleb128 0x17
 692 043f 00000000 		.long	.LASF234
 693 0443 9E060000 		.long	0x69e
 694 0447 18       		.uleb128 0x18
 695 0448 00000000 		.long	.LASF152
 696 044c 01       		.byte	0x1
GAS LISTING /tmp/ccLn5Wva.s 			page 15


 697 044d 0A       		.byte	0xa
 698 044e 1502     		.value	0x215
 699 0450 01       		.byte	0x1
 700 0451 9D040000 		.long	0x49d
 701 0455 19       		.uleb128 0x19
 702 0456 00000000 		.long	.LASF58
 703 045a 0A       		.byte	0xa
 704 045b 1D02     		.value	0x21d
 705 045d A4110000 		.long	0x11a4
 706 0461 19       		.uleb128 0x19
 707 0462 00000000 		.long	.LASF59
 708 0466 0A       		.byte	0xa
 709 0467 1E02     		.value	0x21e
 710 0469 3A100000 		.long	0x103a
 711 046d 1A       		.uleb128 0x1a
 712 046e 00000000 		.long	.LASF152
 713 0472 0A       		.byte	0xa
 714 0473 1902     		.value	0x219
 715 0475 01       		.byte	0x1
 716 0476 7E040000 		.long	0x47e
 717 047a 84040000 		.long	0x484
 718 047e 1B       		.uleb128 0x1b
 719 047f B9110000 		.long	0x11b9
 720 0483 00       		.byte	0
 721 0484 1C       		.uleb128 0x1c
 722 0485 00000000 		.long	.LASF85
 723 0489 0A       		.byte	0xa
 724 048a 1A02     		.value	0x21a
 725 048c 01       		.byte	0x1
 726 048d 91040000 		.long	0x491
 727 0491 1B       		.uleb128 0x1b
 728 0492 B9110000 		.long	0x11b9
 729 0496 1B       		.uleb128 0x1b
 730 0497 03010000 		.long	0x103
 731 049b 00       		.byte	0
 732 049c 00       		.byte	0
 733 049d 1D       		.uleb128 0x1d
 734 049e 00000000 		.long	.LASF75
 735 04a2 0A       		.byte	0xa
 736 04a3 FF       		.byte	0xff
 737 04a4 22030000 		.long	0x322
 738 04a8 01       		.byte	0x1
 739 04a9 1E       		.uleb128 0x1e
 740 04aa 00000000 		.long	.LASF60
 741 04ae 0A       		.byte	0xa
 742 04af 0201     		.value	0x102
 743 04b1 B7040000 		.long	0x4b7
 744 04b5 01       		.byte	0x1
 745 04b6 01       		.byte	0x1
 746 04b7 0F       		.uleb128 0xf
 747 04b8 9D040000 		.long	0x49d
 748 04bc 1F       		.uleb128 0x1f
 749 04bd 64656300 		.string	"dec"
 750 04c1 0A       		.byte	0xa
 751 04c2 0501     		.value	0x105
 752 04c4 B7040000 		.long	0x4b7
 753 04c8 01       		.byte	0x1
GAS LISTING /tmp/ccLn5Wva.s 			page 16


 754 04c9 02       		.byte	0x2
 755 04ca 1E       		.uleb128 0x1e
 756 04cb 00000000 		.long	.LASF61
 757 04cf 0A       		.byte	0xa
 758 04d0 0801     		.value	0x108
 759 04d2 B7040000 		.long	0x4b7
 760 04d6 01       		.byte	0x1
 761 04d7 04       		.byte	0x4
 762 04d8 1F       		.uleb128 0x1f
 763 04d9 68657800 		.string	"hex"
 764 04dd 0A       		.byte	0xa
 765 04de 0B01     		.value	0x10b
 766 04e0 B7040000 		.long	0x4b7
 767 04e4 01       		.byte	0x1
 768 04e5 08       		.byte	0x8
 769 04e6 1E       		.uleb128 0x1e
 770 04e7 00000000 		.long	.LASF62
 771 04eb 0A       		.byte	0xa
 772 04ec 1001     		.value	0x110
 773 04ee B7040000 		.long	0x4b7
 774 04f2 01       		.byte	0x1
 775 04f3 10       		.byte	0x10
 776 04f4 1E       		.uleb128 0x1e
 777 04f5 00000000 		.long	.LASF63
 778 04f9 0A       		.byte	0xa
 779 04fa 1401     		.value	0x114
 780 04fc B7040000 		.long	0x4b7
 781 0500 01       		.byte	0x1
 782 0501 20       		.byte	0x20
 783 0502 1F       		.uleb128 0x1f
 784 0503 6F637400 		.string	"oct"
 785 0507 0A       		.byte	0xa
 786 0508 1701     		.value	0x117
 787 050a B7040000 		.long	0x4b7
 788 050e 01       		.byte	0x1
 789 050f 40       		.byte	0x40
 790 0510 1E       		.uleb128 0x1e
 791 0511 00000000 		.long	.LASF64
 792 0515 0A       		.byte	0xa
 793 0516 1B01     		.value	0x11b
 794 0518 B7040000 		.long	0x4b7
 795 051c 01       		.byte	0x1
 796 051d 80       		.byte	0x80
 797 051e 20       		.uleb128 0x20
 798 051f 00000000 		.long	.LASF65
 799 0523 0A       		.byte	0xa
 800 0524 1E01     		.value	0x11e
 801 0526 B7040000 		.long	0x4b7
 802 052a 01       		.byte	0x1
 803 052b 0001     		.value	0x100
 804 052d 20       		.uleb128 0x20
 805 052e 00000000 		.long	.LASF66
 806 0532 0A       		.byte	0xa
 807 0533 2201     		.value	0x122
 808 0535 B7040000 		.long	0x4b7
 809 0539 01       		.byte	0x1
 810 053a 0002     		.value	0x200
GAS LISTING /tmp/ccLn5Wva.s 			page 17


 811 053c 20       		.uleb128 0x20
 812 053d 00000000 		.long	.LASF67
 813 0541 0A       		.byte	0xa
 814 0542 2601     		.value	0x126
 815 0544 B7040000 		.long	0x4b7
 816 0548 01       		.byte	0x1
 817 0549 0004     		.value	0x400
 818 054b 20       		.uleb128 0x20
 819 054c 00000000 		.long	.LASF68
 820 0550 0A       		.byte	0xa
 821 0551 2901     		.value	0x129
 822 0553 B7040000 		.long	0x4b7
 823 0557 01       		.byte	0x1
 824 0558 0008     		.value	0x800
 825 055a 20       		.uleb128 0x20
 826 055b 00000000 		.long	.LASF69
 827 055f 0A       		.byte	0xa
 828 0560 2C01     		.value	0x12c
 829 0562 B7040000 		.long	0x4b7
 830 0566 01       		.byte	0x1
 831 0567 0010     		.value	0x1000
 832 0569 20       		.uleb128 0x20
 833 056a 00000000 		.long	.LASF70
 834 056e 0A       		.byte	0xa
 835 056f 2F01     		.value	0x12f
 836 0571 B7040000 		.long	0x4b7
 837 0575 01       		.byte	0x1
 838 0576 0020     		.value	0x2000
 839 0578 20       		.uleb128 0x20
 840 0579 00000000 		.long	.LASF71
 841 057d 0A       		.byte	0xa
 842 057e 3301     		.value	0x133
 843 0580 B7040000 		.long	0x4b7
 844 0584 01       		.byte	0x1
 845 0585 0040     		.value	0x4000
 846 0587 1E       		.uleb128 0x1e
 847 0588 00000000 		.long	.LASF72
 848 058c 0A       		.byte	0xa
 849 058d 3601     		.value	0x136
 850 058f B7040000 		.long	0x4b7
 851 0593 01       		.byte	0x1
 852 0594 B0       		.byte	0xb0
 853 0595 1E       		.uleb128 0x1e
 854 0596 00000000 		.long	.LASF73
 855 059a 0A       		.byte	0xa
 856 059b 3901     		.value	0x139
 857 059d B7040000 		.long	0x4b7
 858 05a1 01       		.byte	0x1
 859 05a2 4A       		.byte	0x4a
 860 05a3 20       		.uleb128 0x20
 861 05a4 00000000 		.long	.LASF74
 862 05a8 0A       		.byte	0xa
 863 05a9 3C01     		.value	0x13c
 864 05ab B7040000 		.long	0x4b7
 865 05af 01       		.byte	0x1
 866 05b0 0401     		.value	0x104
 867 05b2 21       		.uleb128 0x21
GAS LISTING /tmp/ccLn5Wva.s 			page 18


 868 05b3 00000000 		.long	.LASF76
 869 05b7 0A       		.byte	0xa
 870 05b8 4A01     		.value	0x14a
 871 05ba EA030000 		.long	0x3ea
 872 05be 01       		.byte	0x1
 873 05bf 1E       		.uleb128 0x1e
 874 05c0 00000000 		.long	.LASF77
 875 05c4 0A       		.byte	0xa
 876 05c5 4E01     		.value	0x14e
 877 05c7 CD050000 		.long	0x5cd
 878 05cb 01       		.byte	0x1
 879 05cc 01       		.byte	0x1
 880 05cd 0F       		.uleb128 0xf
 881 05ce B2050000 		.long	0x5b2
 882 05d2 1E       		.uleb128 0x1e
 883 05d3 00000000 		.long	.LASF78
 884 05d7 0A       		.byte	0xa
 885 05d8 5101     		.value	0x151
 886 05da CD050000 		.long	0x5cd
 887 05de 01       		.byte	0x1
 888 05df 02       		.byte	0x2
 889 05e0 1E       		.uleb128 0x1e
 890 05e1 00000000 		.long	.LASF79
 891 05e5 0A       		.byte	0xa
 892 05e6 5601     		.value	0x156
 893 05e8 CD050000 		.long	0x5cd
 894 05ec 01       		.byte	0x1
 895 05ed 04       		.byte	0x4
 896 05ee 1E       		.uleb128 0x1e
 897 05ef 00000000 		.long	.LASF80
 898 05f3 0A       		.byte	0xa
 899 05f4 5901     		.value	0x159
 900 05f6 CD050000 		.long	0x5cd
 901 05fa 01       		.byte	0x1
 902 05fb 00       		.byte	0
 903 05fc 21       		.uleb128 0x21
 904 05fd 00000000 		.long	.LASF81
 905 0601 0A       		.byte	0xa
 906 0602 6901     		.value	0x169
 907 0604 B1030000 		.long	0x3b1
 908 0608 01       		.byte	0x1
 909 0609 1F       		.uleb128 0x1f
 910 060a 61707000 		.string	"app"
 911 060e 0A       		.byte	0xa
 912 060f 6C01     		.value	0x16c
 913 0611 17060000 		.long	0x617
 914 0615 01       		.byte	0x1
 915 0616 01       		.byte	0x1
 916 0617 0F       		.uleb128 0xf
 917 0618 FC050000 		.long	0x5fc
 918 061c 1F       		.uleb128 0x1f
 919 061d 61746500 		.string	"ate"
 920 0621 0A       		.byte	0xa
 921 0622 6F01     		.value	0x16f
 922 0624 17060000 		.long	0x617
 923 0628 01       		.byte	0x1
 924 0629 02       		.byte	0x2
GAS LISTING /tmp/ccLn5Wva.s 			page 19


 925 062a 1E       		.uleb128 0x1e
 926 062b 00000000 		.long	.LASF82
 927 062f 0A       		.byte	0xa
 928 0630 7401     		.value	0x174
 929 0632 17060000 		.long	0x617
 930 0636 01       		.byte	0x1
 931 0637 04       		.byte	0x4
 932 0638 1F       		.uleb128 0x1f
 933 0639 696E00   		.string	"in"
 934 063c 0A       		.byte	0xa
 935 063d 7701     		.value	0x177
 936 063f 17060000 		.long	0x617
 937 0643 01       		.byte	0x1
 938 0644 08       		.byte	0x8
 939 0645 1F       		.uleb128 0x1f
 940 0646 6F757400 		.string	"out"
 941 064a 0A       		.byte	0xa
 942 064b 7A01     		.value	0x17a
 943 064d 17060000 		.long	0x617
 944 0651 01       		.byte	0x1
 945 0652 10       		.byte	0x10
 946 0653 1E       		.uleb128 0x1e
 947 0654 00000000 		.long	.LASF83
 948 0658 0A       		.byte	0xa
 949 0659 7D01     		.value	0x17d
 950 065b 17060000 		.long	0x617
 951 065f 01       		.byte	0x1
 952 0660 20       		.byte	0x20
 953 0661 21       		.uleb128 0x21
 954 0662 00000000 		.long	.LASF84
 955 0666 0A       		.byte	0xa
 956 0667 8901     		.value	0x189
 957 0669 17040000 		.long	0x417
 958 066d 01       		.byte	0x1
 959 066e 1F       		.uleb128 0x1f
 960 066f 62656700 		.string	"beg"
 961 0673 0A       		.byte	0xa
 962 0674 8C01     		.value	0x18c
 963 0676 7C060000 		.long	0x67c
 964 067a 01       		.byte	0x1
 965 067b 00       		.byte	0
 966 067c 0F       		.uleb128 0xf
 967 067d 61060000 		.long	0x661
 968 0681 1F       		.uleb128 0x1f
 969 0682 63757200 		.string	"cur"
 970 0686 0A       		.byte	0xa
 971 0687 8F01     		.value	0x18f
 972 0689 7C060000 		.long	0x67c
 973 068d 01       		.byte	0x1
 974 068e 01       		.byte	0x1
 975 068f 1F       		.uleb128 0x1f
 976 0690 656E6400 		.string	"end"
 977 0694 0A       		.byte	0xa
 978 0695 9201     		.value	0x192
 979 0697 7C060000 		.long	0x67c
 980 069b 01       		.byte	0x1
 981 069c 02       		.byte	0x2
GAS LISTING /tmp/ccLn5Wva.s 			page 20


 982 069d 00       		.byte	0
 983 069e 12       		.uleb128 0x12
 984 069f 0B       		.byte	0xb
 985 06a0 52       		.byte	0x52
 986 06a1 CA110000 		.long	0x11ca
 987 06a5 12       		.uleb128 0x12
 988 06a6 0B       		.byte	0xb
 989 06a7 53       		.byte	0x53
 990 06a8 BF110000 		.long	0x11bf
 991 06ac 12       		.uleb128 0x12
 992 06ad 0B       		.byte	0xb
 993 06ae 54       		.byte	0x54
 994 06af 9C000000 		.long	0x9c
 995 06b3 12       		.uleb128 0x12
 996 06b4 0B       		.byte	0xb
 997 06b5 5C       		.byte	0x5c
 998 06b6 E0110000 		.long	0x11e0
 999 06ba 12       		.uleb128 0x12
 1000 06bb 0B       		.byte	0xb
 1001 06bc 65       		.byte	0x65
 1002 06bd FA110000 		.long	0x11fa
 1003 06c1 12       		.uleb128 0x12
 1004 06c2 0B       		.byte	0xb
 1005 06c3 68       		.byte	0x68
 1006 06c4 14120000 		.long	0x1214
 1007 06c8 12       		.uleb128 0x12
 1008 06c9 0B       		.byte	0xb
 1009 06ca 69       		.byte	0x69
 1010 06cb 29120000 		.long	0x1229
 1011 06cf 22       		.uleb128 0x22
 1012 06d0 00000000 		.long	.LASF217
 1013 06d4 02       		.byte	0x2
 1014 06d5 4A       		.byte	0x4a
 1015 06d6 47040000 		.long	0x447
 1016 06da 00       		.byte	0
 1017 06db 23       		.uleb128 0x23
 1018 06dc 00000000 		.long	.LASF86
 1019 06e0 06       		.byte	0x6
 1020 06e1 6101     		.value	0x161
 1021 06e3 9C000000 		.long	0x9c
 1022 06e7 F1060000 		.long	0x6f1
 1023 06eb 24       		.uleb128 0x24
 1024 06ec 03010000 		.long	0x103
 1025 06f0 00       		.byte	0
 1026 06f1 23       		.uleb128 0x23
 1027 06f2 00000000 		.long	.LASF87
 1028 06f6 06       		.byte	0x6
 1029 06f7 E902     		.value	0x2e9
 1030 06f9 9C000000 		.long	0x9c
 1031 06fd 07070000 		.long	0x707
 1032 0701 24       		.uleb128 0x24
 1033 0702 07070000 		.long	0x707
 1034 0706 00       		.byte	0
 1035 0707 10       		.uleb128 0x10
 1036 0708 08       		.byte	0x8
 1037 0709 32000000 		.long	0x32
 1038 070d 23       		.uleb128 0x23
GAS LISTING /tmp/ccLn5Wva.s 			page 21


 1039 070e 00000000 		.long	.LASF88
 1040 0712 06       		.byte	0x6
 1041 0713 0603     		.value	0x306
 1042 0715 2D070000 		.long	0x72d
 1043 0719 2D070000 		.long	0x72d
 1044 071d 24       		.uleb128 0x24
 1045 071e 2D070000 		.long	0x72d
 1046 0722 24       		.uleb128 0x24
 1047 0723 03010000 		.long	0x103
 1048 0727 24       		.uleb128 0x24
 1049 0728 07070000 		.long	0x707
 1050 072c 00       		.byte	0
 1051 072d 10       		.uleb128 0x10
 1052 072e 08       		.byte	0x8
 1053 072f 33070000 		.long	0x733
 1054 0733 04       		.uleb128 0x4
 1055 0734 04       		.byte	0x4
 1056 0735 05       		.byte	0x5
 1057 0736 00000000 		.long	.LASF89
 1058 073a 23       		.uleb128 0x23
 1059 073b 00000000 		.long	.LASF90
 1060 073f 06       		.byte	0x6
 1061 0740 F702     		.value	0x2f7
 1062 0742 9C000000 		.long	0x9c
 1063 0746 55070000 		.long	0x755
 1064 074a 24       		.uleb128 0x24
 1065 074b 33070000 		.long	0x733
 1066 074f 24       		.uleb128 0x24
 1067 0750 07070000 		.long	0x707
 1068 0754 00       		.byte	0
 1069 0755 23       		.uleb128 0x23
 1070 0756 00000000 		.long	.LASF91
 1071 075a 06       		.byte	0x6
 1072 075b 0D03     		.value	0x30d
 1073 075d 03010000 		.long	0x103
 1074 0761 70070000 		.long	0x770
 1075 0765 24       		.uleb128 0x24
 1076 0766 70070000 		.long	0x770
 1077 076a 24       		.uleb128 0x24
 1078 076b 07070000 		.long	0x707
 1079 076f 00       		.byte	0
 1080 0770 10       		.uleb128 0x10
 1081 0771 08       		.byte	0x8
 1082 0772 76070000 		.long	0x776
 1083 0776 0F       		.uleb128 0xf
 1084 0777 33070000 		.long	0x733
 1085 077b 23       		.uleb128 0x23
 1086 077c 00000000 		.long	.LASF92
 1087 0780 06       		.byte	0x6
 1088 0781 4B02     		.value	0x24b
 1089 0783 03010000 		.long	0x103
 1090 0787 96070000 		.long	0x796
 1091 078b 24       		.uleb128 0x24
 1092 078c 07070000 		.long	0x707
 1093 0790 24       		.uleb128 0x24
 1094 0791 03010000 		.long	0x103
 1095 0795 00       		.byte	0
GAS LISTING /tmp/ccLn5Wva.s 			page 22


 1096 0796 23       		.uleb128 0x23
 1097 0797 00000000 		.long	.LASF93
 1098 079b 06       		.byte	0x6
 1099 079c 5202     		.value	0x252
 1100 079e 03010000 		.long	0x103
 1101 07a2 B2070000 		.long	0x7b2
 1102 07a6 24       		.uleb128 0x24
 1103 07a7 07070000 		.long	0x707
 1104 07ab 24       		.uleb128 0x24
 1105 07ac 70070000 		.long	0x770
 1106 07b0 25       		.uleb128 0x25
 1107 07b1 00       		.byte	0
 1108 07b2 23       		.uleb128 0x23
 1109 07b3 00000000 		.long	.LASF94
 1110 07b7 06       		.byte	0x6
 1111 07b8 7B02     		.value	0x27b
 1112 07ba 03010000 		.long	0x103
 1113 07be CE070000 		.long	0x7ce
 1114 07c2 24       		.uleb128 0x24
 1115 07c3 07070000 		.long	0x707
 1116 07c7 24       		.uleb128 0x24
 1117 07c8 70070000 		.long	0x770
 1118 07cc 25       		.uleb128 0x25
 1119 07cd 00       		.byte	0
 1120 07ce 23       		.uleb128 0x23
 1121 07cf 00000000 		.long	.LASF95
 1122 07d3 06       		.byte	0x6
 1123 07d4 EA02     		.value	0x2ea
 1124 07d6 9C000000 		.long	0x9c
 1125 07da E4070000 		.long	0x7e4
 1126 07de 24       		.uleb128 0x24
 1127 07df 07070000 		.long	0x707
 1128 07e3 00       		.byte	0
 1129 07e4 26       		.uleb128 0x26
 1130 07e5 00000000 		.long	.LASF207
 1131 07e9 06       		.byte	0x6
 1132 07ea F002     		.value	0x2f0
 1133 07ec 9C000000 		.long	0x9c
 1134 07f0 23       		.uleb128 0x23
 1135 07f1 00000000 		.long	.LASF96
 1136 07f5 06       		.byte	0x6
 1137 07f6 7801     		.value	0x178
 1138 07f8 8A000000 		.long	0x8a
 1139 07fc 10080000 		.long	0x810
 1140 0800 24       		.uleb128 0x24
 1141 0801 2C010000 		.long	0x12c
 1142 0805 24       		.uleb128 0x24
 1143 0806 8A000000 		.long	0x8a
 1144 080a 24       		.uleb128 0x24
 1145 080b 10080000 		.long	0x810
 1146 080f 00       		.byte	0
 1147 0810 10       		.uleb128 0x10
 1148 0811 08       		.byte	0x8
 1149 0812 15010000 		.long	0x115
 1150 0816 23       		.uleb128 0x23
 1151 0817 00000000 		.long	.LASF97
 1152 081b 06       		.byte	0x6
GAS LISTING /tmp/ccLn5Wva.s 			page 23


 1153 081c 6D01     		.value	0x16d
 1154 081e 8A000000 		.long	0x8a
 1155 0822 3B080000 		.long	0x83b
 1156 0826 24       		.uleb128 0x24
 1157 0827 2D070000 		.long	0x72d
 1158 082b 24       		.uleb128 0x24
 1159 082c 2C010000 		.long	0x12c
 1160 0830 24       		.uleb128 0x24
 1161 0831 8A000000 		.long	0x8a
 1162 0835 24       		.uleb128 0x24
 1163 0836 10080000 		.long	0x810
 1164 083a 00       		.byte	0
 1165 083b 23       		.uleb128 0x23
 1166 083c 00000000 		.long	.LASF98
 1167 0840 06       		.byte	0x6
 1168 0841 6901     		.value	0x169
 1169 0843 03010000 		.long	0x103
 1170 0847 51080000 		.long	0x851
 1171 084b 24       		.uleb128 0x24
 1172 084c 51080000 		.long	0x851
 1173 0850 00       		.byte	0
 1174 0851 10       		.uleb128 0x10
 1175 0852 08       		.byte	0x8
 1176 0853 57080000 		.long	0x857
 1177 0857 0F       		.uleb128 0xf
 1178 0858 15010000 		.long	0x115
 1179 085c 23       		.uleb128 0x23
 1180 085d 00000000 		.long	.LASF99
 1181 0861 06       		.byte	0x6
 1182 0862 9801     		.value	0x198
 1183 0864 8A000000 		.long	0x8a
 1184 0868 81080000 		.long	0x881
 1185 086c 24       		.uleb128 0x24
 1186 086d 2D070000 		.long	0x72d
 1187 0871 24       		.uleb128 0x24
 1188 0872 81080000 		.long	0x881
 1189 0876 24       		.uleb128 0x24
 1190 0877 8A000000 		.long	0x8a
 1191 087b 24       		.uleb128 0x24
 1192 087c 10080000 		.long	0x810
 1193 0880 00       		.byte	0
 1194 0881 10       		.uleb128 0x10
 1195 0882 08       		.byte	0x8
 1196 0883 2C010000 		.long	0x12c
 1197 0887 23       		.uleb128 0x23
 1198 0888 00000000 		.long	.LASF100
 1199 088c 06       		.byte	0x6
 1200 088d F802     		.value	0x2f8
 1201 088f 9C000000 		.long	0x9c
 1202 0893 A2080000 		.long	0x8a2
 1203 0897 24       		.uleb128 0x24
 1204 0898 33070000 		.long	0x733
 1205 089c 24       		.uleb128 0x24
 1206 089d 07070000 		.long	0x707
 1207 08a1 00       		.byte	0
 1208 08a2 23       		.uleb128 0x23
 1209 08a3 00000000 		.long	.LASF101
GAS LISTING /tmp/ccLn5Wva.s 			page 24


 1210 08a7 06       		.byte	0x6
 1211 08a8 FE02     		.value	0x2fe
 1212 08aa 9C000000 		.long	0x9c
 1213 08ae B8080000 		.long	0x8b8
 1214 08b2 24       		.uleb128 0x24
 1215 08b3 33070000 		.long	0x733
 1216 08b7 00       		.byte	0
 1217 08b8 23       		.uleb128 0x23
 1218 08b9 00000000 		.long	.LASF102
 1219 08bd 06       		.byte	0x6
 1220 08be 5C02     		.value	0x25c
 1221 08c0 03010000 		.long	0x103
 1222 08c4 D9080000 		.long	0x8d9
 1223 08c8 24       		.uleb128 0x24
 1224 08c9 2D070000 		.long	0x72d
 1225 08cd 24       		.uleb128 0x24
 1226 08ce 8A000000 		.long	0x8a
 1227 08d2 24       		.uleb128 0x24
 1228 08d3 70070000 		.long	0x770
 1229 08d7 25       		.uleb128 0x25
 1230 08d8 00       		.byte	0
 1231 08d9 23       		.uleb128 0x23
 1232 08da 00000000 		.long	.LASF103
 1233 08de 06       		.byte	0x6
 1234 08df 8502     		.value	0x285
 1235 08e1 03010000 		.long	0x103
 1236 08e5 F5080000 		.long	0x8f5
 1237 08e9 24       		.uleb128 0x24
 1238 08ea 70070000 		.long	0x770
 1239 08ee 24       		.uleb128 0x24
 1240 08ef 70070000 		.long	0x770
 1241 08f3 25       		.uleb128 0x25
 1242 08f4 00       		.byte	0
 1243 08f5 23       		.uleb128 0x23
 1244 08f6 00000000 		.long	.LASF104
 1245 08fa 06       		.byte	0x6
 1246 08fb 1503     		.value	0x315
 1247 08fd 9C000000 		.long	0x9c
 1248 0901 10090000 		.long	0x910
 1249 0905 24       		.uleb128 0x24
 1250 0906 9C000000 		.long	0x9c
 1251 090a 24       		.uleb128 0x24
 1252 090b 07070000 		.long	0x707
 1253 090f 00       		.byte	0
 1254 0910 23       		.uleb128 0x23
 1255 0911 00000000 		.long	.LASF105
 1256 0915 06       		.byte	0x6
 1257 0916 6402     		.value	0x264
 1258 0918 03010000 		.long	0x103
 1259 091c 30090000 		.long	0x930
 1260 0920 24       		.uleb128 0x24
 1261 0921 07070000 		.long	0x707
 1262 0925 24       		.uleb128 0x24
 1263 0926 70070000 		.long	0x770
 1264 092a 24       		.uleb128 0x24
 1265 092b 30090000 		.long	0x930
 1266 092f 00       		.byte	0
GAS LISTING /tmp/ccLn5Wva.s 			page 25


 1267 0930 10       		.uleb128 0x10
 1268 0931 08       		.byte	0x8
 1269 0932 44000000 		.long	0x44
 1270 0936 23       		.uleb128 0x23
 1271 0937 00000000 		.long	.LASF106
 1272 093b 06       		.byte	0x6
 1273 093c B102     		.value	0x2b1
 1274 093e 03010000 		.long	0x103
 1275 0942 56090000 		.long	0x956
 1276 0946 24       		.uleb128 0x24
 1277 0947 07070000 		.long	0x707
 1278 094b 24       		.uleb128 0x24
 1279 094c 70070000 		.long	0x770
 1280 0950 24       		.uleb128 0x24
 1281 0951 30090000 		.long	0x930
 1282 0955 00       		.byte	0
 1283 0956 23       		.uleb128 0x23
 1284 0957 00000000 		.long	.LASF107
 1285 095b 06       		.byte	0x6
 1286 095c 7102     		.value	0x271
 1287 095e 03010000 		.long	0x103
 1288 0962 7B090000 		.long	0x97b
 1289 0966 24       		.uleb128 0x24
 1290 0967 2D070000 		.long	0x72d
 1291 096b 24       		.uleb128 0x24
 1292 096c 8A000000 		.long	0x8a
 1293 0970 24       		.uleb128 0x24
 1294 0971 70070000 		.long	0x770
 1295 0975 24       		.uleb128 0x24
 1296 0976 30090000 		.long	0x930
 1297 097a 00       		.byte	0
 1298 097b 23       		.uleb128 0x23
 1299 097c 00000000 		.long	.LASF108
 1300 0980 06       		.byte	0x6
 1301 0981 BD02     		.value	0x2bd
 1302 0983 03010000 		.long	0x103
 1303 0987 9B090000 		.long	0x99b
 1304 098b 24       		.uleb128 0x24
 1305 098c 70070000 		.long	0x770
 1306 0990 24       		.uleb128 0x24
 1307 0991 70070000 		.long	0x770
 1308 0995 24       		.uleb128 0x24
 1309 0996 30090000 		.long	0x930
 1310 099a 00       		.byte	0
 1311 099b 23       		.uleb128 0x23
 1312 099c 00000000 		.long	.LASF109
 1313 09a0 06       		.byte	0x6
 1314 09a1 6C02     		.value	0x26c
 1315 09a3 03010000 		.long	0x103
 1316 09a7 B6090000 		.long	0x9b6
 1317 09ab 24       		.uleb128 0x24
 1318 09ac 70070000 		.long	0x770
 1319 09b0 24       		.uleb128 0x24
 1320 09b1 30090000 		.long	0x930
 1321 09b5 00       		.byte	0
 1322 09b6 23       		.uleb128 0x23
 1323 09b7 00000000 		.long	.LASF110
GAS LISTING /tmp/ccLn5Wva.s 			page 26


 1324 09bb 06       		.byte	0x6
 1325 09bc B902     		.value	0x2b9
 1326 09be 03010000 		.long	0x103
 1327 09c2 D1090000 		.long	0x9d1
 1328 09c6 24       		.uleb128 0x24
 1329 09c7 70070000 		.long	0x770
 1330 09cb 24       		.uleb128 0x24
 1331 09cc 30090000 		.long	0x930
 1332 09d0 00       		.byte	0
 1333 09d1 23       		.uleb128 0x23
 1334 09d2 00000000 		.long	.LASF111
 1335 09d6 06       		.byte	0x6
 1336 09d7 7201     		.value	0x172
 1337 09d9 8A000000 		.long	0x8a
 1338 09dd F1090000 		.long	0x9f1
 1339 09e1 24       		.uleb128 0x24
 1340 09e2 F1090000 		.long	0x9f1
 1341 09e6 24       		.uleb128 0x24
 1342 09e7 33070000 		.long	0x733
 1343 09eb 24       		.uleb128 0x24
 1344 09ec 10080000 		.long	0x810
 1345 09f0 00       		.byte	0
 1346 09f1 10       		.uleb128 0x10
 1347 09f2 08       		.byte	0x8
 1348 09f3 FC000000 		.long	0xfc
 1349 09f7 27       		.uleb128 0x27
 1350 09f8 00000000 		.long	.LASF112
 1351 09fc 06       		.byte	0x6
 1352 09fd 9B       		.byte	0x9b
 1353 09fe 2D070000 		.long	0x72d
 1354 0a02 110A0000 		.long	0xa11
 1355 0a06 24       		.uleb128 0x24
 1356 0a07 2D070000 		.long	0x72d
 1357 0a0b 24       		.uleb128 0x24
 1358 0a0c 70070000 		.long	0x770
 1359 0a10 00       		.byte	0
 1360 0a11 27       		.uleb128 0x27
 1361 0a12 00000000 		.long	.LASF113
 1362 0a16 06       		.byte	0x6
 1363 0a17 A3       		.byte	0xa3
 1364 0a18 03010000 		.long	0x103
 1365 0a1c 2B0A0000 		.long	0xa2b
 1366 0a20 24       		.uleb128 0x24
 1367 0a21 70070000 		.long	0x770
 1368 0a25 24       		.uleb128 0x24
 1369 0a26 70070000 		.long	0x770
 1370 0a2a 00       		.byte	0
 1371 0a2b 27       		.uleb128 0x27
 1372 0a2c 00000000 		.long	.LASF114
 1373 0a30 06       		.byte	0x6
 1374 0a31 C0       		.byte	0xc0
 1375 0a32 03010000 		.long	0x103
 1376 0a36 450A0000 		.long	0xa45
 1377 0a3a 24       		.uleb128 0x24
 1378 0a3b 70070000 		.long	0x770
 1379 0a3f 24       		.uleb128 0x24
 1380 0a40 70070000 		.long	0x770
GAS LISTING /tmp/ccLn5Wva.s 			page 27


 1381 0a44 00       		.byte	0
 1382 0a45 27       		.uleb128 0x27
 1383 0a46 00000000 		.long	.LASF115
 1384 0a4a 06       		.byte	0x6
 1385 0a4b 93       		.byte	0x93
 1386 0a4c 2D070000 		.long	0x72d
 1387 0a50 5F0A0000 		.long	0xa5f
 1388 0a54 24       		.uleb128 0x24
 1389 0a55 2D070000 		.long	0x72d
 1390 0a59 24       		.uleb128 0x24
 1391 0a5a 70070000 		.long	0x770
 1392 0a5e 00       		.byte	0
 1393 0a5f 27       		.uleb128 0x27
 1394 0a60 00000000 		.long	.LASF116
 1395 0a64 06       		.byte	0x6
 1396 0a65 FC       		.byte	0xfc
 1397 0a66 8A000000 		.long	0x8a
 1398 0a6a 790A0000 		.long	0xa79
 1399 0a6e 24       		.uleb128 0x24
 1400 0a6f 70070000 		.long	0x770
 1401 0a73 24       		.uleb128 0x24
 1402 0a74 70070000 		.long	0x770
 1403 0a78 00       		.byte	0
 1404 0a79 23       		.uleb128 0x23
 1405 0a7a 00000000 		.long	.LASF117
 1406 0a7e 06       		.byte	0x6
 1407 0a7f 5703     		.value	0x357
 1408 0a81 8A000000 		.long	0x8a
 1409 0a85 9E0A0000 		.long	0xa9e
 1410 0a89 24       		.uleb128 0x24
 1411 0a8a 2D070000 		.long	0x72d
 1412 0a8e 24       		.uleb128 0x24
 1413 0a8f 8A000000 		.long	0x8a
 1414 0a93 24       		.uleb128 0x24
 1415 0a94 70070000 		.long	0x770
 1416 0a98 24       		.uleb128 0x24
 1417 0a99 9E0A0000 		.long	0xa9e
 1418 0a9d 00       		.byte	0
 1419 0a9e 10       		.uleb128 0x10
 1420 0a9f 08       		.byte	0x8
 1421 0aa0 A40A0000 		.long	0xaa4
 1422 0aa4 0F       		.uleb128 0xf
 1423 0aa5 A90A0000 		.long	0xaa9
 1424 0aa9 28       		.uleb128 0x28
 1425 0aaa 746D00   		.string	"tm"
 1426 0aad 38       		.byte	0x38
 1427 0aae 0C       		.byte	0xc
 1428 0aaf 85       		.byte	0x85
 1429 0ab0 390B0000 		.long	0xb39
 1430 0ab4 06       		.uleb128 0x6
 1431 0ab5 00000000 		.long	.LASF119
 1432 0ab9 0C       		.byte	0xc
 1433 0aba 87       		.byte	0x87
 1434 0abb 03010000 		.long	0x103
 1435 0abf 00       		.byte	0
 1436 0ac0 06       		.uleb128 0x6
 1437 0ac1 00000000 		.long	.LASF120
GAS LISTING /tmp/ccLn5Wva.s 			page 28


 1438 0ac5 0C       		.byte	0xc
 1439 0ac6 88       		.byte	0x88
 1440 0ac7 03010000 		.long	0x103
 1441 0acb 04       		.byte	0x4
 1442 0acc 06       		.uleb128 0x6
 1443 0acd 00000000 		.long	.LASF121
 1444 0ad1 0C       		.byte	0xc
 1445 0ad2 89       		.byte	0x89
 1446 0ad3 03010000 		.long	0x103
 1447 0ad7 08       		.byte	0x8
 1448 0ad8 06       		.uleb128 0x6
 1449 0ad9 00000000 		.long	.LASF122
 1450 0add 0C       		.byte	0xc
 1451 0ade 8A       		.byte	0x8a
 1452 0adf 03010000 		.long	0x103
 1453 0ae3 0C       		.byte	0xc
 1454 0ae4 06       		.uleb128 0x6
 1455 0ae5 00000000 		.long	.LASF123
 1456 0ae9 0C       		.byte	0xc
 1457 0aea 8B       		.byte	0x8b
 1458 0aeb 03010000 		.long	0x103
 1459 0aef 10       		.byte	0x10
 1460 0af0 06       		.uleb128 0x6
 1461 0af1 00000000 		.long	.LASF124
 1462 0af5 0C       		.byte	0xc
 1463 0af6 8C       		.byte	0x8c
 1464 0af7 03010000 		.long	0x103
 1465 0afb 14       		.byte	0x14
 1466 0afc 06       		.uleb128 0x6
 1467 0afd 00000000 		.long	.LASF125
 1468 0b01 0C       		.byte	0xc
 1469 0b02 8D       		.byte	0x8d
 1470 0b03 03010000 		.long	0x103
 1471 0b07 18       		.byte	0x18
 1472 0b08 06       		.uleb128 0x6
 1473 0b09 00000000 		.long	.LASF126
 1474 0b0d 0C       		.byte	0xc
 1475 0b0e 8E       		.byte	0x8e
 1476 0b0f 03010000 		.long	0x103
 1477 0b13 1C       		.byte	0x1c
 1478 0b14 06       		.uleb128 0x6
 1479 0b15 00000000 		.long	.LASF127
 1480 0b19 0C       		.byte	0xc
 1481 0b1a 8F       		.byte	0x8f
 1482 0b1b 03010000 		.long	0x103
 1483 0b1f 20       		.byte	0x20
 1484 0b20 06       		.uleb128 0x6
 1485 0b21 00000000 		.long	.LASF128
 1486 0b25 0C       		.byte	0xc
 1487 0b26 92       		.byte	0x92
 1488 0b27 7C0C0000 		.long	0xc7c
 1489 0b2b 28       		.byte	0x28
 1490 0b2c 06       		.uleb128 0x6
 1491 0b2d 00000000 		.long	.LASF129
 1492 0b31 0C       		.byte	0xc
 1493 0b32 93       		.byte	0x93
 1494 0b33 2C010000 		.long	0x12c
GAS LISTING /tmp/ccLn5Wva.s 			page 29


 1495 0b37 30       		.byte	0x30
 1496 0b38 00       		.byte	0
 1497 0b39 23       		.uleb128 0x23
 1498 0b3a 00000000 		.long	.LASF130
 1499 0b3e 06       		.byte	0x6
 1500 0b3f 1F01     		.value	0x11f
 1501 0b41 8A000000 		.long	0x8a
 1502 0b45 4F0B0000 		.long	0xb4f
 1503 0b49 24       		.uleb128 0x24
 1504 0b4a 70070000 		.long	0x770
 1505 0b4e 00       		.byte	0
 1506 0b4f 27       		.uleb128 0x27
 1507 0b50 00000000 		.long	.LASF131
 1508 0b54 06       		.byte	0x6
 1509 0b55 9E       		.byte	0x9e
 1510 0b56 2D070000 		.long	0x72d
 1511 0b5a 6E0B0000 		.long	0xb6e
 1512 0b5e 24       		.uleb128 0x24
 1513 0b5f 2D070000 		.long	0x72d
 1514 0b63 24       		.uleb128 0x24
 1515 0b64 70070000 		.long	0x770
 1516 0b68 24       		.uleb128 0x24
 1517 0b69 8A000000 		.long	0x8a
 1518 0b6d 00       		.byte	0
 1519 0b6e 27       		.uleb128 0x27
 1520 0b6f 00000000 		.long	.LASF132
 1521 0b73 06       		.byte	0x6
 1522 0b74 A6       		.byte	0xa6
 1523 0b75 03010000 		.long	0x103
 1524 0b79 8D0B0000 		.long	0xb8d
 1525 0b7d 24       		.uleb128 0x24
 1526 0b7e 70070000 		.long	0x770
 1527 0b82 24       		.uleb128 0x24
 1528 0b83 70070000 		.long	0x770
 1529 0b87 24       		.uleb128 0x24
 1530 0b88 8A000000 		.long	0x8a
 1531 0b8c 00       		.byte	0
 1532 0b8d 27       		.uleb128 0x27
 1533 0b8e 00000000 		.long	.LASF133
 1534 0b92 06       		.byte	0x6
 1535 0b93 96       		.byte	0x96
 1536 0b94 2D070000 		.long	0x72d
 1537 0b98 AC0B0000 		.long	0xbac
 1538 0b9c 24       		.uleb128 0x24
 1539 0b9d 2D070000 		.long	0x72d
 1540 0ba1 24       		.uleb128 0x24
 1541 0ba2 70070000 		.long	0x770
 1542 0ba6 24       		.uleb128 0x24
 1543 0ba7 8A000000 		.long	0x8a
 1544 0bab 00       		.byte	0
 1545 0bac 23       		.uleb128 0x23
 1546 0bad 00000000 		.long	.LASF134
 1547 0bb1 06       		.byte	0x6
 1548 0bb2 9E01     		.value	0x19e
 1549 0bb4 8A000000 		.long	0x8a
 1550 0bb8 D10B0000 		.long	0xbd1
 1551 0bbc 24       		.uleb128 0x24
GAS LISTING /tmp/ccLn5Wva.s 			page 30


 1552 0bbd F1090000 		.long	0x9f1
 1553 0bc1 24       		.uleb128 0x24
 1554 0bc2 D10B0000 		.long	0xbd1
 1555 0bc6 24       		.uleb128 0x24
 1556 0bc7 8A000000 		.long	0x8a
 1557 0bcb 24       		.uleb128 0x24
 1558 0bcc 10080000 		.long	0x810
 1559 0bd0 00       		.byte	0
 1560 0bd1 10       		.uleb128 0x10
 1561 0bd2 08       		.byte	0x8
 1562 0bd3 70070000 		.long	0x770
 1563 0bd7 23       		.uleb128 0x23
 1564 0bd8 00000000 		.long	.LASF135
 1565 0bdc 06       		.byte	0x6
 1566 0bdd 0001     		.value	0x100
 1567 0bdf 8A000000 		.long	0x8a
 1568 0be3 F20B0000 		.long	0xbf2
 1569 0be7 24       		.uleb128 0x24
 1570 0be8 70070000 		.long	0x770
 1571 0bec 24       		.uleb128 0x24
 1572 0bed 70070000 		.long	0x770
 1573 0bf1 00       		.byte	0
 1574 0bf2 23       		.uleb128 0x23
 1575 0bf3 00000000 		.long	.LASF136
 1576 0bf7 06       		.byte	0x6
 1577 0bf8 C201     		.value	0x1c2
 1578 0bfa 0D0C0000 		.long	0xc0d
 1579 0bfe 0D0C0000 		.long	0xc0d
 1580 0c02 24       		.uleb128 0x24
 1581 0c03 70070000 		.long	0x770
 1582 0c07 24       		.uleb128 0x24
 1583 0c08 140C0000 		.long	0xc14
 1584 0c0c 00       		.byte	0
 1585 0c0d 04       		.uleb128 0x4
 1586 0c0e 08       		.byte	0x8
 1587 0c0f 04       		.byte	0x4
 1588 0c10 00000000 		.long	.LASF137
 1589 0c14 10       		.uleb128 0x10
 1590 0c15 08       		.byte	0x8
 1591 0c16 2D070000 		.long	0x72d
 1592 0c1a 23       		.uleb128 0x23
 1593 0c1b 00000000 		.long	.LASF138
 1594 0c1f 06       		.byte	0x6
 1595 0c20 C901     		.value	0x1c9
 1596 0c22 350C0000 		.long	0xc35
 1597 0c26 350C0000 		.long	0xc35
 1598 0c2a 24       		.uleb128 0x24
 1599 0c2b 70070000 		.long	0x770
 1600 0c2f 24       		.uleb128 0x24
 1601 0c30 140C0000 		.long	0xc14
 1602 0c34 00       		.byte	0
 1603 0c35 04       		.uleb128 0x4
 1604 0c36 04       		.byte	0x4
 1605 0c37 04       		.byte	0x4
 1606 0c38 00000000 		.long	.LASF139
 1607 0c3c 23       		.uleb128 0x23
 1608 0c3d 00000000 		.long	.LASF140
GAS LISTING /tmp/ccLn5Wva.s 			page 31


 1609 0c41 06       		.byte	0x6
 1610 0c42 1A01     		.value	0x11a
 1611 0c44 2D070000 		.long	0x72d
 1612 0c48 5C0C0000 		.long	0xc5c
 1613 0c4c 24       		.uleb128 0x24
 1614 0c4d 2D070000 		.long	0x72d
 1615 0c51 24       		.uleb128 0x24
 1616 0c52 70070000 		.long	0x770
 1617 0c56 24       		.uleb128 0x24
 1618 0c57 140C0000 		.long	0xc14
 1619 0c5b 00       		.byte	0
 1620 0c5c 23       		.uleb128 0x23
 1621 0c5d 00000000 		.long	.LASF141
 1622 0c61 06       		.byte	0x6
 1623 0c62 D401     		.value	0x1d4
 1624 0c64 7C0C0000 		.long	0xc7c
 1625 0c68 7C0C0000 		.long	0xc7c
 1626 0c6c 24       		.uleb128 0x24
 1627 0c6d 70070000 		.long	0x770
 1628 0c71 24       		.uleb128 0x24
 1629 0c72 140C0000 		.long	0xc14
 1630 0c76 24       		.uleb128 0x24
 1631 0c77 03010000 		.long	0x103
 1632 0c7b 00       		.byte	0
 1633 0c7c 04       		.uleb128 0x4
 1634 0c7d 08       		.byte	0x8
 1635 0c7e 05       		.byte	0x5
 1636 0c7f 00000000 		.long	.LASF142
 1637 0c83 23       		.uleb128 0x23
 1638 0c84 00000000 		.long	.LASF143
 1639 0c88 06       		.byte	0x6
 1640 0c89 D901     		.value	0x1d9
 1641 0c8b 95000000 		.long	0x95
 1642 0c8f A30C0000 		.long	0xca3
 1643 0c93 24       		.uleb128 0x24
 1644 0c94 70070000 		.long	0x770
 1645 0c98 24       		.uleb128 0x24
 1646 0c99 140C0000 		.long	0xc14
 1647 0c9d 24       		.uleb128 0x24
 1648 0c9e 03010000 		.long	0x103
 1649 0ca2 00       		.byte	0
 1650 0ca3 27       		.uleb128 0x27
 1651 0ca4 00000000 		.long	.LASF144
 1652 0ca8 06       		.byte	0x6
 1653 0ca9 C4       		.byte	0xc4
 1654 0caa 8A000000 		.long	0x8a
 1655 0cae C20C0000 		.long	0xcc2
 1656 0cb2 24       		.uleb128 0x24
 1657 0cb3 2D070000 		.long	0x72d
 1658 0cb7 24       		.uleb128 0x24
 1659 0cb8 70070000 		.long	0x770
 1660 0cbc 24       		.uleb128 0x24
 1661 0cbd 8A000000 		.long	0x8a
 1662 0cc1 00       		.byte	0
 1663 0cc2 23       		.uleb128 0x23
 1664 0cc3 00000000 		.long	.LASF145
 1665 0cc7 06       		.byte	0x6
GAS LISTING /tmp/ccLn5Wva.s 			page 32


 1666 0cc8 6501     		.value	0x165
 1667 0cca 03010000 		.long	0x103
 1668 0cce D80C0000 		.long	0xcd8
 1669 0cd2 24       		.uleb128 0x24
 1670 0cd3 9C000000 		.long	0x9c
 1671 0cd7 00       		.byte	0
 1672 0cd8 23       		.uleb128 0x23
 1673 0cd9 00000000 		.long	.LASF146
 1674 0cdd 06       		.byte	0x6
 1675 0cde 4501     		.value	0x145
 1676 0ce0 03010000 		.long	0x103
 1677 0ce4 F80C0000 		.long	0xcf8
 1678 0ce8 24       		.uleb128 0x24
 1679 0ce9 70070000 		.long	0x770
 1680 0ced 24       		.uleb128 0x24
 1681 0cee 70070000 		.long	0x770
 1682 0cf2 24       		.uleb128 0x24
 1683 0cf3 8A000000 		.long	0x8a
 1684 0cf7 00       		.byte	0
 1685 0cf8 23       		.uleb128 0x23
 1686 0cf9 00000000 		.long	.LASF147
 1687 0cfd 06       		.byte	0x6
 1688 0cfe 4901     		.value	0x149
 1689 0d00 2D070000 		.long	0x72d
 1690 0d04 180D0000 		.long	0xd18
 1691 0d08 24       		.uleb128 0x24
 1692 0d09 2D070000 		.long	0x72d
 1693 0d0d 24       		.uleb128 0x24
 1694 0d0e 70070000 		.long	0x770
 1695 0d12 24       		.uleb128 0x24
 1696 0d13 8A000000 		.long	0x8a
 1697 0d17 00       		.byte	0
 1698 0d18 23       		.uleb128 0x23
 1699 0d19 00000000 		.long	.LASF148
 1700 0d1d 06       		.byte	0x6
 1701 0d1e 4E01     		.value	0x14e
 1702 0d20 2D070000 		.long	0x72d
 1703 0d24 380D0000 		.long	0xd38
 1704 0d28 24       		.uleb128 0x24
 1705 0d29 2D070000 		.long	0x72d
 1706 0d2d 24       		.uleb128 0x24
 1707 0d2e 70070000 		.long	0x770
 1708 0d32 24       		.uleb128 0x24
 1709 0d33 8A000000 		.long	0x8a
 1710 0d37 00       		.byte	0
 1711 0d38 23       		.uleb128 0x23
 1712 0d39 00000000 		.long	.LASF149
 1713 0d3d 06       		.byte	0x6
 1714 0d3e 5201     		.value	0x152
 1715 0d40 2D070000 		.long	0x72d
 1716 0d44 580D0000 		.long	0xd58
 1717 0d48 24       		.uleb128 0x24
 1718 0d49 2D070000 		.long	0x72d
 1719 0d4d 24       		.uleb128 0x24
 1720 0d4e 33070000 		.long	0x733
 1721 0d52 24       		.uleb128 0x24
 1722 0d53 8A000000 		.long	0x8a
GAS LISTING /tmp/ccLn5Wva.s 			page 33


 1723 0d57 00       		.byte	0
 1724 0d58 23       		.uleb128 0x23
 1725 0d59 00000000 		.long	.LASF150
 1726 0d5d 06       		.byte	0x6
 1727 0d5e 5902     		.value	0x259
 1728 0d60 03010000 		.long	0x103
 1729 0d64 6F0D0000 		.long	0xd6f
 1730 0d68 24       		.uleb128 0x24
 1731 0d69 70070000 		.long	0x770
 1732 0d6d 25       		.uleb128 0x25
 1733 0d6e 00       		.byte	0
 1734 0d6f 23       		.uleb128 0x23
 1735 0d70 00000000 		.long	.LASF151
 1736 0d74 06       		.byte	0x6
 1737 0d75 8202     		.value	0x282
 1738 0d77 03010000 		.long	0x103
 1739 0d7b 860D0000 		.long	0xd86
 1740 0d7f 24       		.uleb128 0x24
 1741 0d80 70070000 		.long	0x770
 1742 0d84 25       		.uleb128 0x25
 1743 0d85 00       		.byte	0
 1744 0d86 29       		.uleb128 0x29
 1745 0d87 00000000 		.long	.LASF153
 1746 0d8b 06       		.byte	0x6
 1747 0d8c E0       		.byte	0xe0
 1748 0d8d 00000000 		.long	.LASF153
 1749 0d91 70070000 		.long	0x770
 1750 0d95 A40D0000 		.long	0xda4
 1751 0d99 24       		.uleb128 0x24
 1752 0d9a 70070000 		.long	0x770
 1753 0d9e 24       		.uleb128 0x24
 1754 0d9f 33070000 		.long	0x733
 1755 0da3 00       		.byte	0
 1756 0da4 2A       		.uleb128 0x2a
 1757 0da5 00000000 		.long	.LASF154
 1758 0da9 06       		.byte	0x6
 1759 0daa 0601     		.value	0x106
 1760 0dac 00000000 		.long	.LASF154
 1761 0db0 70070000 		.long	0x770
 1762 0db4 C30D0000 		.long	0xdc3
 1763 0db8 24       		.uleb128 0x24
 1764 0db9 70070000 		.long	0x770
 1765 0dbd 24       		.uleb128 0x24
 1766 0dbe 70070000 		.long	0x770
 1767 0dc2 00       		.byte	0
 1768 0dc3 29       		.uleb128 0x29
 1769 0dc4 00000000 		.long	.LASF155
 1770 0dc8 06       		.byte	0x6
 1771 0dc9 EA       		.byte	0xea
 1772 0dca 00000000 		.long	.LASF155
 1773 0dce 70070000 		.long	0x770
 1774 0dd2 E10D0000 		.long	0xde1
 1775 0dd6 24       		.uleb128 0x24
 1776 0dd7 70070000 		.long	0x770
 1777 0ddb 24       		.uleb128 0x24
 1778 0ddc 33070000 		.long	0x733
 1779 0de0 00       		.byte	0
GAS LISTING /tmp/ccLn5Wva.s 			page 34


 1780 0de1 2A       		.uleb128 0x2a
 1781 0de2 00000000 		.long	.LASF156
 1782 0de6 06       		.byte	0x6
 1783 0de7 1101     		.value	0x111
 1784 0de9 00000000 		.long	.LASF156
 1785 0ded 70070000 		.long	0x770
 1786 0df1 000E0000 		.long	0xe00
 1787 0df5 24       		.uleb128 0x24
 1788 0df6 70070000 		.long	0x770
 1789 0dfa 24       		.uleb128 0x24
 1790 0dfb 70070000 		.long	0x770
 1791 0dff 00       		.byte	0
 1792 0e00 2A       		.uleb128 0x2a
 1793 0e01 00000000 		.long	.LASF157
 1794 0e05 06       		.byte	0x6
 1795 0e06 3C01     		.value	0x13c
 1796 0e08 00000000 		.long	.LASF157
 1797 0e0c 70070000 		.long	0x770
 1798 0e10 240E0000 		.long	0xe24
 1799 0e14 24       		.uleb128 0x24
 1800 0e15 70070000 		.long	0x770
 1801 0e19 24       		.uleb128 0x24
 1802 0e1a 33070000 		.long	0x733
 1803 0e1e 24       		.uleb128 0x24
 1804 0e1f 8A000000 		.long	0x8a
 1805 0e23 00       		.byte	0
 1806 0e24 2B       		.uleb128 0x2b
 1807 0e25 00000000 		.long	.LASF158
 1808 0e29 07       		.byte	0x7
 1809 0e2a F2       		.byte	0xf2
 1810 0e2b A20F0000 		.long	0xfa2
 1811 0e2f 12       		.uleb128 0x12
 1812 0e30 07       		.byte	0x7
 1813 0e31 F8       		.byte	0xf8
 1814 0e32 A20F0000 		.long	0xfa2
 1815 0e36 13       		.uleb128 0x13
 1816 0e37 07       		.byte	0x7
 1817 0e38 0101     		.value	0x101
 1818 0e3a C40F0000 		.long	0xfc4
 1819 0e3e 13       		.uleb128 0x13
 1820 0e3f 07       		.byte	0x7
 1821 0e40 0201     		.value	0x102
 1822 0e42 EB0F0000 		.long	0xfeb
 1823 0e46 14       		.uleb128 0x14
 1824 0e47 00000000 		.long	.LASF160
 1825 0e4b 0E       		.byte	0xe
 1826 0e4c 24       		.byte	0x24
 1827 0e4d 12       		.uleb128 0x12
 1828 0e4e 0F       		.byte	0xf
 1829 0e4f 2C       		.byte	0x2c
 1830 0e50 F7020000 		.long	0x2f7
 1831 0e54 12       		.uleb128 0x12
 1832 0e55 0F       		.byte	0xf
 1833 0e56 2D       		.byte	0x2d
 1834 0e57 17030000 		.long	0x317
 1835 0e5b 05       		.uleb128 0x5
 1836 0e5c 00000000 		.long	.LASF161
GAS LISTING /tmp/ccLn5Wva.s 			page 35


 1837 0e60 01       		.byte	0x1
 1838 0e61 10       		.byte	0x10
 1839 0e62 37       		.byte	0x37
 1840 0e63 9D0E0000 		.long	0xe9d
 1841 0e67 2C       		.uleb128 0x2c
 1842 0e68 00000000 		.long	.LASF162
 1843 0e6c 10       		.byte	0x10
 1844 0e6d 3A       		.byte	0x3a
 1845 0e6e 27010000 		.long	0x127
 1846 0e72 2C       		.uleb128 0x2c
 1847 0e73 00000000 		.long	.LASF163
 1848 0e77 10       		.byte	0x10
 1849 0e78 3B       		.byte	0x3b
 1850 0e79 27010000 		.long	0x127
 1851 0e7d 2C       		.uleb128 0x2c
 1852 0e7e 00000000 		.long	.LASF164
 1853 0e82 10       		.byte	0x10
 1854 0e83 3F       		.byte	0x3f
 1855 0e84 AF110000 		.long	0x11af
 1856 0e88 2C       		.uleb128 0x2c
 1857 0e89 00000000 		.long	.LASF165
 1858 0e8d 10       		.byte	0x10
 1859 0e8e 40       		.byte	0x40
 1860 0e8f 27010000 		.long	0x127
 1861 0e93 2D       		.uleb128 0x2d
 1862 0e94 00000000 		.long	.LASF167
 1863 0e98 03010000 		.long	0x103
 1864 0e9c 00       		.byte	0
 1865 0e9d 05       		.uleb128 0x5
 1866 0e9e 00000000 		.long	.LASF166
 1867 0ea2 01       		.byte	0x1
 1868 0ea3 10       		.byte	0x10
 1869 0ea4 37       		.byte	0x37
 1870 0ea5 DF0E0000 		.long	0xedf
 1871 0ea9 2C       		.uleb128 0x2c
 1872 0eaa 00000000 		.long	.LASF162
 1873 0eae 10       		.byte	0x10
 1874 0eaf 3A       		.byte	0x3a
 1875 0eb0 B4110000 		.long	0x11b4
 1876 0eb4 2C       		.uleb128 0x2c
 1877 0eb5 00000000 		.long	.LASF163
 1878 0eb9 10       		.byte	0x10
 1879 0eba 3B       		.byte	0x3b
 1880 0ebb B4110000 		.long	0x11b4
 1881 0ebf 2C       		.uleb128 0x2c
 1882 0ec0 00000000 		.long	.LASF164
 1883 0ec4 10       		.byte	0x10
 1884 0ec5 3F       		.byte	0x3f
 1885 0ec6 AF110000 		.long	0x11af
 1886 0eca 2C       		.uleb128 0x2c
 1887 0ecb 00000000 		.long	.LASF165
 1888 0ecf 10       		.byte	0x10
 1889 0ed0 40       		.byte	0x40
 1890 0ed1 27010000 		.long	0x127
 1891 0ed5 2D       		.uleb128 0x2d
 1892 0ed6 00000000 		.long	.LASF167
 1893 0eda 95000000 		.long	0x95
GAS LISTING /tmp/ccLn5Wva.s 			page 36


 1894 0ede 00       		.byte	0
 1895 0edf 05       		.uleb128 0x5
 1896 0ee0 00000000 		.long	.LASF168
 1897 0ee4 01       		.byte	0x1
 1898 0ee5 10       		.byte	0x10
 1899 0ee6 37       		.byte	0x37
 1900 0ee7 210F0000 		.long	0xf21
 1901 0eeb 2C       		.uleb128 0x2c
 1902 0eec 00000000 		.long	.LASF162
 1903 0ef0 10       		.byte	0x10
 1904 0ef1 3A       		.byte	0x3a
 1905 0ef2 32010000 		.long	0x132
 1906 0ef6 2C       		.uleb128 0x2c
 1907 0ef7 00000000 		.long	.LASF163
 1908 0efb 10       		.byte	0x10
 1909 0efc 3B       		.byte	0x3b
 1910 0efd 32010000 		.long	0x132
 1911 0f01 2C       		.uleb128 0x2c
 1912 0f02 00000000 		.long	.LASF164
 1913 0f06 10       		.byte	0x10
 1914 0f07 3F       		.byte	0x3f
 1915 0f08 AF110000 		.long	0x11af
 1916 0f0c 2C       		.uleb128 0x2c
 1917 0f0d 00000000 		.long	.LASF165
 1918 0f11 10       		.byte	0x10
 1919 0f12 40       		.byte	0x40
 1920 0f13 27010000 		.long	0x127
 1921 0f17 2D       		.uleb128 0x2d
 1922 0f18 00000000 		.long	.LASF167
 1923 0f1c FC000000 		.long	0xfc
 1924 0f20 00       		.byte	0
 1925 0f21 05       		.uleb128 0x5
 1926 0f22 00000000 		.long	.LASF169
 1927 0f26 01       		.byte	0x1
 1928 0f27 10       		.byte	0x10
 1929 0f28 37       		.byte	0x37
 1930 0f29 630F0000 		.long	0xf63
 1931 0f2d 2C       		.uleb128 0x2c
 1932 0f2e 00000000 		.long	.LASF162
 1933 0f32 10       		.byte	0x10
 1934 0f33 3A       		.byte	0x3a
 1935 0f34 3E120000 		.long	0x123e
 1936 0f38 2C       		.uleb128 0x2c
 1937 0f39 00000000 		.long	.LASF163
 1938 0f3d 10       		.byte	0x10
 1939 0f3e 3B       		.byte	0x3b
 1940 0f3f 3E120000 		.long	0x123e
 1941 0f43 2C       		.uleb128 0x2c
 1942 0f44 00000000 		.long	.LASF164
 1943 0f48 10       		.byte	0x10
 1944 0f49 3F       		.byte	0x3f
 1945 0f4a AF110000 		.long	0x11af
 1946 0f4e 2C       		.uleb128 0x2c
 1947 0f4f 00000000 		.long	.LASF165
 1948 0f53 10       		.byte	0x10
 1949 0f54 40       		.byte	0x40
 1950 0f55 27010000 		.long	0x127
GAS LISTING /tmp/ccLn5Wva.s 			page 37


 1951 0f59 2D       		.uleb128 0x2d
 1952 0f5a 00000000 		.long	.LASF167
 1953 0f5e 20100000 		.long	0x1020
 1954 0f62 00       		.byte	0
 1955 0f63 2E       		.uleb128 0x2e
 1956 0f64 00000000 		.long	.LASF235
 1957 0f68 01       		.byte	0x1
 1958 0f69 10       		.byte	0x10
 1959 0f6a 37       		.byte	0x37
 1960 0f6b 2C       		.uleb128 0x2c
 1961 0f6c 00000000 		.long	.LASF162
 1962 0f70 10       		.byte	0x10
 1963 0f71 3A       		.byte	0x3a
 1964 0f72 43120000 		.long	0x1243
 1965 0f76 2C       		.uleb128 0x2c
 1966 0f77 00000000 		.long	.LASF163
 1967 0f7b 10       		.byte	0x10
 1968 0f7c 3B       		.byte	0x3b
 1969 0f7d 43120000 		.long	0x1243
 1970 0f81 2C       		.uleb128 0x2c
 1971 0f82 00000000 		.long	.LASF164
 1972 0f86 10       		.byte	0x10
 1973 0f87 3F       		.byte	0x3f
 1974 0f88 AF110000 		.long	0x11af
 1975 0f8c 2C       		.uleb128 0x2c
 1976 0f8d 00000000 		.long	.LASF165
 1977 0f91 10       		.byte	0x10
 1978 0f92 40       		.byte	0x40
 1979 0f93 27010000 		.long	0x127
 1980 0f97 2D       		.uleb128 0x2d
 1981 0f98 00000000 		.long	.LASF167
 1982 0f9c 7C0C0000 		.long	0xc7c
 1983 0fa0 00       		.byte	0
 1984 0fa1 00       		.byte	0
 1985 0fa2 23       		.uleb128 0x23
 1986 0fa3 00000000 		.long	.LASF170
 1987 0fa7 06       		.byte	0x6
 1988 0fa8 CB01     		.value	0x1cb
 1989 0faa BD0F0000 		.long	0xfbd
 1990 0fae BD0F0000 		.long	0xfbd
 1991 0fb2 24       		.uleb128 0x24
 1992 0fb3 70070000 		.long	0x770
 1993 0fb7 24       		.uleb128 0x24
 1994 0fb8 140C0000 		.long	0xc14
 1995 0fbc 00       		.byte	0
 1996 0fbd 04       		.uleb128 0x4
 1997 0fbe 10       		.byte	0x10
 1998 0fbf 04       		.byte	0x4
 1999 0fc0 00000000 		.long	.LASF171
 2000 0fc4 23       		.uleb128 0x23
 2001 0fc5 00000000 		.long	.LASF172
 2002 0fc9 06       		.byte	0x6
 2003 0fca E301     		.value	0x1e3
 2004 0fcc E40F0000 		.long	0xfe4
 2005 0fd0 E40F0000 		.long	0xfe4
 2006 0fd4 24       		.uleb128 0x24
 2007 0fd5 70070000 		.long	0x770
GAS LISTING /tmp/ccLn5Wva.s 			page 38


 2008 0fd9 24       		.uleb128 0x24
 2009 0fda 140C0000 		.long	0xc14
 2010 0fde 24       		.uleb128 0x24
 2011 0fdf 03010000 		.long	0x103
 2012 0fe3 00       		.byte	0
 2013 0fe4 04       		.uleb128 0x4
 2014 0fe5 08       		.byte	0x8
 2015 0fe6 05       		.byte	0x5
 2016 0fe7 00000000 		.long	.LASF173
 2017 0feb 23       		.uleb128 0x23
 2018 0fec 00000000 		.long	.LASF174
 2019 0ff0 06       		.byte	0x6
 2020 0ff1 EA01     		.value	0x1ea
 2021 0ff3 0B100000 		.long	0x100b
 2022 0ff7 0B100000 		.long	0x100b
 2023 0ffb 24       		.uleb128 0x24
 2024 0ffc 70070000 		.long	0x770
 2025 1000 24       		.uleb128 0x24
 2026 1001 140C0000 		.long	0xc14
 2027 1005 24       		.uleb128 0x24
 2028 1006 03010000 		.long	0x103
 2029 100a 00       		.byte	0
 2030 100b 04       		.uleb128 0x4
 2031 100c 08       		.byte	0x8
 2032 100d 07       		.byte	0x7
 2033 100e 00000000 		.long	.LASF175
 2034 1012 04       		.uleb128 0x4
 2035 1013 01       		.byte	0x1
 2036 1014 08       		.byte	0x8
 2037 1015 00000000 		.long	.LASF176
 2038 1019 04       		.uleb128 0x4
 2039 101a 01       		.byte	0x1
 2040 101b 06       		.byte	0x6
 2041 101c 00000000 		.long	.LASF177
 2042 1020 04       		.uleb128 0x4
 2043 1021 02       		.byte	0x2
 2044 1022 05       		.byte	0x5
 2045 1023 00000000 		.long	.LASF178
 2046 1027 2B       		.uleb128 0x2b
 2047 1028 00000000 		.long	.LASF179
 2048 102c 0D       		.byte	0xd
 2049 102d 37       		.byte	0x37
 2050 102e 3A100000 		.long	0x103a
 2051 1032 2F       		.uleb128 0x2f
 2052 1033 0D       		.byte	0xd
 2053 1034 38       		.byte	0x38
 2054 1035 F0020000 		.long	0x2f0
 2055 1039 00       		.byte	0
 2056 103a 04       		.uleb128 0x4
 2057 103b 01       		.byte	0x1
 2058 103c 02       		.byte	0x2
 2059 103d 00000000 		.long	.LASF180
 2060 1041 05       		.uleb128 0x5
 2061 1042 00000000 		.long	.LASF181
 2062 1046 60       		.byte	0x60
 2063 1047 11       		.byte	0x11
 2064 1048 35       		.byte	0x35
GAS LISTING /tmp/ccLn5Wva.s 			page 39


 2065 1049 6E110000 		.long	0x116e
 2066 104d 06       		.uleb128 0x6
 2067 104e 00000000 		.long	.LASF182
 2068 1052 11       		.byte	0x11
 2069 1053 39       		.byte	0x39
 2070 1054 F1090000 		.long	0x9f1
 2071 1058 00       		.byte	0
 2072 1059 06       		.uleb128 0x6
 2073 105a 00000000 		.long	.LASF183
 2074 105e 11       		.byte	0x11
 2075 105f 3A       		.byte	0x3a
 2076 1060 F1090000 		.long	0x9f1
 2077 1064 08       		.byte	0x8
 2078 1065 06       		.uleb128 0x6
 2079 1066 00000000 		.long	.LASF184
 2080 106a 11       		.byte	0x11
 2081 106b 40       		.byte	0x40
 2082 106c F1090000 		.long	0x9f1
 2083 1070 10       		.byte	0x10
 2084 1071 06       		.uleb128 0x6
 2085 1072 00000000 		.long	.LASF185
 2086 1076 11       		.byte	0x11
 2087 1077 46       		.byte	0x46
 2088 1078 F1090000 		.long	0x9f1
 2089 107c 18       		.byte	0x18
 2090 107d 06       		.uleb128 0x6
 2091 107e 00000000 		.long	.LASF186
 2092 1082 11       		.byte	0x11
 2093 1083 47       		.byte	0x47
 2094 1084 F1090000 		.long	0x9f1
 2095 1088 20       		.byte	0x20
 2096 1089 06       		.uleb128 0x6
 2097 108a 00000000 		.long	.LASF187
 2098 108e 11       		.byte	0x11
 2099 108f 48       		.byte	0x48
 2100 1090 F1090000 		.long	0x9f1
 2101 1094 28       		.byte	0x28
 2102 1095 06       		.uleb128 0x6
 2103 1096 00000000 		.long	.LASF188
 2104 109a 11       		.byte	0x11
 2105 109b 49       		.byte	0x49
 2106 109c F1090000 		.long	0x9f1
 2107 10a0 30       		.byte	0x30
 2108 10a1 06       		.uleb128 0x6
 2109 10a2 00000000 		.long	.LASF189
 2110 10a6 11       		.byte	0x11
 2111 10a7 4A       		.byte	0x4a
 2112 10a8 F1090000 		.long	0x9f1
 2113 10ac 38       		.byte	0x38
 2114 10ad 06       		.uleb128 0x6
 2115 10ae 00000000 		.long	.LASF190
 2116 10b2 11       		.byte	0x11
 2117 10b3 4B       		.byte	0x4b
 2118 10b4 F1090000 		.long	0x9f1
 2119 10b8 40       		.byte	0x40
 2120 10b9 06       		.uleb128 0x6
 2121 10ba 00000000 		.long	.LASF191
GAS LISTING /tmp/ccLn5Wva.s 			page 40


 2122 10be 11       		.byte	0x11
 2123 10bf 4C       		.byte	0x4c
 2124 10c0 F1090000 		.long	0x9f1
 2125 10c4 48       		.byte	0x48
 2126 10c5 06       		.uleb128 0x6
 2127 10c6 00000000 		.long	.LASF192
 2128 10ca 11       		.byte	0x11
 2129 10cb 4D       		.byte	0x4d
 2130 10cc FC000000 		.long	0xfc
 2131 10d0 50       		.byte	0x50
 2132 10d1 06       		.uleb128 0x6
 2133 10d2 00000000 		.long	.LASF193
 2134 10d6 11       		.byte	0x11
 2135 10d7 4E       		.byte	0x4e
 2136 10d8 FC000000 		.long	0xfc
 2137 10dc 51       		.byte	0x51
 2138 10dd 06       		.uleb128 0x6
 2139 10de 00000000 		.long	.LASF194
 2140 10e2 11       		.byte	0x11
 2141 10e3 50       		.byte	0x50
 2142 10e4 FC000000 		.long	0xfc
 2143 10e8 52       		.byte	0x52
 2144 10e9 06       		.uleb128 0x6
 2145 10ea 00000000 		.long	.LASF195
 2146 10ee 11       		.byte	0x11
 2147 10ef 52       		.byte	0x52
 2148 10f0 FC000000 		.long	0xfc
 2149 10f4 53       		.byte	0x53
 2150 10f5 06       		.uleb128 0x6
 2151 10f6 00000000 		.long	.LASF196
 2152 10fa 11       		.byte	0x11
 2153 10fb 54       		.byte	0x54
 2154 10fc FC000000 		.long	0xfc
 2155 1100 54       		.byte	0x54
 2156 1101 06       		.uleb128 0x6
 2157 1102 00000000 		.long	.LASF197
 2158 1106 11       		.byte	0x11
 2159 1107 56       		.byte	0x56
 2160 1108 FC000000 		.long	0xfc
 2161 110c 55       		.byte	0x55
 2162 110d 06       		.uleb128 0x6
 2163 110e 00000000 		.long	.LASF198
 2164 1112 11       		.byte	0x11
 2165 1113 5D       		.byte	0x5d
 2166 1114 FC000000 		.long	0xfc
 2167 1118 56       		.byte	0x56
 2168 1119 06       		.uleb128 0x6
 2169 111a 00000000 		.long	.LASF199
 2170 111e 11       		.byte	0x11
 2171 111f 5E       		.byte	0x5e
 2172 1120 FC000000 		.long	0xfc
 2173 1124 57       		.byte	0x57
 2174 1125 06       		.uleb128 0x6
 2175 1126 00000000 		.long	.LASF200
 2176 112a 11       		.byte	0x11
 2177 112b 61       		.byte	0x61
 2178 112c FC000000 		.long	0xfc
GAS LISTING /tmp/ccLn5Wva.s 			page 41


 2179 1130 58       		.byte	0x58
 2180 1131 06       		.uleb128 0x6
 2181 1132 00000000 		.long	.LASF201
 2182 1136 11       		.byte	0x11
 2183 1137 63       		.byte	0x63
 2184 1138 FC000000 		.long	0xfc
 2185 113c 59       		.byte	0x59
 2186 113d 06       		.uleb128 0x6
 2187 113e 00000000 		.long	.LASF202
 2188 1142 11       		.byte	0x11
 2189 1143 65       		.byte	0x65
 2190 1144 FC000000 		.long	0xfc
 2191 1148 5A       		.byte	0x5a
 2192 1149 06       		.uleb128 0x6
 2193 114a 00000000 		.long	.LASF203
 2194 114e 11       		.byte	0x11
 2195 114f 67       		.byte	0x67
 2196 1150 FC000000 		.long	0xfc
 2197 1154 5B       		.byte	0x5b
 2198 1155 06       		.uleb128 0x6
 2199 1156 00000000 		.long	.LASF204
 2200 115a 11       		.byte	0x11
 2201 115b 6E       		.byte	0x6e
 2202 115c FC000000 		.long	0xfc
 2203 1160 5C       		.byte	0x5c
 2204 1161 06       		.uleb128 0x6
 2205 1162 00000000 		.long	.LASF205
 2206 1166 11       		.byte	0x11
 2207 1167 6F       		.byte	0x6f
 2208 1168 FC000000 		.long	0xfc
 2209 116c 5D       		.byte	0x5d
 2210 116d 00       		.byte	0
 2211 116e 27       		.uleb128 0x27
 2212 116f 00000000 		.long	.LASF206
 2213 1173 11       		.byte	0x11
 2214 1174 7C       		.byte	0x7c
 2215 1175 F1090000 		.long	0x9f1
 2216 1179 88110000 		.long	0x1188
 2217 117d 24       		.uleb128 0x24
 2218 117e 03010000 		.long	0x103
 2219 1182 24       		.uleb128 0x24
 2220 1183 2C010000 		.long	0x12c
 2221 1187 00       		.byte	0
 2222 1188 30       		.uleb128 0x30
 2223 1189 00000000 		.long	.LASF208
 2224 118d 11       		.byte	0x11
 2225 118e 7F       		.byte	0x7f
 2226 118f 93110000 		.long	0x1193
 2227 1193 10       		.uleb128 0x10
 2228 1194 08       		.byte	0x8
 2229 1195 41100000 		.long	0x1041
 2230 1199 03       		.uleb128 0x3
 2231 119a 00000000 		.long	.LASF209
 2232 119e 12       		.byte	0x12
 2233 119f 28       		.byte	0x28
 2234 11a0 03010000 		.long	0x103
 2235 11a4 03       		.uleb128 0x3
GAS LISTING /tmp/ccLn5Wva.s 			page 42


 2236 11a5 00000000 		.long	.LASF210
 2237 11a9 13       		.byte	0x13
 2238 11aa 20       		.byte	0x20
 2239 11ab 03010000 		.long	0x103
 2240 11af 0F       		.uleb128 0xf
 2241 11b0 3A100000 		.long	0x103a
 2242 11b4 0F       		.uleb128 0xf
 2243 11b5 95000000 		.long	0x95
 2244 11b9 10       		.uleb128 0x10
 2245 11ba 08       		.byte	0x8
 2246 11bb 47040000 		.long	0x447
 2247 11bf 03       		.uleb128 0x3
 2248 11c0 00000000 		.long	.LASF211
 2249 11c4 14       		.byte	0x14
 2250 11c5 34       		.byte	0x34
 2251 11c6 95000000 		.long	0x95
 2252 11ca 03       		.uleb128 0x3
 2253 11cb 00000000 		.long	.LASF212
 2254 11cf 14       		.byte	0x14
 2255 11d0 BA       		.byte	0xba
 2256 11d1 D5110000 		.long	0x11d5
 2257 11d5 10       		.uleb128 0x10
 2258 11d6 08       		.byte	0x8
 2259 11d7 DB110000 		.long	0x11db
 2260 11db 0F       		.uleb128 0xf
 2261 11dc 99110000 		.long	0x1199
 2262 11e0 27       		.uleb128 0x27
 2263 11e1 00000000 		.long	.LASF213
 2264 11e5 14       		.byte	0x14
 2265 11e6 AF       		.byte	0xaf
 2266 11e7 03010000 		.long	0x103
 2267 11eb FA110000 		.long	0x11fa
 2268 11ef 24       		.uleb128 0x24
 2269 11f0 9C000000 		.long	0x9c
 2270 11f4 24       		.uleb128 0x24
 2271 11f5 BF110000 		.long	0x11bf
 2272 11f9 00       		.byte	0
 2273 11fa 27       		.uleb128 0x27
 2274 11fb 00000000 		.long	.LASF214
 2275 11ff 14       		.byte	0x14
 2276 1200 DD       		.byte	0xdd
 2277 1201 9C000000 		.long	0x9c
 2278 1205 14120000 		.long	0x1214
 2279 1209 24       		.uleb128 0x24
 2280 120a 9C000000 		.long	0x9c
 2281 120e 24       		.uleb128 0x24
 2282 120f CA110000 		.long	0x11ca
 2283 1213 00       		.byte	0
 2284 1214 27       		.uleb128 0x27
 2285 1215 00000000 		.long	.LASF215
 2286 1219 14       		.byte	0x14
 2287 121a DA       		.byte	0xda
 2288 121b CA110000 		.long	0x11ca
 2289 121f 29120000 		.long	0x1229
 2290 1223 24       		.uleb128 0x24
 2291 1224 2C010000 		.long	0x12c
 2292 1228 00       		.byte	0
GAS LISTING /tmp/ccLn5Wva.s 			page 43


 2293 1229 27       		.uleb128 0x27
 2294 122a 00000000 		.long	.LASF216
 2295 122e 14       		.byte	0x14
 2296 122f AB       		.byte	0xab
 2297 1230 BF110000 		.long	0x11bf
 2298 1234 3E120000 		.long	0x123e
 2299 1238 24       		.uleb128 0x24
 2300 1239 2C010000 		.long	0x12c
 2301 123d 00       		.byte	0
 2302 123e 0F       		.uleb128 0xf
 2303 123f 20100000 		.long	0x1020
 2304 1243 0F       		.uleb128 0xf
 2305 1244 7C0C0000 		.long	0xc7c
 2306 1248 2F       		.uleb128 0x2f
 2307 1249 01       		.byte	0x1
 2308 124a 03       		.byte	0x3
 2309 124b 37010000 		.long	0x137
 2310 124f 31       		.uleb128 0x31
 2311 1250 00000000 		.long	.LASF236
 2312 1254 01       		.byte	0x1
 2313 1255 05       		.byte	0x5
 2314 1256 00000000 		.long	.LASF237
 2315 125a 03010000 		.long	0x103
 2316 125e 00000000 		.quad	.LFB1020
 2316      00000000 
 2317 1266 10000000 		.quad	.LFE1020-.LFB1020
 2317      00000000 
 2318 126e 01       		.uleb128 0x1
 2319 126f 9C       		.byte	0x9c
 2320 1270 81120000 		.long	0x1281
 2321 1274 32       		.uleb128 0x32
 2322 1275 7800     		.string	"x"
 2323 1277 01       		.byte	0x1
 2324 1278 05       		.byte	0x5
 2325 1279 03010000 		.long	0x103
 2326 127d 02       		.uleb128 0x2
 2327 127e 91       		.byte	0x91
 2328 127f 6C       		.sleb128 -20
 2329 1280 00       		.byte	0
 2330 1281 33       		.uleb128 0x33
 2331 1282 00000000 		.long	.LASF238
 2332 1286 01       		.byte	0x1
 2333 1287 09       		.byte	0x9
 2334 1288 03010000 		.long	0x103
 2335 128c 00000000 		.quad	.LFB1021
 2335      00000000 
 2336 1294 23000000 		.quad	.LFE1021-.LFB1021
 2336      00000000 
 2337 129c 01       		.uleb128 0x1
 2338 129d 9C       		.byte	0x9c
 2339 129e C1120000 		.long	0x12c1
 2340 12a2 34       		.uleb128 0x34
 2341 12a3 00000000 		.quad	.LBB2
 2341      00000000 
 2342 12ab 14000000 		.quad	.LBE2-.LBB2
 2342      00000000 
 2343 12b3 35       		.uleb128 0x35
GAS LISTING /tmp/ccLn5Wva.s 			page 44


 2344 12b4 6900     		.string	"i"
 2345 12b6 01       		.byte	0x1
 2346 12b7 0A       		.byte	0xa
 2347 12b8 03010000 		.long	0x103
 2348 12bc 02       		.uleb128 0x2
 2349 12bd 91       		.byte	0x91
 2350 12be 6C       		.sleb128 -20
 2351 12bf 00       		.byte	0
 2352 12c0 00       		.byte	0
 2353 12c1 36       		.uleb128 0x36
 2354 12c2 00000000 		.long	.LASF239
 2355 12c6 00000000 		.quad	.LFB1022
 2355      00000000 
 2356 12ce 3D000000 		.quad	.LFE1022-.LFB1022
 2356      00000000 
 2357 12d6 01       		.uleb128 0x1
 2358 12d7 9C       		.byte	0x9c
 2359 12d8 F9120000 		.long	0x12f9
 2360 12dc 37       		.uleb128 0x37
 2361 12dd 00000000 		.long	.LASF218
 2362 12e1 01       		.byte	0x1
 2363 12e2 0C       		.byte	0xc
 2364 12e3 03010000 		.long	0x103
 2365 12e7 02       		.uleb128 0x2
 2366 12e8 91       		.byte	0x91
 2367 12e9 6C       		.sleb128 -20
 2368 12ea 37       		.uleb128 0x37
 2369 12eb 00000000 		.long	.LASF219
 2370 12ef 01       		.byte	0x1
 2371 12f0 0C       		.byte	0xc
 2372 12f1 03010000 		.long	0x103
 2373 12f5 02       		.uleb128 0x2
 2374 12f6 91       		.byte	0x91
 2375 12f7 68       		.sleb128 -24
 2376 12f8 00       		.byte	0
 2377 12f9 38       		.uleb128 0x38
 2378 12fa 00000000 		.long	.LASF240
 2379 12fe 00000000 		.quad	.LFB1023
 2379      00000000 
 2380 1306 15000000 		.quad	.LFE1023-.LFB1023
 2380      00000000 
 2381 130e 01       		.uleb128 0x1
 2382 130f 9C       		.byte	0x9c
 2383 1310 39       		.uleb128 0x39
 2384 1311 00000000 		.long	.LASF220
 2385 1315 88000000 		.long	0x88
 2386 1319 3A       		.uleb128 0x3a
 2387 131a CF060000 		.long	0x6cf
 2388 131e 09       		.uleb128 0x9
 2389 131f 03       		.byte	0x3
 2390 1320 00000000 		.quad	_ZStL8__ioinit
 2390      00000000 
 2391 1328 3B       		.uleb128 0x3b
 2392 1329 670E0000 		.long	0xe67
 2393 132d 00000000 		.long	.LASF221
 2394 1331 80808080 		.sleb128 -2147483648
 2394      78
GAS LISTING /tmp/ccLn5Wva.s 			page 45


 2395 1336 3C       		.uleb128 0x3c
 2396 1337 720E0000 		.long	0xe72
 2397 133b 00000000 		.long	.LASF222
 2398 133f FFFFFF7F 		.long	0x7fffffff
 2399 1343 3D       		.uleb128 0x3d
 2400 1344 CA0E0000 		.long	0xeca
 2401 1348 00000000 		.long	.LASF223
 2402 134c 40       		.byte	0x40
 2403 134d 3D       		.uleb128 0x3d
 2404 134e F60E0000 		.long	0xef6
 2405 1352 00000000 		.long	.LASF224
 2406 1356 7F       		.byte	0x7f
 2407 1357 3B       		.uleb128 0x3b
 2408 1358 2D0F0000 		.long	0xf2d
 2409 135c 00000000 		.long	.LASF225
 2410 1360 80807E   		.sleb128 -32768
 2411 1363 3E       		.uleb128 0x3e
 2412 1364 380F0000 		.long	0xf38
 2413 1368 00000000 		.long	.LASF226
 2414 136c FF7F     		.value	0x7fff
 2415 136e 3B       		.uleb128 0x3b
 2416 136f 6B0F0000 		.long	0xf6b
 2417 1373 00000000 		.long	.LASF227
 2418 1377 80808080 		.sleb128 -9223372036854775808
 2418      80808080 
 2418      807F
 2419 1381 3F       		.uleb128 0x3f
 2420 1382 760F0000 		.long	0xf76
 2421 1386 00000000 		.long	.LASF228
 2422 138a FFFFFFFF 		.quad	0x7fffffffffffffff
 2422      FFFFFF7F 
 2423 1392 00       		.byte	0
 2424              		.section	.debug_abbrev,"",@progbits
 2425              	.Ldebug_abbrev0:
 2426 0000 01       		.uleb128 0x1
 2427 0001 11       		.uleb128 0x11
 2428 0002 01       		.byte	0x1
 2429 0003 25       		.uleb128 0x25
 2430 0004 0E       		.uleb128 0xe
 2431 0005 13       		.uleb128 0x13
 2432 0006 0B       		.uleb128 0xb
 2433 0007 03       		.uleb128 0x3
 2434 0008 0E       		.uleb128 0xe
 2435 0009 1B       		.uleb128 0x1b
 2436 000a 0E       		.uleb128 0xe
 2437 000b 11       		.uleb128 0x11
 2438 000c 01       		.uleb128 0x1
 2439 000d 12       		.uleb128 0x12
 2440 000e 07       		.uleb128 0x7
 2441 000f 10       		.uleb128 0x10
 2442 0010 17       		.uleb128 0x17
 2443 0011 00       		.byte	0
 2444 0012 00       		.byte	0
 2445 0013 02       		.uleb128 0x2
 2446 0014 13       		.uleb128 0x13
 2447 0015 00       		.byte	0
 2448 0016 03       		.uleb128 0x3
GAS LISTING /tmp/ccLn5Wva.s 			page 46


 2449 0017 0E       		.uleb128 0xe
 2450 0018 3C       		.uleb128 0x3c
 2451 0019 19       		.uleb128 0x19
 2452 001a 00       		.byte	0
 2453 001b 00       		.byte	0
 2454 001c 03       		.uleb128 0x3
 2455 001d 16       		.uleb128 0x16
 2456 001e 00       		.byte	0
 2457 001f 03       		.uleb128 0x3
 2458 0020 0E       		.uleb128 0xe
 2459 0021 3A       		.uleb128 0x3a
 2460 0022 0B       		.uleb128 0xb
 2461 0023 3B       		.uleb128 0x3b
 2462 0024 0B       		.uleb128 0xb
 2463 0025 49       		.uleb128 0x49
 2464 0026 13       		.uleb128 0x13
 2465 0027 00       		.byte	0
 2466 0028 00       		.byte	0
 2467 0029 04       		.uleb128 0x4
 2468 002a 24       		.uleb128 0x24
 2469 002b 00       		.byte	0
 2470 002c 0B       		.uleb128 0xb
 2471 002d 0B       		.uleb128 0xb
 2472 002e 3E       		.uleb128 0x3e
 2473 002f 0B       		.uleb128 0xb
 2474 0030 03       		.uleb128 0x3
 2475 0031 0E       		.uleb128 0xe
 2476 0032 00       		.byte	0
 2477 0033 00       		.byte	0
 2478 0034 05       		.uleb128 0x5
 2479 0035 13       		.uleb128 0x13
 2480 0036 01       		.byte	0x1
 2481 0037 03       		.uleb128 0x3
 2482 0038 0E       		.uleb128 0xe
 2483 0039 0B       		.uleb128 0xb
 2484 003a 0B       		.uleb128 0xb
 2485 003b 3A       		.uleb128 0x3a
 2486 003c 0B       		.uleb128 0xb
 2487 003d 3B       		.uleb128 0x3b
 2488 003e 0B       		.uleb128 0xb
 2489 003f 01       		.uleb128 0x1
 2490 0040 13       		.uleb128 0x13
 2491 0041 00       		.byte	0
 2492 0042 00       		.byte	0
 2493 0043 06       		.uleb128 0x6
 2494 0044 0D       		.uleb128 0xd
 2495 0045 00       		.byte	0
 2496 0046 03       		.uleb128 0x3
 2497 0047 0E       		.uleb128 0xe
 2498 0048 3A       		.uleb128 0x3a
 2499 0049 0B       		.uleb128 0xb
 2500 004a 3B       		.uleb128 0x3b
 2501 004b 0B       		.uleb128 0xb
 2502 004c 49       		.uleb128 0x49
 2503 004d 13       		.uleb128 0x13
 2504 004e 38       		.uleb128 0x38
 2505 004f 0B       		.uleb128 0xb
GAS LISTING /tmp/ccLn5Wva.s 			page 47


 2506 0050 00       		.byte	0
 2507 0051 00       		.byte	0
 2508 0052 07       		.uleb128 0x7
 2509 0053 0F       		.uleb128 0xf
 2510 0054 00       		.byte	0
 2511 0055 0B       		.uleb128 0xb
 2512 0056 0B       		.uleb128 0xb
 2513 0057 00       		.byte	0
 2514 0058 00       		.byte	0
 2515 0059 08       		.uleb128 0x8
 2516 005a 16       		.uleb128 0x16
 2517 005b 00       		.byte	0
 2518 005c 03       		.uleb128 0x3
 2519 005d 0E       		.uleb128 0xe
 2520 005e 3A       		.uleb128 0x3a
 2521 005f 0B       		.uleb128 0xb
 2522 0060 3B       		.uleb128 0x3b
 2523 0061 05       		.uleb128 0x5
 2524 0062 49       		.uleb128 0x49
 2525 0063 13       		.uleb128 0x13
 2526 0064 00       		.byte	0
 2527 0065 00       		.byte	0
 2528 0066 09       		.uleb128 0x9
 2529 0067 13       		.uleb128 0x13
 2530 0068 01       		.byte	0x1
 2531 0069 0B       		.uleb128 0xb
 2532 006a 0B       		.uleb128 0xb
 2533 006b 3A       		.uleb128 0x3a
 2534 006c 0B       		.uleb128 0xb
 2535 006d 3B       		.uleb128 0x3b
 2536 006e 0B       		.uleb128 0xb
 2537 006f 6E       		.uleb128 0x6e
 2538 0070 0E       		.uleb128 0xe
 2539 0071 01       		.uleb128 0x1
 2540 0072 13       		.uleb128 0x13
 2541 0073 00       		.byte	0
 2542 0074 00       		.byte	0
 2543 0075 0A       		.uleb128 0xa
 2544 0076 17       		.uleb128 0x17
 2545 0077 01       		.byte	0x1
 2546 0078 0B       		.uleb128 0xb
 2547 0079 0B       		.uleb128 0xb
 2548 007a 3A       		.uleb128 0x3a
 2549 007b 0B       		.uleb128 0xb
 2550 007c 3B       		.uleb128 0x3b
 2551 007d 0B       		.uleb128 0xb
 2552 007e 01       		.uleb128 0x1
 2553 007f 13       		.uleb128 0x13
 2554 0080 00       		.byte	0
 2555 0081 00       		.byte	0
 2556 0082 0B       		.uleb128 0xb
 2557 0083 0D       		.uleb128 0xd
 2558 0084 00       		.byte	0
 2559 0085 03       		.uleb128 0x3
 2560 0086 0E       		.uleb128 0xe
 2561 0087 3A       		.uleb128 0x3a
 2562 0088 0B       		.uleb128 0xb
GAS LISTING /tmp/ccLn5Wva.s 			page 48


 2563 0089 3B       		.uleb128 0x3b
 2564 008a 0B       		.uleb128 0xb
 2565 008b 49       		.uleb128 0x49
 2566 008c 13       		.uleb128 0x13
 2567 008d 00       		.byte	0
 2568 008e 00       		.byte	0
 2569 008f 0C       		.uleb128 0xc
 2570 0090 01       		.uleb128 0x1
 2571 0091 01       		.byte	0x1
 2572 0092 49       		.uleb128 0x49
 2573 0093 13       		.uleb128 0x13
 2574 0094 01       		.uleb128 0x1
 2575 0095 13       		.uleb128 0x13
 2576 0096 00       		.byte	0
 2577 0097 00       		.byte	0
 2578 0098 0D       		.uleb128 0xd
 2579 0099 21       		.uleb128 0x21
 2580 009a 00       		.byte	0
 2581 009b 49       		.uleb128 0x49
 2582 009c 13       		.uleb128 0x13
 2583 009d 2F       		.uleb128 0x2f
 2584 009e 0B       		.uleb128 0xb
 2585 009f 00       		.byte	0
 2586 00a0 00       		.byte	0
 2587 00a1 0E       		.uleb128 0xe
 2588 00a2 24       		.uleb128 0x24
 2589 00a3 00       		.byte	0
 2590 00a4 0B       		.uleb128 0xb
 2591 00a5 0B       		.uleb128 0xb
 2592 00a6 3E       		.uleb128 0x3e
 2593 00a7 0B       		.uleb128 0xb
 2594 00a8 03       		.uleb128 0x3
 2595 00a9 08       		.uleb128 0x8
 2596 00aa 00       		.byte	0
 2597 00ab 00       		.byte	0
 2598 00ac 0F       		.uleb128 0xf
 2599 00ad 26       		.uleb128 0x26
 2600 00ae 00       		.byte	0
 2601 00af 49       		.uleb128 0x49
 2602 00b0 13       		.uleb128 0x13
 2603 00b1 00       		.byte	0
 2604 00b2 00       		.byte	0
 2605 00b3 10       		.uleb128 0x10
 2606 00b4 0F       		.uleb128 0xf
 2607 00b5 00       		.byte	0
 2608 00b6 0B       		.uleb128 0xb
 2609 00b7 0B       		.uleb128 0xb
 2610 00b8 49       		.uleb128 0x49
 2611 00b9 13       		.uleb128 0x13
 2612 00ba 00       		.byte	0
 2613 00bb 00       		.byte	0
 2614 00bc 11       		.uleb128 0x11
 2615 00bd 39       		.uleb128 0x39
 2616 00be 01       		.byte	0x1
 2617 00bf 03       		.uleb128 0x3
 2618 00c0 08       		.uleb128 0x8
 2619 00c1 3A       		.uleb128 0x3a
GAS LISTING /tmp/ccLn5Wva.s 			page 49


 2620 00c2 0B       		.uleb128 0xb
 2621 00c3 3B       		.uleb128 0x3b
 2622 00c4 0B       		.uleb128 0xb
 2623 00c5 01       		.uleb128 0x1
 2624 00c6 13       		.uleb128 0x13
 2625 00c7 00       		.byte	0
 2626 00c8 00       		.byte	0
 2627 00c9 12       		.uleb128 0x12
 2628 00ca 08       		.uleb128 0x8
 2629 00cb 00       		.byte	0
 2630 00cc 3A       		.uleb128 0x3a
 2631 00cd 0B       		.uleb128 0xb
 2632 00ce 3B       		.uleb128 0x3b
 2633 00cf 0B       		.uleb128 0xb
 2634 00d0 18       		.uleb128 0x18
 2635 00d1 13       		.uleb128 0x13
 2636 00d2 00       		.byte	0
 2637 00d3 00       		.byte	0
 2638 00d4 13       		.uleb128 0x13
 2639 00d5 08       		.uleb128 0x8
 2640 00d6 00       		.byte	0
 2641 00d7 3A       		.uleb128 0x3a
 2642 00d8 0B       		.uleb128 0xb
 2643 00d9 3B       		.uleb128 0x3b
 2644 00da 05       		.uleb128 0x5
 2645 00db 18       		.uleb128 0x18
 2646 00dc 13       		.uleb128 0x13
 2647 00dd 00       		.byte	0
 2648 00de 00       		.byte	0
 2649 00df 14       		.uleb128 0x14
 2650 00e0 39       		.uleb128 0x39
 2651 00e1 00       		.byte	0
 2652 00e2 03       		.uleb128 0x3
 2653 00e3 0E       		.uleb128 0xe
 2654 00e4 3A       		.uleb128 0x3a
 2655 00e5 0B       		.uleb128 0xb
 2656 00e6 3B       		.uleb128 0x3b
 2657 00e7 0B       		.uleb128 0xb
 2658 00e8 00       		.byte	0
 2659 00e9 00       		.byte	0
 2660 00ea 15       		.uleb128 0x15
 2661 00eb 04       		.uleb128 0x4
 2662 00ec 01       		.byte	0x1
 2663 00ed 03       		.uleb128 0x3
 2664 00ee 0E       		.uleb128 0xe
 2665 00ef 0B       		.uleb128 0xb
 2666 00f0 0B       		.uleb128 0xb
 2667 00f1 3A       		.uleb128 0x3a
 2668 00f2 0B       		.uleb128 0xb
 2669 00f3 3B       		.uleb128 0x3b
 2670 00f4 0B       		.uleb128 0xb
 2671 00f5 01       		.uleb128 0x1
 2672 00f6 13       		.uleb128 0x13
 2673 00f7 00       		.byte	0
 2674 00f8 00       		.byte	0
 2675 00f9 16       		.uleb128 0x16
 2676 00fa 28       		.uleb128 0x28
GAS LISTING /tmp/ccLn5Wva.s 			page 50


 2677 00fb 00       		.byte	0
 2678 00fc 03       		.uleb128 0x3
 2679 00fd 0E       		.uleb128 0xe
 2680 00fe 1C       		.uleb128 0x1c
 2681 00ff 0D       		.uleb128 0xd
 2682 0100 00       		.byte	0
 2683 0101 00       		.byte	0
 2684 0102 17       		.uleb128 0x17
 2685 0103 02       		.uleb128 0x2
 2686 0104 01       		.byte	0x1
 2687 0105 03       		.uleb128 0x3
 2688 0106 0E       		.uleb128 0xe
 2689 0107 3C       		.uleb128 0x3c
 2690 0108 19       		.uleb128 0x19
 2691 0109 01       		.uleb128 0x1
 2692 010a 13       		.uleb128 0x13
 2693 010b 00       		.byte	0
 2694 010c 00       		.byte	0
 2695 010d 18       		.uleb128 0x18
 2696 010e 02       		.uleb128 0x2
 2697 010f 01       		.byte	0x1
 2698 0110 03       		.uleb128 0x3
 2699 0111 0E       		.uleb128 0xe
 2700 0112 0B       		.uleb128 0xb
 2701 0113 0B       		.uleb128 0xb
 2702 0114 3A       		.uleb128 0x3a
 2703 0115 0B       		.uleb128 0xb
 2704 0116 3B       		.uleb128 0x3b
 2705 0117 05       		.uleb128 0x5
 2706 0118 32       		.uleb128 0x32
 2707 0119 0B       		.uleb128 0xb
 2708 011a 01       		.uleb128 0x1
 2709 011b 13       		.uleb128 0x13
 2710 011c 00       		.byte	0
 2711 011d 00       		.byte	0
 2712 011e 19       		.uleb128 0x19
 2713 011f 0D       		.uleb128 0xd
 2714 0120 00       		.byte	0
 2715 0121 03       		.uleb128 0x3
 2716 0122 0E       		.uleb128 0xe
 2717 0123 3A       		.uleb128 0x3a
 2718 0124 0B       		.uleb128 0xb
 2719 0125 3B       		.uleb128 0x3b
 2720 0126 05       		.uleb128 0x5
 2721 0127 49       		.uleb128 0x49
 2722 0128 13       		.uleb128 0x13
 2723 0129 3F       		.uleb128 0x3f
 2724 012a 19       		.uleb128 0x19
 2725 012b 3C       		.uleb128 0x3c
 2726 012c 19       		.uleb128 0x19
 2727 012d 00       		.byte	0
 2728 012e 00       		.byte	0
 2729 012f 1A       		.uleb128 0x1a
 2730 0130 2E       		.uleb128 0x2e
 2731 0131 01       		.byte	0x1
 2732 0132 3F       		.uleb128 0x3f
 2733 0133 19       		.uleb128 0x19
GAS LISTING /tmp/ccLn5Wva.s 			page 51


 2734 0134 03       		.uleb128 0x3
 2735 0135 0E       		.uleb128 0xe
 2736 0136 3A       		.uleb128 0x3a
 2737 0137 0B       		.uleb128 0xb
 2738 0138 3B       		.uleb128 0x3b
 2739 0139 05       		.uleb128 0x5
 2740 013a 32       		.uleb128 0x32
 2741 013b 0B       		.uleb128 0xb
 2742 013c 3C       		.uleb128 0x3c
 2743 013d 19       		.uleb128 0x19
 2744 013e 64       		.uleb128 0x64
 2745 013f 13       		.uleb128 0x13
 2746 0140 01       		.uleb128 0x1
 2747 0141 13       		.uleb128 0x13
 2748 0142 00       		.byte	0
 2749 0143 00       		.byte	0
 2750 0144 1B       		.uleb128 0x1b
 2751 0145 05       		.uleb128 0x5
 2752 0146 00       		.byte	0
 2753 0147 49       		.uleb128 0x49
 2754 0148 13       		.uleb128 0x13
 2755 0149 34       		.uleb128 0x34
 2756 014a 19       		.uleb128 0x19
 2757 014b 00       		.byte	0
 2758 014c 00       		.byte	0
 2759 014d 1C       		.uleb128 0x1c
 2760 014e 2E       		.uleb128 0x2e
 2761 014f 01       		.byte	0x1
 2762 0150 3F       		.uleb128 0x3f
 2763 0151 19       		.uleb128 0x19
 2764 0152 03       		.uleb128 0x3
 2765 0153 0E       		.uleb128 0xe
 2766 0154 3A       		.uleb128 0x3a
 2767 0155 0B       		.uleb128 0xb
 2768 0156 3B       		.uleb128 0x3b
 2769 0157 05       		.uleb128 0x5
 2770 0158 32       		.uleb128 0x32
 2771 0159 0B       		.uleb128 0xb
 2772 015a 3C       		.uleb128 0x3c
 2773 015b 19       		.uleb128 0x19
 2774 015c 64       		.uleb128 0x64
 2775 015d 13       		.uleb128 0x13
 2776 015e 00       		.byte	0
 2777 015f 00       		.byte	0
 2778 0160 1D       		.uleb128 0x1d
 2779 0161 16       		.uleb128 0x16
 2780 0162 00       		.byte	0
 2781 0163 03       		.uleb128 0x3
 2782 0164 0E       		.uleb128 0xe
 2783 0165 3A       		.uleb128 0x3a
 2784 0166 0B       		.uleb128 0xb
 2785 0167 3B       		.uleb128 0x3b
 2786 0168 0B       		.uleb128 0xb
 2787 0169 49       		.uleb128 0x49
 2788 016a 13       		.uleb128 0x13
 2789 016b 32       		.uleb128 0x32
 2790 016c 0B       		.uleb128 0xb
GAS LISTING /tmp/ccLn5Wva.s 			page 52


 2791 016d 00       		.byte	0
 2792 016e 00       		.byte	0
 2793 016f 1E       		.uleb128 0x1e
 2794 0170 0D       		.uleb128 0xd
 2795 0171 00       		.byte	0
 2796 0172 03       		.uleb128 0x3
 2797 0173 0E       		.uleb128 0xe
 2798 0174 3A       		.uleb128 0x3a
 2799 0175 0B       		.uleb128 0xb
 2800 0176 3B       		.uleb128 0x3b
 2801 0177 05       		.uleb128 0x5
 2802 0178 49       		.uleb128 0x49
 2803 0179 13       		.uleb128 0x13
 2804 017a 3F       		.uleb128 0x3f
 2805 017b 19       		.uleb128 0x19
 2806 017c 32       		.uleb128 0x32
 2807 017d 0B       		.uleb128 0xb
 2808 017e 3C       		.uleb128 0x3c
 2809 017f 19       		.uleb128 0x19
 2810 0180 1C       		.uleb128 0x1c
 2811 0181 0B       		.uleb128 0xb
 2812 0182 00       		.byte	0
 2813 0183 00       		.byte	0
 2814 0184 1F       		.uleb128 0x1f
 2815 0185 0D       		.uleb128 0xd
 2816 0186 00       		.byte	0
 2817 0187 03       		.uleb128 0x3
 2818 0188 08       		.uleb128 0x8
 2819 0189 3A       		.uleb128 0x3a
 2820 018a 0B       		.uleb128 0xb
 2821 018b 3B       		.uleb128 0x3b
 2822 018c 05       		.uleb128 0x5
 2823 018d 49       		.uleb128 0x49
 2824 018e 13       		.uleb128 0x13
 2825 018f 3F       		.uleb128 0x3f
 2826 0190 19       		.uleb128 0x19
 2827 0191 32       		.uleb128 0x32
 2828 0192 0B       		.uleb128 0xb
 2829 0193 3C       		.uleb128 0x3c
 2830 0194 19       		.uleb128 0x19
 2831 0195 1C       		.uleb128 0x1c
 2832 0196 0B       		.uleb128 0xb
 2833 0197 00       		.byte	0
 2834 0198 00       		.byte	0
 2835 0199 20       		.uleb128 0x20
 2836 019a 0D       		.uleb128 0xd
 2837 019b 00       		.byte	0
 2838 019c 03       		.uleb128 0x3
 2839 019d 0E       		.uleb128 0xe
 2840 019e 3A       		.uleb128 0x3a
 2841 019f 0B       		.uleb128 0xb
 2842 01a0 3B       		.uleb128 0x3b
 2843 01a1 05       		.uleb128 0x5
 2844 01a2 49       		.uleb128 0x49
 2845 01a3 13       		.uleb128 0x13
 2846 01a4 3F       		.uleb128 0x3f
 2847 01a5 19       		.uleb128 0x19
GAS LISTING /tmp/ccLn5Wva.s 			page 53


 2848 01a6 32       		.uleb128 0x32
 2849 01a7 0B       		.uleb128 0xb
 2850 01a8 3C       		.uleb128 0x3c
 2851 01a9 19       		.uleb128 0x19
 2852 01aa 1C       		.uleb128 0x1c
 2853 01ab 05       		.uleb128 0x5
 2854 01ac 00       		.byte	0
 2855 01ad 00       		.byte	0
 2856 01ae 21       		.uleb128 0x21
 2857 01af 16       		.uleb128 0x16
 2858 01b0 00       		.byte	0
 2859 01b1 03       		.uleb128 0x3
 2860 01b2 0E       		.uleb128 0xe
 2861 01b3 3A       		.uleb128 0x3a
 2862 01b4 0B       		.uleb128 0xb
 2863 01b5 3B       		.uleb128 0x3b
 2864 01b6 05       		.uleb128 0x5
 2865 01b7 49       		.uleb128 0x49
 2866 01b8 13       		.uleb128 0x13
 2867 01b9 32       		.uleb128 0x32
 2868 01ba 0B       		.uleb128 0xb
 2869 01bb 00       		.byte	0
 2870 01bc 00       		.byte	0
 2871 01bd 22       		.uleb128 0x22
 2872 01be 34       		.uleb128 0x34
 2873 01bf 00       		.byte	0
 2874 01c0 03       		.uleb128 0x3
 2875 01c1 0E       		.uleb128 0xe
 2876 01c2 3A       		.uleb128 0x3a
 2877 01c3 0B       		.uleb128 0xb
 2878 01c4 3B       		.uleb128 0x3b
 2879 01c5 0B       		.uleb128 0xb
 2880 01c6 49       		.uleb128 0x49
 2881 01c7 13       		.uleb128 0x13
 2882 01c8 3C       		.uleb128 0x3c
 2883 01c9 19       		.uleb128 0x19
 2884 01ca 00       		.byte	0
 2885 01cb 00       		.byte	0
 2886 01cc 23       		.uleb128 0x23
 2887 01cd 2E       		.uleb128 0x2e
 2888 01ce 01       		.byte	0x1
 2889 01cf 3F       		.uleb128 0x3f
 2890 01d0 19       		.uleb128 0x19
 2891 01d1 03       		.uleb128 0x3
 2892 01d2 0E       		.uleb128 0xe
 2893 01d3 3A       		.uleb128 0x3a
 2894 01d4 0B       		.uleb128 0xb
 2895 01d5 3B       		.uleb128 0x3b
 2896 01d6 05       		.uleb128 0x5
 2897 01d7 49       		.uleb128 0x49
 2898 01d8 13       		.uleb128 0x13
 2899 01d9 3C       		.uleb128 0x3c
 2900 01da 19       		.uleb128 0x19
 2901 01db 01       		.uleb128 0x1
 2902 01dc 13       		.uleb128 0x13
 2903 01dd 00       		.byte	0
 2904 01de 00       		.byte	0
GAS LISTING /tmp/ccLn5Wva.s 			page 54


 2905 01df 24       		.uleb128 0x24
 2906 01e0 05       		.uleb128 0x5
 2907 01e1 00       		.byte	0
 2908 01e2 49       		.uleb128 0x49
 2909 01e3 13       		.uleb128 0x13
 2910 01e4 00       		.byte	0
 2911 01e5 00       		.byte	0
 2912 01e6 25       		.uleb128 0x25
 2913 01e7 18       		.uleb128 0x18
 2914 01e8 00       		.byte	0
 2915 01e9 00       		.byte	0
 2916 01ea 00       		.byte	0
 2917 01eb 26       		.uleb128 0x26
 2918 01ec 2E       		.uleb128 0x2e
 2919 01ed 00       		.byte	0
 2920 01ee 3F       		.uleb128 0x3f
 2921 01ef 19       		.uleb128 0x19
 2922 01f0 03       		.uleb128 0x3
 2923 01f1 0E       		.uleb128 0xe
 2924 01f2 3A       		.uleb128 0x3a
 2925 01f3 0B       		.uleb128 0xb
 2926 01f4 3B       		.uleb128 0x3b
 2927 01f5 05       		.uleb128 0x5
 2928 01f6 49       		.uleb128 0x49
 2929 01f7 13       		.uleb128 0x13
 2930 01f8 3C       		.uleb128 0x3c
 2931 01f9 19       		.uleb128 0x19
 2932 01fa 00       		.byte	0
 2933 01fb 00       		.byte	0
 2934 01fc 27       		.uleb128 0x27
 2935 01fd 2E       		.uleb128 0x2e
 2936 01fe 01       		.byte	0x1
 2937 01ff 3F       		.uleb128 0x3f
 2938 0200 19       		.uleb128 0x19
 2939 0201 03       		.uleb128 0x3
 2940 0202 0E       		.uleb128 0xe
 2941 0203 3A       		.uleb128 0x3a
 2942 0204 0B       		.uleb128 0xb
 2943 0205 3B       		.uleb128 0x3b
 2944 0206 0B       		.uleb128 0xb
 2945 0207 49       		.uleb128 0x49
 2946 0208 13       		.uleb128 0x13
 2947 0209 3C       		.uleb128 0x3c
 2948 020a 19       		.uleb128 0x19
 2949 020b 01       		.uleb128 0x1
 2950 020c 13       		.uleb128 0x13
 2951 020d 00       		.byte	0
 2952 020e 00       		.byte	0
 2953 020f 28       		.uleb128 0x28
 2954 0210 13       		.uleb128 0x13
 2955 0211 01       		.byte	0x1
 2956 0212 03       		.uleb128 0x3
 2957 0213 08       		.uleb128 0x8
 2958 0214 0B       		.uleb128 0xb
 2959 0215 0B       		.uleb128 0xb
 2960 0216 3A       		.uleb128 0x3a
 2961 0217 0B       		.uleb128 0xb
GAS LISTING /tmp/ccLn5Wva.s 			page 55


 2962 0218 3B       		.uleb128 0x3b
 2963 0219 0B       		.uleb128 0xb
 2964 021a 01       		.uleb128 0x1
 2965 021b 13       		.uleb128 0x13
 2966 021c 00       		.byte	0
 2967 021d 00       		.byte	0
 2968 021e 29       		.uleb128 0x29
 2969 021f 2E       		.uleb128 0x2e
 2970 0220 01       		.byte	0x1
 2971 0221 3F       		.uleb128 0x3f
 2972 0222 19       		.uleb128 0x19
 2973 0223 03       		.uleb128 0x3
 2974 0224 0E       		.uleb128 0xe
 2975 0225 3A       		.uleb128 0x3a
 2976 0226 0B       		.uleb128 0xb
 2977 0227 3B       		.uleb128 0x3b
 2978 0228 0B       		.uleb128 0xb
 2979 0229 6E       		.uleb128 0x6e
 2980 022a 0E       		.uleb128 0xe
 2981 022b 49       		.uleb128 0x49
 2982 022c 13       		.uleb128 0x13
 2983 022d 3C       		.uleb128 0x3c
 2984 022e 19       		.uleb128 0x19
 2985 022f 01       		.uleb128 0x1
 2986 0230 13       		.uleb128 0x13
 2987 0231 00       		.byte	0
 2988 0232 00       		.byte	0
 2989 0233 2A       		.uleb128 0x2a
 2990 0234 2E       		.uleb128 0x2e
 2991 0235 01       		.byte	0x1
 2992 0236 3F       		.uleb128 0x3f
 2993 0237 19       		.uleb128 0x19
 2994 0238 03       		.uleb128 0x3
 2995 0239 0E       		.uleb128 0xe
 2996 023a 3A       		.uleb128 0x3a
 2997 023b 0B       		.uleb128 0xb
 2998 023c 3B       		.uleb128 0x3b
 2999 023d 05       		.uleb128 0x5
 3000 023e 6E       		.uleb128 0x6e
 3001 023f 0E       		.uleb128 0xe
 3002 0240 49       		.uleb128 0x49
 3003 0241 13       		.uleb128 0x13
 3004 0242 3C       		.uleb128 0x3c
 3005 0243 19       		.uleb128 0x19
 3006 0244 01       		.uleb128 0x1
 3007 0245 13       		.uleb128 0x13
 3008 0246 00       		.byte	0
 3009 0247 00       		.byte	0
 3010 0248 2B       		.uleb128 0x2b
 3011 0249 39       		.uleb128 0x39
 3012 024a 01       		.byte	0x1
 3013 024b 03       		.uleb128 0x3
 3014 024c 0E       		.uleb128 0xe
 3015 024d 3A       		.uleb128 0x3a
 3016 024e 0B       		.uleb128 0xb
 3017 024f 3B       		.uleb128 0x3b
 3018 0250 0B       		.uleb128 0xb
GAS LISTING /tmp/ccLn5Wva.s 			page 56


 3019 0251 01       		.uleb128 0x1
 3020 0252 13       		.uleb128 0x13
 3021 0253 00       		.byte	0
 3022 0254 00       		.byte	0
 3023 0255 2C       		.uleb128 0x2c
 3024 0256 0D       		.uleb128 0xd
 3025 0257 00       		.byte	0
 3026 0258 03       		.uleb128 0x3
 3027 0259 0E       		.uleb128 0xe
 3028 025a 3A       		.uleb128 0x3a
 3029 025b 0B       		.uleb128 0xb
 3030 025c 3B       		.uleb128 0x3b
 3031 025d 0B       		.uleb128 0xb
 3032 025e 49       		.uleb128 0x49
 3033 025f 13       		.uleb128 0x13
 3034 0260 3F       		.uleb128 0x3f
 3035 0261 19       		.uleb128 0x19
 3036 0262 3C       		.uleb128 0x3c
 3037 0263 19       		.uleb128 0x19
 3038 0264 00       		.byte	0
 3039 0265 00       		.byte	0
 3040 0266 2D       		.uleb128 0x2d
 3041 0267 2F       		.uleb128 0x2f
 3042 0268 00       		.byte	0
 3043 0269 03       		.uleb128 0x3
 3044 026a 0E       		.uleb128 0xe
 3045 026b 49       		.uleb128 0x49
 3046 026c 13       		.uleb128 0x13
 3047 026d 00       		.byte	0
 3048 026e 00       		.byte	0
 3049 026f 2E       		.uleb128 0x2e
 3050 0270 13       		.uleb128 0x13
 3051 0271 01       		.byte	0x1
 3052 0272 03       		.uleb128 0x3
 3053 0273 0E       		.uleb128 0xe
 3054 0274 0B       		.uleb128 0xb
 3055 0275 0B       		.uleb128 0xb
 3056 0276 3A       		.uleb128 0x3a
 3057 0277 0B       		.uleb128 0xb
 3058 0278 3B       		.uleb128 0x3b
 3059 0279 0B       		.uleb128 0xb
 3060 027a 00       		.byte	0
 3061 027b 00       		.byte	0
 3062 027c 2F       		.uleb128 0x2f
 3063 027d 3A       		.uleb128 0x3a
 3064 027e 00       		.byte	0
 3065 027f 3A       		.uleb128 0x3a
 3066 0280 0B       		.uleb128 0xb
 3067 0281 3B       		.uleb128 0x3b
 3068 0282 0B       		.uleb128 0xb
 3069 0283 18       		.uleb128 0x18
 3070 0284 13       		.uleb128 0x13
 3071 0285 00       		.byte	0
 3072 0286 00       		.byte	0
 3073 0287 30       		.uleb128 0x30
 3074 0288 2E       		.uleb128 0x2e
 3075 0289 00       		.byte	0
GAS LISTING /tmp/ccLn5Wva.s 			page 57


 3076 028a 3F       		.uleb128 0x3f
 3077 028b 19       		.uleb128 0x19
 3078 028c 03       		.uleb128 0x3
 3079 028d 0E       		.uleb128 0xe
 3080 028e 3A       		.uleb128 0x3a
 3081 028f 0B       		.uleb128 0xb
 3082 0290 3B       		.uleb128 0x3b
 3083 0291 0B       		.uleb128 0xb
 3084 0292 49       		.uleb128 0x49
 3085 0293 13       		.uleb128 0x13
 3086 0294 3C       		.uleb128 0x3c
 3087 0295 19       		.uleb128 0x19
 3088 0296 00       		.byte	0
 3089 0297 00       		.byte	0
 3090 0298 31       		.uleb128 0x31
 3091 0299 2E       		.uleb128 0x2e
 3092 029a 01       		.byte	0x1
 3093 029b 3F       		.uleb128 0x3f
 3094 029c 19       		.uleb128 0x19
 3095 029d 03       		.uleb128 0x3
 3096 029e 0E       		.uleb128 0xe
 3097 029f 3A       		.uleb128 0x3a
 3098 02a0 0B       		.uleb128 0xb
 3099 02a1 3B       		.uleb128 0x3b
 3100 02a2 0B       		.uleb128 0xb
 3101 02a3 6E       		.uleb128 0x6e
 3102 02a4 0E       		.uleb128 0xe
 3103 02a5 49       		.uleb128 0x49
 3104 02a6 13       		.uleb128 0x13
 3105 02a7 11       		.uleb128 0x11
 3106 02a8 01       		.uleb128 0x1
 3107 02a9 12       		.uleb128 0x12
 3108 02aa 07       		.uleb128 0x7
 3109 02ab 40       		.uleb128 0x40
 3110 02ac 18       		.uleb128 0x18
 3111 02ad 9742     		.uleb128 0x2117
 3112 02af 19       		.uleb128 0x19
 3113 02b0 01       		.uleb128 0x1
 3114 02b1 13       		.uleb128 0x13
 3115 02b2 00       		.byte	0
 3116 02b3 00       		.byte	0
 3117 02b4 32       		.uleb128 0x32
 3118 02b5 05       		.uleb128 0x5
 3119 02b6 00       		.byte	0
 3120 02b7 03       		.uleb128 0x3
 3121 02b8 08       		.uleb128 0x8
 3122 02b9 3A       		.uleb128 0x3a
 3123 02ba 0B       		.uleb128 0xb
 3124 02bb 3B       		.uleb128 0x3b
 3125 02bc 0B       		.uleb128 0xb
 3126 02bd 49       		.uleb128 0x49
 3127 02be 13       		.uleb128 0x13
 3128 02bf 02       		.uleb128 0x2
 3129 02c0 18       		.uleb128 0x18
 3130 02c1 00       		.byte	0
 3131 02c2 00       		.byte	0
 3132 02c3 33       		.uleb128 0x33
GAS LISTING /tmp/ccLn5Wva.s 			page 58


 3133 02c4 2E       		.uleb128 0x2e
 3134 02c5 01       		.byte	0x1
 3135 02c6 3F       		.uleb128 0x3f
 3136 02c7 19       		.uleb128 0x19
 3137 02c8 03       		.uleb128 0x3
 3138 02c9 0E       		.uleb128 0xe
 3139 02ca 3A       		.uleb128 0x3a
 3140 02cb 0B       		.uleb128 0xb
 3141 02cc 3B       		.uleb128 0x3b
 3142 02cd 0B       		.uleb128 0xb
 3143 02ce 49       		.uleb128 0x49
 3144 02cf 13       		.uleb128 0x13
 3145 02d0 11       		.uleb128 0x11
 3146 02d1 01       		.uleb128 0x1
 3147 02d2 12       		.uleb128 0x12
 3148 02d3 07       		.uleb128 0x7
 3149 02d4 40       		.uleb128 0x40
 3150 02d5 18       		.uleb128 0x18
 3151 02d6 9642     		.uleb128 0x2116
 3152 02d8 19       		.uleb128 0x19
 3153 02d9 01       		.uleb128 0x1
 3154 02da 13       		.uleb128 0x13
 3155 02db 00       		.byte	0
 3156 02dc 00       		.byte	0
 3157 02dd 34       		.uleb128 0x34
 3158 02de 0B       		.uleb128 0xb
 3159 02df 01       		.byte	0x1
 3160 02e0 11       		.uleb128 0x11
 3161 02e1 01       		.uleb128 0x1
 3162 02e2 12       		.uleb128 0x12
 3163 02e3 07       		.uleb128 0x7
 3164 02e4 00       		.byte	0
 3165 02e5 00       		.byte	0
 3166 02e6 35       		.uleb128 0x35
 3167 02e7 34       		.uleb128 0x34
 3168 02e8 00       		.byte	0
 3169 02e9 03       		.uleb128 0x3
 3170 02ea 08       		.uleb128 0x8
 3171 02eb 3A       		.uleb128 0x3a
 3172 02ec 0B       		.uleb128 0xb
 3173 02ed 3B       		.uleb128 0x3b
 3174 02ee 0B       		.uleb128 0xb
 3175 02ef 49       		.uleb128 0x49
 3176 02f0 13       		.uleb128 0x13
 3177 02f1 02       		.uleb128 0x2
 3178 02f2 18       		.uleb128 0x18
 3179 02f3 00       		.byte	0
 3180 02f4 00       		.byte	0
 3181 02f5 36       		.uleb128 0x36
 3182 02f6 2E       		.uleb128 0x2e
 3183 02f7 01       		.byte	0x1
 3184 02f8 03       		.uleb128 0x3
 3185 02f9 0E       		.uleb128 0xe
 3186 02fa 34       		.uleb128 0x34
 3187 02fb 19       		.uleb128 0x19
 3188 02fc 11       		.uleb128 0x11
 3189 02fd 01       		.uleb128 0x1
GAS LISTING /tmp/ccLn5Wva.s 			page 59


 3190 02fe 12       		.uleb128 0x12
 3191 02ff 07       		.uleb128 0x7
 3192 0300 40       		.uleb128 0x40
 3193 0301 18       		.uleb128 0x18
 3194 0302 9642     		.uleb128 0x2116
 3195 0304 19       		.uleb128 0x19
 3196 0305 01       		.uleb128 0x1
 3197 0306 13       		.uleb128 0x13
 3198 0307 00       		.byte	0
 3199 0308 00       		.byte	0
 3200 0309 37       		.uleb128 0x37
 3201 030a 05       		.uleb128 0x5
 3202 030b 00       		.byte	0
 3203 030c 03       		.uleb128 0x3
 3204 030d 0E       		.uleb128 0xe
 3205 030e 3A       		.uleb128 0x3a
 3206 030f 0B       		.uleb128 0xb
 3207 0310 3B       		.uleb128 0x3b
 3208 0311 0B       		.uleb128 0xb
 3209 0312 49       		.uleb128 0x49
 3210 0313 13       		.uleb128 0x13
 3211 0314 02       		.uleb128 0x2
 3212 0315 18       		.uleb128 0x18
 3213 0316 00       		.byte	0
 3214 0317 00       		.byte	0
 3215 0318 38       		.uleb128 0x38
 3216 0319 2E       		.uleb128 0x2e
 3217 031a 00       		.byte	0
 3218 031b 03       		.uleb128 0x3
 3219 031c 0E       		.uleb128 0xe
 3220 031d 34       		.uleb128 0x34
 3221 031e 19       		.uleb128 0x19
 3222 031f 11       		.uleb128 0x11
 3223 0320 01       		.uleb128 0x1
 3224 0321 12       		.uleb128 0x12
 3225 0322 07       		.uleb128 0x7
 3226 0323 40       		.uleb128 0x40
 3227 0324 18       		.uleb128 0x18
 3228 0325 9642     		.uleb128 0x2116
 3229 0327 19       		.uleb128 0x19
 3230 0328 00       		.byte	0
 3231 0329 00       		.byte	0
 3232 032a 39       		.uleb128 0x39
 3233 032b 34       		.uleb128 0x34
 3234 032c 00       		.byte	0
 3235 032d 03       		.uleb128 0x3
 3236 032e 0E       		.uleb128 0xe
 3237 032f 49       		.uleb128 0x49
 3238 0330 13       		.uleb128 0x13
 3239 0331 3F       		.uleb128 0x3f
 3240 0332 19       		.uleb128 0x19
 3241 0333 34       		.uleb128 0x34
 3242 0334 19       		.uleb128 0x19
 3243 0335 3C       		.uleb128 0x3c
 3244 0336 19       		.uleb128 0x19
 3245 0337 00       		.byte	0
 3246 0338 00       		.byte	0
GAS LISTING /tmp/ccLn5Wva.s 			page 60


 3247 0339 3A       		.uleb128 0x3a
 3248 033a 34       		.uleb128 0x34
 3249 033b 00       		.byte	0
 3250 033c 47       		.uleb128 0x47
 3251 033d 13       		.uleb128 0x13
 3252 033e 02       		.uleb128 0x2
 3253 033f 18       		.uleb128 0x18
 3254 0340 00       		.byte	0
 3255 0341 00       		.byte	0
 3256 0342 3B       		.uleb128 0x3b
 3257 0343 34       		.uleb128 0x34
 3258 0344 00       		.byte	0
 3259 0345 47       		.uleb128 0x47
 3260 0346 13       		.uleb128 0x13
 3261 0347 6E       		.uleb128 0x6e
 3262 0348 0E       		.uleb128 0xe
 3263 0349 1C       		.uleb128 0x1c
 3264 034a 0D       		.uleb128 0xd
 3265 034b 00       		.byte	0
 3266 034c 00       		.byte	0
 3267 034d 3C       		.uleb128 0x3c
 3268 034e 34       		.uleb128 0x34
 3269 034f 00       		.byte	0
 3270 0350 47       		.uleb128 0x47
 3271 0351 13       		.uleb128 0x13
 3272 0352 6E       		.uleb128 0x6e
 3273 0353 0E       		.uleb128 0xe
 3274 0354 1C       		.uleb128 0x1c
 3275 0355 06       		.uleb128 0x6
 3276 0356 00       		.byte	0
 3277 0357 00       		.byte	0
 3278 0358 3D       		.uleb128 0x3d
 3279 0359 34       		.uleb128 0x34
 3280 035a 00       		.byte	0
 3281 035b 47       		.uleb128 0x47
 3282 035c 13       		.uleb128 0x13
 3283 035d 6E       		.uleb128 0x6e
 3284 035e 0E       		.uleb128 0xe
 3285 035f 1C       		.uleb128 0x1c
 3286 0360 0B       		.uleb128 0xb
 3287 0361 00       		.byte	0
 3288 0362 00       		.byte	0
 3289 0363 3E       		.uleb128 0x3e
 3290 0364 34       		.uleb128 0x34
 3291 0365 00       		.byte	0
 3292 0366 47       		.uleb128 0x47
 3293 0367 13       		.uleb128 0x13
 3294 0368 6E       		.uleb128 0x6e
 3295 0369 0E       		.uleb128 0xe
 3296 036a 1C       		.uleb128 0x1c
 3297 036b 05       		.uleb128 0x5
 3298 036c 00       		.byte	0
 3299 036d 00       		.byte	0
 3300 036e 3F       		.uleb128 0x3f
 3301 036f 34       		.uleb128 0x34
 3302 0370 00       		.byte	0
 3303 0371 47       		.uleb128 0x47
GAS LISTING /tmp/ccLn5Wva.s 			page 61


 3304 0372 13       		.uleb128 0x13
 3305 0373 6E       		.uleb128 0x6e
 3306 0374 0E       		.uleb128 0xe
 3307 0375 1C       		.uleb128 0x1c
 3308 0376 07       		.uleb128 0x7
 3309 0377 00       		.byte	0
 3310 0378 00       		.byte	0
 3311 0379 00       		.byte	0
 3312              		.section	.debug_aranges,"",@progbits
 3313 0000 2C000000 		.long	0x2c
 3314 0004 0200     		.value	0x2
 3315 0006 00000000 		.long	.Ldebug_info0
 3316 000a 08       		.byte	0x8
 3317 000b 00       		.byte	0
 3318 000c 0000     		.value	0
 3319 000e 0000     		.value	0
 3320 0010 00000000 		.quad	.Ltext0
 3320      00000000 
 3321 0018 85000000 		.quad	.Letext0-.Ltext0
 3321      00000000 
 3322 0020 00000000 		.quad	0
 3322      00000000 
 3323 0028 00000000 		.quad	0
 3323      00000000 
 3324              		.section	.debug_line,"",@progbits
 3325              	.Ldebug_line0:
 3326 0000 42020000 		.section	.debug_str,"MS",@progbits,1
 3326      02001002 
 3326      00000101 
 3326      FB0E0D00 
 3326      01010101 
 3327              	.LASF56:
 3328 0000 5F535F65 		.string	"_S_end"
 3328      6E6400
 3329              	.LASF7:
 3330 0007 73697A65 		.string	"size_t"
 3330      5F7400
 3331              	.LASF4:
 3332 000e 73697A65 		.string	"sizetype"
 3332      74797065 
 3332      00
 3333              	.LASF121:
 3334 0017 746D5F68 		.string	"tm_hour"
 3334      6F757200 
 3335              	.LASF13:
 3336 001f 5F5F7661 		.string	"__value"
 3336      6C756500 
 3337              	.LASF161:
 3338 0027 5F5F6E75 		.string	"__numeric_traits_integer<int>"
 3338      6D657269 
 3338      635F7472 
 3338      61697473 
 3338      5F696E74 
 3339              	.LASF222:
 3340 0045 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIiE5__maxE"
 3340      5F5F676E 
 3340      755F6378 
GAS LISTING /tmp/ccLn5Wva.s 			page 62


 3340      7832345F 
 3340      5F6E756D 
 3341              	.LASF60:
 3342 0077 626F6F6C 		.string	"boolalpha"
 3342      616C7068 
 3342      6100
 3343              	.LASF65:
 3344 0081 73636965 		.string	"scientific"
 3344      6E746966 
 3344      696300
 3345              	.LASF163:
 3346 008c 5F5F6D61 		.string	"__max"
 3346      7800
 3347              	.LASF116:
 3348 0092 77637363 		.string	"wcscspn"
 3348      73706E00 
 3349              	.LASF208:
 3350 009a 6C6F6361 		.string	"localeconv"
 3350      6C65636F 
 3350      6E7600
 3351              	.LASF237:
 3352 00a5 5F5A3466 		.string	"_Z4funci"
 3352      756E6369 
 3352      00
 3353              	.LASF193:
 3354 00ae 66726163 		.string	"frac_digits"
 3354      5F646967 
 3354      69747300 
 3355              	.LASF230:
 3356 00ba 65785F31 		.string	"ex_10.cpp"
 3356      302E6370 
 3356      7000
 3357              	.LASF185:
 3358 00c4 696E745F 		.string	"int_curr_symbol"
 3358      63757272 
 3358      5F73796D 
 3358      626F6C00 
 3359              	.LASF80:
 3360 00d4 676F6F64 		.string	"goodbit"
 3360      62697400 
 3361              	.LASF153:
 3362 00dc 77637363 		.string	"wcschr"
 3362      687200
 3363              	.LASF19:
 3364 00e3 5F535F62 		.string	"_S_boolalpha"
 3364      6F6F6C61 
 3364      6C706861 
 3364      00
 3365              	.LASF79:
 3366 00f0 6661696C 		.string	"failbit"
 3366      62697400 
 3367              	.LASF196:
 3368 00f8 6E5F6373 		.string	"n_cs_precedes"
 3368      5F707265 
 3368      63656465 
 3368      7300
 3369              	.LASF97:
GAS LISTING /tmp/ccLn5Wva.s 			page 63


 3370 0106 6D627274 		.string	"mbrtowc"
 3370      6F776300 
 3371              	.LASF144:
 3372 010e 77637378 		.string	"wcsxfrm"
 3372      66726D00 
 3373              	.LASF192:
 3374 0116 696E745F 		.string	"int_frac_digits"
 3374      66726163 
 3374      5F646967 
 3374      69747300 
 3375              	.LASF240:
 3376 0126 5F474C4F 		.string	"_GLOBAL__sub_I__Z4funci"
 3376      42414C5F 
 3376      5F737562 
 3376      5F495F5F 
 3376      5A346675 
 3377              	.LASF54:
 3378 013e 5F535F62 		.string	"_S_beg"
 3378      656700
 3379              	.LASF114:
 3380 0145 77637363 		.string	"wcscoll"
 3380      6F6C6C00 
 3381              	.LASF69:
 3382 014d 736B6970 		.string	"skipws"
 3382      777300
 3383              	.LASF10:
 3384 0154 5F5F7763 		.string	"__wch"
 3384      6800
 3385              	.LASF71:
 3386 015a 75707065 		.string	"uppercase"
 3386      72636173 
 3386      6500
 3387              	.LASF35:
 3388 0164 5F535F62 		.string	"_S_basefield"
 3388      61736566 
 3388      69656C64 
 3388      00
 3389              	.LASF155:
 3390 0171 77637372 		.string	"wcsrchr"
 3390      63687200 
 3391              	.LASF187:
 3392 0179 6D6F6E5F 		.string	"mon_decimal_point"
 3392      64656369 
 3392      6D616C5F 
 3392      706F696E 
 3392      7400
 3393              	.LASF142:
 3394 018b 6C6F6E67 		.string	"long int"
 3394      20696E74 
 3394      00
 3395              	.LASF168:
 3396 0194 5F5F6E75 		.string	"__numeric_traits_integer<char>"
 3396      6D657269 
 3396      635F7472 
 3396      61697473 
 3396      5F696E74 
 3397              	.LASF109:
GAS LISTING /tmp/ccLn5Wva.s 			page 64


 3398 01b3 76777072 		.string	"vwprintf"
 3398      696E7466 
 3398      00
 3399              	.LASF39:
 3400 01bc 5F496F73 		.string	"_Ios_Openmode"
 3400      5F4F7065 
 3400      6E6D6F64 
 3400      6500
 3401              	.LASF238:
 3402 01ca 6D61696E 		.string	"main"
 3402      00
 3403              	.LASF202:
 3404 01cf 696E745F 		.string	"int_n_cs_precedes"
 3404      6E5F6373 
 3404      5F707265 
 3404      63656465 
 3404      7300
 3405              	.LASF214:
 3406 01e1 746F7763 		.string	"towctrans"
 3406      7472616E 
 3406      7300
 3407              	.LASF16:
 3408 01eb 6D627374 		.string	"mbstate_t"
 3408      6174655F 
 3408      7400
 3409              	.LASF217:
 3410 01f5 5F5F696F 		.string	"__ioinit"
 3410      696E6974 
 3410      00
 3411              	.LASF49:
 3412 01fe 5F535F62 		.string	"_S_badbit"
 3412      61646269 
 3412      7400
 3413              	.LASF59:
 3414 0208 5F535F73 		.string	"_S_synced_with_stdio"
 3414      796E6365 
 3414      645F7769 
 3414      74685F73 
 3414      7464696F 
 3415              	.LASF167:
 3416 021d 5F56616C 		.string	"_Value"
 3416      756500
 3417              	.LASF50:
 3418 0224 5F535F65 		.string	"_S_eofbit"
 3418      6F666269 
 3418      7400
 3419              	.LASF126:
 3420 022e 746D5F79 		.string	"tm_yday"
 3420      64617900 
 3421              	.LASF177:
 3422 0236 7369676E 		.string	"signed char"
 3422      65642063 
 3422      68617200 
 3423              	.LASF232:
 3424 0242 5F494F5F 		.string	"_IO_FILE"
 3424      46494C45 
 3424      00
GAS LISTING /tmp/ccLn5Wva.s 			page 65


 3425              	.LASF211:
 3426 024b 77637479 		.string	"wctype_t"
 3426      70655F74 
 3426      00
 3427              	.LASF87:
 3428 0254 66676574 		.string	"fgetwc"
 3428      776300
 3429              	.LASF207:
 3430 025b 67657477 		.string	"getwchar"
 3430      63686172 
 3430      00
 3431              	.LASF88:
 3432 0264 66676574 		.string	"fgetws"
 3432      777300
 3433              	.LASF26:
 3434 026b 5F535F72 		.string	"_S_right"
 3434      69676874 
 3434      00
 3435              	.LASF176:
 3436 0274 756E7369 		.string	"unsigned char"
 3436      676E6564 
 3436      20636861 
 3436      7200
 3437              	.LASF197:
 3438 0282 6E5F7365 		.string	"n_sep_by_space"
 3438      705F6279 
 3438      5F737061 
 3438      636500
 3439              	.LASF157:
 3440 0291 776D656D 		.string	"wmemchr"
 3440      63687200 
 3441              	.LASF48:
 3442 0299 5F535F67 		.string	"_S_goodbit"
 3442      6F6F6462 
 3442      697400
 3443              	.LASF226:
 3444 02a4 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIsE5__maxE"
 3444      5F5F676E 
 3444      755F6378 
 3444      7832345F 
 3444      5F6E756D 
 3445              	.LASF42:
 3446 02d6 5F535F62 		.string	"_S_bin"
 3446      696E00
 3447              	.LASF113:
 3448 02dd 77637363 		.string	"wcscmp"
 3448      6D7000
 3449              	.LASF102:
 3450 02e4 73777072 		.string	"swprintf"
 3450      696E7466 
 3450      00
 3451              	.LASF154:
 3452 02ed 77637370 		.string	"wcspbrk"
 3452      62726B00 
 3453              	.LASF228:
 3454 02f5 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIlE5__maxE"
 3454      5F5F676E 
GAS LISTING /tmp/ccLn5Wva.s 			page 66


 3454      755F6378 
 3454      7832345F 
 3454      5F6E756D 
 3455              	.LASF164:
 3456 0327 5F5F6973 		.string	"__is_signed"
 3456      5F736967 
 3456      6E656400 
 3457              	.LASF44:
 3458 0333 5F535F6F 		.string	"_S_out"
 3458      757400
 3459              	.LASF14:
 3460 033a 63686172 		.string	"char"
 3460      00
 3461              	.LASF40:
 3462 033f 5F535F61 		.string	"_S_app"
 3462      707000
 3463              	.LASF216:
 3464 0346 77637479 		.string	"wctype"
 3464      706500
 3465              	.LASF81:
 3466 034d 6F70656E 		.string	"openmode"
 3466      6D6F6465 
 3466      00
 3467              	.LASF132:
 3468 0356 7763736E 		.string	"wcsncmp"
 3468      636D7000 
 3469              	.LASF205:
 3470 035e 696E745F 		.string	"int_n_sign_posn"
 3470      6E5F7369 
 3470      676E5F70 
 3470      6F736E00 
 3471              	.LASF199:
 3472 036e 6E5F7369 		.string	"n_sign_posn"
 3472      676E5F70 
 3472      6F736E00 
 3473              	.LASF148:
 3474 037a 776D656D 		.string	"wmemmove"
 3474      6D6F7665 
 3474      00
 3475              	.LASF162:
 3476 0383 5F5F6D69 		.string	"__min"
 3476      6E00
 3477              	.LASF86:
 3478 0389 62746F77 		.string	"btowc"
 3478      6300
 3479              	.LASF151:
 3480 038f 77736361 		.string	"wscanf"
 3480      6E6600
 3481              	.LASF188:
 3482 0396 6D6F6E5F 		.string	"mon_thousands_sep"
 3482      74686F75 
 3482      73616E64 
 3482      735F7365 
 3482      7000
 3483              	.LASF104:
 3484 03a8 756E6765 		.string	"ungetwc"
 3484      74776300 
GAS LISTING /tmp/ccLn5Wva.s 			page 67


 3485              	.LASF1:
 3486 03b0 66705F6F 		.string	"fp_offset"
 3486      66667365 
 3486      7400
 3487              	.LASF18:
 3488 03ba 70747264 		.string	"ptrdiff_t"
 3488      6966665F 
 3488      7400
 3489              	.LASF221:
 3490 03c4 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIiE5__minE"
 3490      5F5F676E 
 3490      755F6378 
 3490      7832345F 
 3490      5F6E756D 
 3491              	.LASF212:
 3492 03f6 77637472 		.string	"wctrans_t"
 3492      616E735F 
 3492      7400
 3493              	.LASF96:
 3494 0400 6D62726C 		.string	"mbrlen"
 3494      656E00
 3495              	.LASF191:
 3496 0407 6E656761 		.string	"negative_sign"
 3496      74697665 
 3496      5F736967 
 3496      6E00
 3497              	.LASF22:
 3498 0415 5F535F68 		.string	"_S_hex"
 3498      657800
 3499              	.LASF200:
 3500 041c 696E745F 		.string	"int_p_cs_precedes"
 3500      705F6373 
 3500      5F707265 
 3500      63656465 
 3500      7300
 3501              	.LASF93:
 3502 042e 66777072 		.string	"fwprintf"
 3502      696E7466 
 3502      00
 3503              	.LASF174:
 3504 0437 77637374 		.string	"wcstoull"
 3504      6F756C6C 
 3504      00
 3505              	.LASF23:
 3506 0440 5F535F69 		.string	"_S_internal"
 3506      6E746572 
 3506      6E616C00 
 3507              	.LASF190:
 3508 044c 706F7369 		.string	"positive_sign"
 3508      74697665 
 3508      5F736967 
 3508      6E00
 3509              	.LASF122:
 3510 045a 746D5F6D 		.string	"tm_mday"
 3510      64617900 
 3511              	.LASF73:
 3512 0462 62617365 		.string	"basefield"
GAS LISTING /tmp/ccLn5Wva.s 			page 68


 3512      6669656C 
 3512      6400
 3513              	.LASF115:
 3514 046c 77637363 		.string	"wcscpy"
 3514      707900
 3515              	.LASF61:
 3516 0473 66697865 		.string	"fixed"
 3516      6400
 3517              	.LASF107:
 3518 0479 76737770 		.string	"vswprintf"
 3518      72696E74 
 3518      6600
 3519              	.LASF149:
 3520 0483 776D656D 		.string	"wmemset"
 3520      73657400 
 3521              	.LASF84:
 3522 048b 7365656B 		.string	"seekdir"
 3522      64697200 
 3523              	.LASF92:
 3524 0493 66776964 		.string	"fwide"
 3524      6500
 3525              	.LASF63:
 3526 0499 6C656674 		.string	"left"
 3526      00
 3527              	.LASF119:
 3528 049e 746D5F73 		.string	"tm_sec"
 3528      656300
 3529              	.LASF231:
 3530 04a5 2F686F6D 		.string	"/home/mridul/Private/coding/CPP/code/TICPP/Vol_1/chap_11/ex_10"
 3530      652F6D72 
 3530      6964756C 
 3530      2F507269 
 3530      76617465 
 3531              	.LASF127:
 3532 04e4 746D5F69 		.string	"tm_isdst"
 3532      73647374 
 3532      00
 3533              	.LASF133:
 3534 04ed 7763736E 		.string	"wcsncpy"
 3534      63707900 
 3535              	.LASF101:
 3536 04f5 70757477 		.string	"putwchar"
 3536      63686172 
 3536      00
 3537              	.LASF146:
 3538 04fe 776D656D 		.string	"wmemcmp"
 3538      636D7000 
 3539              	.LASF41:
 3540 0506 5F535F61 		.string	"_S_ate"
 3540      746500
 3541              	.LASF21:
 3542 050d 5F535F66 		.string	"_S_fixed"
 3542      69786564 
 3542      00
 3543              	.LASF203:
 3544 0516 696E745F 		.string	"int_n_sep_by_space"
 3544      6E5F7365 
GAS LISTING /tmp/ccLn5Wva.s 			page 69


 3544      705F6279 
 3544      5F737061 
 3544      636500
 3545              	.LASF219:
 3546 0529 5F5F7072 		.string	"__priority"
 3546      696F7269 
 3546      747900
 3547              	.LASF8:
 3548 0534 6C6F6E67 		.string	"long unsigned int"
 3548      20756E73 
 3548      69676E65 
 3548      6420696E 
 3548      7400
 3549              	.LASF28:
 3550 0546 5F535F73 		.string	"_S_showbase"
 3550      686F7762 
 3550      61736500 
 3551              	.LASF43:
 3552 0552 5F535F69 		.string	"_S_in"
 3552      6E00
 3553              	.LASF179:
 3554 0558 5F5F676E 		.string	"__gnu_debug"
 3554      755F6465 
 3554      62756700 
 3555              	.LASF105:
 3556 0564 76667770 		.string	"vfwprintf"
 3556      72696E74 
 3556      6600
 3557              	.LASF108:
 3558 056e 76737773 		.string	"vswscanf"
 3558      63616E66 
 3558      00
 3559              	.LASF195:
 3560 0577 705F7365 		.string	"p_sep_by_space"
 3560      705F6279 
 3560      5F737061 
 3560      636500
 3561              	.LASF58:
 3562 0586 5F535F72 		.string	"_S_refcount"
 3562      6566636F 
 3562      756E7400 
 3563              	.LASF45:
 3564 0592 5F535F74 		.string	"_S_trunc"
 3564      72756E63 
 3564      00
 3565              	.LASF218:
 3566 059b 5F5F696E 		.string	"__initialize_p"
 3566      69746961 
 3566      6C697A65 
 3566      5F7000
 3567              	.LASF64:
 3568 05aa 72696768 		.string	"right"
 3568      7400
 3569              	.LASF30:
 3570 05b0 5F535F73 		.string	"_S_showpos"
 3570      686F7770 
 3570      6F7300
GAS LISTING /tmp/ccLn5Wva.s 			page 70


 3571              	.LASF15:
 3572 05bb 5F5F6D62 		.string	"__mbstate_t"
 3572      73746174 
 3572      655F7400 
 3573              	.LASF147:
 3574 05c7 776D656D 		.string	"wmemcpy"
 3574      63707900 
 3575              	.LASF123:
 3576 05cf 746D5F6D 		.string	"tm_mon"
 3576      6F6E00
 3577              	.LASF20:
 3578 05d6 5F535F64 		.string	"_S_dec"
 3578      656300
 3579              	.LASF38:
 3580 05dd 5F496F73 		.string	"_Ios_Fmtflags"
 3580      5F466D74 
 3580      666C6167 
 3580      7300
 3581              	.LASF137:
 3582 05eb 646F7562 		.string	"double"
 3582      6C6500
 3583              	.LASF62:
 3584 05f2 696E7465 		.string	"internal"
 3584      726E616C 
 3584      00
 3585              	.LASF225:
 3586 05fb 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIsE5__minE"
 3586      5F5F676E 
 3586      755F6378 
 3586      7832345F 
 3586      5F6E756D 
 3587              	.LASF145:
 3588 062d 7763746F 		.string	"wctob"
 3588      6200
 3589              	.LASF29:
 3590 0633 5F535F73 		.string	"_S_showpoint"
 3590      686F7770 
 3590      6F696E74 
 3590      00
 3591              	.LASF31:
 3592 0640 5F535F73 		.string	"_S_skipws"
 3592      6B697077 
 3592      7300
 3593              	.LASF0:
 3594 064a 67705F6F 		.string	"gp_offset"
 3594      66667365 
 3594      7400
 3595              	.LASF34:
 3596 0654 5F535F61 		.string	"_S_adjustfield"
 3596      646A7573 
 3596      74666965 
 3596      6C6400
 3597              	.LASF223:
 3598 0663 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerImE8__digitsE"
 3598      5F5F676E 
 3598      755F6378 
 3598      7832345F 
GAS LISTING /tmp/ccLn5Wva.s 			page 71


 3598      5F6E756D 
 3599              	.LASF139:
 3600 0698 666C6F61 		.string	"float"
 3600      7400
 3601              	.LASF120:
 3602 069e 746D5F6D 		.string	"tm_min"
 3602      696E00
 3603              	.LASF24:
 3604 06a5 5F535F6C 		.string	"_S_left"
 3604      65667400 
 3605              	.LASF5:
 3606 06ad 756E7369 		.string	"unsigned int"
 3606      676E6564 
 3606      20696E74 
 3606      00
 3607              	.LASF46:
 3608 06ba 5F535F69 		.string	"_S_ios_openmode_end"
 3608      6F735F6F 
 3608      70656E6D 
 3608      6F64655F 
 3608      656E6400 
 3609              	.LASF135:
 3610 06ce 77637373 		.string	"wcsspn"
 3610      706E00
 3611              	.LASF198:
 3612 06d5 705F7369 		.string	"p_sign_posn"
 3612      676E5F70 
 3612      6F736E00 
 3613              	.LASF229:
 3614 06e1 474E5520 		.string	"GNU C++ 4.9.2 -mtune=generic -march=x86-64 -g"
 3614      432B2B20 
 3614      342E392E 
 3614      32202D6D 
 3614      74756E65 
 3615              	.LASF33:
 3616 070f 5F535F75 		.string	"_S_uppercase"
 3616      70706572 
 3616      63617365 
 3616      00
 3617              	.LASF210:
 3618 071c 5F41746F 		.string	"_Atomic_word"
 3618      6D69635F 
 3618      776F7264 
 3618      00
 3619              	.LASF66:
 3620 0729 73686F77 		.string	"showbase"
 3620      62617365 
 3620      00
 3621              	.LASF2:
 3622 0732 6F766572 		.string	"overflow_arg_area"
 3622      666C6F77 
 3622      5F617267 
 3622      5F617265 
 3622      6100
 3623              	.LASF37:
 3624 0744 5F535F69 		.string	"_S_ios_fmtflags_end"
 3624      6F735F66 
GAS LISTING /tmp/ccLn5Wva.s 			page 72


 3624      6D74666C 
 3624      6167735F 
 3624      656E6400 
 3625              	.LASF152:
 3626 0758 496E6974 		.string	"Init"
 3626      00
 3627              	.LASF182:
 3628 075d 64656369 		.string	"decimal_point"
 3628      6D616C5F 
 3628      706F696E 
 3628      7400
 3629              	.LASF12:
 3630 076b 5F5F636F 		.string	"__count"
 3630      756E7400 
 3631              	.LASF158:
 3632 0773 5F5F676E 		.string	"__gnu_cxx"
 3632      755F6378 
 3632      7800
 3633              	.LASF180:
 3634 077d 626F6F6C 		.string	"bool"
 3634      00
 3635              	.LASF171:
 3636 0782 6C6F6E67 		.string	"long double"
 3636      20646F75 
 3636      626C6500 
 3637              	.LASF100:
 3638 078e 70757477 		.string	"putwc"
 3638      6300
 3639              	.LASF235:
 3640 0794 5F5F6E75 		.string	"__numeric_traits_integer<long int>"
 3640      6D657269 
 3640      635F7472 
 3640      61697473 
 3640      5F696E74 
 3641              	.LASF68:
 3642 07b7 73686F77 		.string	"showpos"
 3642      706F7300 
 3643              	.LASF36:
 3644 07bf 5F535F66 		.string	"_S_floatfield"
 3644      6C6F6174 
 3644      6669656C 
 3644      6400
 3645              	.LASF25:
 3646 07cd 5F535F6F 		.string	"_S_oct"
 3646      637400
 3647              	.LASF11:
 3648 07d4 5F5F7763 		.string	"__wchb"
 3648      686200
 3649              	.LASF82:
 3650 07db 62696E61 		.string	"binary"
 3650      727900
 3651              	.LASF239:
 3652 07e2 5F5F7374 		.string	"__static_initialization_and_destruction_0"
 3652      61746963 
 3652      5F696E69 
 3652      7469616C 
 3652      697A6174 
GAS LISTING /tmp/ccLn5Wva.s 			page 73


 3653              	.LASF175:
 3654 080c 6C6F6E67 		.string	"long long unsigned int"
 3654      206C6F6E 
 3654      6720756E 
 3654      7369676E 
 3654      65642069 
 3655              	.LASF3:
 3656 0823 7265675F 		.string	"reg_save_area"
 3656      73617665 
 3656      5F617265 
 3656      6100
 3657              	.LASF170:
 3658 0831 77637374 		.string	"wcstold"
 3658      6F6C6400 
 3659              	.LASF201:
 3660 0839 696E745F 		.string	"int_p_sep_by_space"
 3660      705F7365 
 3660      705F6279 
 3660      5F737061 
 3660      636500
 3661              	.LASF57:
 3662 084c 5F535F69 		.string	"_S_ios_seekdir_end"
 3662      6F735F73 
 3662      65656B64 
 3662      69725F65 
 3662      6E6400
 3663              	.LASF172:
 3664 085f 77637374 		.string	"wcstoll"
 3664      6F6C6C00 
 3665              	.LASF156:
 3666 0867 77637373 		.string	"wcsstr"
 3666      747200
 3667              	.LASF47:
 3668 086e 5F496F73 		.string	"_Ios_Iostate"
 3668      5F496F73 
 3668      74617465 
 3668      00
 3669              	.LASF224:
 3670 087b 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIcE5__maxE"
 3670      5F5F676E 
 3670      755F6378 
 3670      7832345F 
 3670      5F6E756D 
 3671              	.LASF134:
 3672 08ad 77637372 		.string	"wcsrtombs"
 3672      746F6D62 
 3672      7300
 3673              	.LASF72:
 3674 08b7 61646A75 		.string	"adjustfield"
 3674      73746669 
 3674      656C6400 
 3675              	.LASF125:
 3676 08c3 746D5F77 		.string	"tm_wday"
 3676      64617900 
 3677              	.LASF32:
 3678 08cb 5F535F75 		.string	"_S_unitbuf"
 3678      6E697462 
GAS LISTING /tmp/ccLn5Wva.s 			page 74


 3678      756600
 3679              	.LASF74:
 3680 08d6 666C6F61 		.string	"floatfield"
 3680      74666965 
 3680      6C6400
 3681              	.LASF103:
 3682 08e1 73777363 		.string	"swscanf"
 3682      616E6600 
 3683              	.LASF165:
 3684 08e9 5F5F6469 		.string	"__digits"
 3684      67697473 
 3684      00
 3685              	.LASF136:
 3686 08f2 77637374 		.string	"wcstod"
 3686      6F6400
 3687              	.LASF138:
 3688 08f9 77637374 		.string	"wcstof"
 3688      6F6600
 3689              	.LASF140:
 3690 0900 77637374 		.string	"wcstok"
 3690      6F6B00
 3691              	.LASF141:
 3692 0907 77637374 		.string	"wcstol"
 3692      6F6C00
 3693              	.LASF83:
 3694 090e 7472756E 		.string	"trunc"
 3694      6300
 3695              	.LASF6:
 3696 0914 5F5F4649 		.string	"__FILE"
 3696      4C4500
 3697              	.LASF67:
 3698 091b 73686F77 		.string	"showpoint"
 3698      706F696E 
 3698      7400
 3699              	.LASF206:
 3700 0925 7365746C 		.string	"setlocale"
 3700      6F63616C 
 3700      6500
 3701              	.LASF94:
 3702 092f 66777363 		.string	"fwscanf"
 3702      616E6600 
 3703              	.LASF9:
 3704 0937 77696E74 		.string	"wint_t"
 3704      5F7400
 3705              	.LASF234:
 3706 093e 696F735F 		.string	"ios_base"
 3706      62617365 
 3706      00
 3707              	.LASF77:
 3708 0947 62616462 		.string	"badbit"
 3708      697400
 3709              	.LASF215:
 3710 094e 77637472 		.string	"wctrans"
 3710      616E7300 
 3711              	.LASF183:
 3712 0956 74686F75 		.string	"thousands_sep"
 3712      73616E64 
GAS LISTING /tmp/ccLn5Wva.s 			page 75


 3712      735F7365 
 3712      7000
 3713              	.LASF78:
 3714 0964 656F6662 		.string	"eofbit"
 3714      697400
 3715              	.LASF130:
 3716 096b 7763736C 		.string	"wcslen"
 3716      656E00
 3717              	.LASF76:
 3718 0972 696F7374 		.string	"iostate"
 3718      61746500 
 3719              	.LASF159:
 3720 097a 5F5F6465 		.string	"__debug"
 3720      62756700 
 3721              	.LASF128:
 3722 0982 746D5F67 		.string	"tm_gmtoff"
 3722      6D746F66 
 3722      6600
 3723              	.LASF186:
 3724 098c 63757272 		.string	"currency_symbol"
 3724      656E6379 
 3724      5F73796D 
 3724      626F6C00 
 3725              	.LASF178:
 3726 099c 73686F72 		.string	"short int"
 3726      7420696E 
 3726      7400
 3727              	.LASF117:
 3728 09a6 77637366 		.string	"wcsftime"
 3728      74696D65 
 3728      00
 3729              	.LASF236:
 3730 09af 66756E63 		.string	"func"
 3730      00
 3731              	.LASF189:
 3732 09b4 6D6F6E5F 		.string	"mon_grouping"
 3732      67726F75 
 3732      70696E67 
 3732      00
 3733              	.LASF55:
 3734 09c1 5F535F63 		.string	"_S_cur"
 3734      757200
 3735              	.LASF112:
 3736 09c8 77637363 		.string	"wcscat"
 3736      617400
 3737              	.LASF233:
 3738 09cf 31315F5F 		.string	"11__mbstate_t"
 3738      6D627374 
 3738      6174655F 
 3738      7400
 3739              	.LASF204:
 3740 09dd 696E745F 		.string	"int_p_sign_posn"
 3740      705F7369 
 3740      676E5F70 
 3740      6F736E00 
 3741              	.LASF129:
 3742 09ed 746D5F7A 		.string	"tm_zone"
GAS LISTING /tmp/ccLn5Wva.s 			page 76


 3742      6F6E6500 
 3743              	.LASF110:
 3744 09f5 76777363 		.string	"vwscanf"
 3744      616E6600 
 3745              	.LASF52:
 3746 09fd 5F535F69 		.string	"_S_ios_iostate_end"
 3746      6F735F69 
 3746      6F737461 
 3746      74655F65 
 3746      6E6400
 3747              	.LASF111:
 3748 0a10 77637274 		.string	"wcrtomb"
 3748      6F6D6200 
 3749              	.LASF181:
 3750 0a18 6C636F6E 		.string	"lconv"
 3750      7600
 3751              	.LASF70:
 3752 0a1e 756E6974 		.string	"unitbuf"
 3752      62756600 
 3753              	.LASF131:
 3754 0a26 7763736E 		.string	"wcsncat"
 3754      63617400 
 3755              	.LASF169:
 3756 0a2e 5F5F6E75 		.string	"__numeric_traits_integer<short int>"
 3756      6D657269 
 3756      635F7472 
 3756      61697473 
 3756      5F696E74 
 3757              	.LASF220:
 3758 0a52 5F5F6473 		.string	"__dso_handle"
 3758      6F5F6861 
 3758      6E646C65 
 3758      00
 3759              	.LASF173:
 3760 0a5f 6C6F6E67 		.string	"long long int"
 3760      206C6F6E 
 3760      6720696E 
 3760      7400
 3761              	.LASF90:
 3762 0a6d 66707574 		.string	"fputwc"
 3762      776300
 3763              	.LASF91:
 3764 0a74 66707574 		.string	"fputws"
 3764      777300
 3765              	.LASF85:
 3766 0a7b 7E496E69 		.string	"~Init"
 3766      7400
 3767              	.LASF99:
 3768 0a81 6D627372 		.string	"mbsrtowcs"
 3768      746F7763 
 3768      7300
 3769              	.LASF51:
 3770 0a8b 5F535F66 		.string	"_S_failbit"
 3770      61696C62 
 3770      697400
 3771              	.LASF194:
 3772 0a96 705F6373 		.string	"p_cs_precedes"
GAS LISTING /tmp/ccLn5Wva.s 			page 77


 3772      5F707265 
 3772      63656465 
 3772      7300
 3773              	.LASF166:
 3774 0aa4 5F5F6E75 		.string	"__numeric_traits_integer<long unsigned int>"
 3774      6D657269 
 3774      635F7472 
 3774      61697473 
 3774      5F696E74 
 3775              	.LASF124:
 3776 0ad0 746D5F79 		.string	"tm_year"
 3776      65617200 
 3777              	.LASF17:
 3778 0ad8 73686F72 		.string	"short unsigned int"
 3778      7420756E 
 3778      7369676E 
 3778      65642069 
 3778      6E7400
 3779              	.LASF160:
 3780 0aeb 5F5F6F70 		.string	"__ops"
 3780      7300
 3781              	.LASF106:
 3782 0af1 76667773 		.string	"vfwscanf"
 3782      63616E66 
 3782      00
 3783              	.LASF53:
 3784 0afa 5F496F73 		.string	"_Ios_Seekdir"
 3784      5F536565 
 3784      6B646972 
 3784      00
 3785              	.LASF75:
 3786 0b07 666D7466 		.string	"fmtflags"
 3786      6C616773 
 3786      00
 3787              	.LASF209:
 3788 0b10 5F5F696E 		.string	"__int32_t"
 3788      7433325F 
 3788      7400
 3789              	.LASF95:
 3790 0b1a 67657477 		.string	"getwc"
 3790      6300
 3791              	.LASF98:
 3792 0b20 6D627369 		.string	"mbsinit"
 3792      6E697400 
 3793              	.LASF213:
 3794 0b28 69737763 		.string	"iswctype"
 3794      74797065 
 3794      00
 3795              	.LASF184:
 3796 0b31 67726F75 		.string	"grouping"
 3796      70696E67 
 3796      00
 3797              	.LASF150:
 3798 0b3a 77707269 		.string	"wprintf"
 3798      6E746600 
 3799              	.LASF227:
 3800 0b42 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIlE5__minE"
GAS LISTING /tmp/ccLn5Wva.s 			page 78


 3800      5F5F676E 
 3800      755F6378 
 3800      7832345F 
 3800      5F6E756D 
 3801              	.LASF27:
 3802 0b74 5F535F73 		.string	"_S_scientific"
 3802      6369656E 
 3802      74696669 
 3802      6300
 3803              	.LASF89:
 3804 0b82 77636861 		.string	"wchar_t"
 3804      725F7400 
 3805              	.LASF118:
 3806 0b8a 74797065 		.string	"typedef __va_list_tag __va_list_tag"
 3806      64656620 
 3806      5F5F7661 
 3806      5F6C6973 
 3806      745F7461 
 3807              	.LASF143:
 3808 0bae 77637374 		.string	"wcstoul"
 3808      6F756C00 
 3809              		.hidden	__dso_handle
 3810              		.ident	"GCC: (GNU) 4.9.2"
 3811              		.section	.note.GNU-stack,"",@progbits
