#include <iostream>
#include <vector>

using namespace std;

// A macro to define dummy functions:
#define DF(N) void N() { \
   cout << "function " #N " called..." << endl; }

class X;
typedef void(X::*fptr)();

class X {
    private:
    /* data */
        vector<fptr> fv;
        DF(a); DF(b); DF(c); DF(d); DF(e); DF(f); DF(g);
    public:
    X (){};
    virtual ~X (){};
    void add(){
        fv.push_back(&X::a);
        fv.push_back(&X::b);
        fv.push_back(&X::c);
        fv.push_back(&X::d);
        fv.push_back(&X::e);
        fv.push_back(&X::f);
        fv.push_back(&X::g);
    }
    void remove(){
        while(fv.size() >= 0)
            fv.pop_back();
    }
    void run() {
        for(vector<fptr>::iterator f1 = fv.begin(); f1 != fv.end(); f1++)
            (this->*(*f1))();
    }
};

int main() {
    X x;
    x.add();
    x.run();
}
