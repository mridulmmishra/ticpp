#include <iostream>

using namespace std;

class X {
    private:
        /* data */

    public:
        X (){};
        virtual ~X (){};
    // private: // Generates compile time error
        X(const X& x) {
            cout << "X::X(X&)" << endl;
        }
};

void func(X x){
    int i = 0;
}

int main(){
    X x;
    func(x);  // works
}
