#include <iostream>

using namespace std;

class X {
    private:
    /* data */
        double* d;
    public:
    X (double dd):d(new double) {
        *d = dd;
    }
    X(const X& x) : d(new double) {
        *d = *(x.d);
    }
    virtual ~X (){
        cout << "X::~X : " << *d << endl;
        *d = -1;
        delete d;
        d = NULL;
    }
};

void func(X x){
    int i = 0;
}

int main() {
    X x(3.2);
    func(x);
}
