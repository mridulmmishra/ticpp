#include <iostream>

using namespace std;

class X {
    private:
        /* data */
    public:
        double d;
        X (double dd = 0.0) : d(dd) {};
        virtual ~X () {};
        void print() {
            cout << "X::d " << d << endl;
        }
};

int main() {
    double X::*dptr = &X::d;
    void (X::*fptr)() = &X::print;

    X x1(3.2);
    X* xptr = &(x1);
    (x1.*fptr)();
    x1.*dptr = 4.3;
    (xptr->*fptr)();
}
