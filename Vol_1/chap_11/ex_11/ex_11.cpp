#include <iostream>

using namespace std;

double func(int x, char c, float f, double d){
    return (x + (int) c + d - f);
}

int main(){
    int i = 3;
    char c = 'M';
    float f = 2.4;
    double d = 3.1;
    d = func(i, c, f, d);
}
