#include <iostream>

using namespace std;

void f(int** i) {
    **i  = 4;
}

int main(int argc, char* argv[]) {
    int i = 47;
    int* ip = &i;
    f(&ip);
    return 0;
}
