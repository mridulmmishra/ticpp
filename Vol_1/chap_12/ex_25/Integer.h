#ifndef GUARD_INTEGER_H
#define GUARD_INTEGER_H 
#include <iostream>

// Non member functions
class Integer {
    private:
        long i;
        Integer* This() { return this; }
    public:
        Integer (long ii = 0) : i(ii) {}
        Integer(const Integer& it){
            std::cout << "Integer::Integer(const Integer&)" << std::endl;
            i = it.i;
        }
        virtual ~Integer (){}
        // Unary operators
        // No Side effects, take const& argument
        friend const Integer& 
            operator+(const Integer& a);
        friend const Integer 
            operator-(const Integer& a);
        friend const Integer 
            operator~(const Integer& a);
        friend Integer* 
            operator&(Integer& a);
        friend const Integer 
            operator!(const Integer& a);
        // With side effect
        friend Integer& operator++(Integer& a);
        friend Integer operator++(Integer& a, int);
        friend Integer& operator--(Integer& a);
        friend Integer operator--(Integer& a, int);
        // Binary Operators
        // Operators that create new or modified value
        friend const Integer 
            operator+(const Integer& left,
                    const Integer& right);
        friend const Integer 
            operator-(const Integer& left,
                    const Integer& right);
        friend const Integer 
            operator*(const Integer& left,
                    const Integer& right);
        friend const Integer 
            operator/(const Integer& left,
                    const Integer& right);
        friend const Integer 
            operator%(const Integer& left,
                    const Integer& right);
        friend const Integer 
            operator^(const Integer& left,
                    const Integer& right);
        friend const Integer 
            operator&(const Integer& left,
                    const Integer& right);
        friend const Integer 
            operator|(const Integer& left,
                    const Integer& right);
        friend const Integer 
            operator<<(const Integer& left,
                    const Integer& right);
        friend const Integer 
            operator>>(const Integer& left,
                    const Integer& right);
        // Operators that modify & return lvalue
        friend Integer& 
            operator+=(Integer& left,
                    const Integer& right);
        friend Integer& 
            operator-=(Integer& left,
                    const Integer& right);
        friend Integer& 
            operator*=(Integer& left,
                    const Integer& right);
        friend Integer& 
            operator/=(Integer& left,
                    const Integer& right);
        friend Integer& 
            operator%=(Integer& left,
                    const Integer& right);
        friend Integer& 
            operator^=(Integer& left,
                    const Integer& right);
        friend Integer& 
            operator&=(Integer& left,
                    const Integer& right);
        friend Integer& 
            operator|=(Integer& left,
                    const Integer& right);
        friend Integer& 
            operator<<=(Integer& left,
                    const Integer& right);
        friend Integer& 
            operator>>=(Integer& left,
                    const Integer& right);
        // Conditional operators which return true/false
        friend int
            operator==(const Integer& left,
                    const Integer& right);
        friend int
            operator!=(const Integer& left,
                    const Integer& right);
        friend int
            operator<(const Integer& left,
                    const Integer& right);
        friend int
            operator>(const Integer& left,
                    const Integer& right);
        friend int
            operator<=(const Integer& left,
                    const Integer& right);
        friend int
            operator>=(const Integer& left,
                    const Integer& right);
        friend int
            operator&&(const Integer& left,
                    const Integer& right);
        friend int
            operator||(const Integer& left,
                    const Integer& right);
        void print(std::ostream& os) const { os << i; }
};
#endif /* INTEGER_H */
