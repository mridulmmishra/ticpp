#include <iostream>

using namespace std;
class Orange {
    private:

    public:
    Orange () {}
    virtual ~Orange () {}
};

class Apple {
    private:

    public:
    explicit Apple (const Orange& o) {
        cout << "Creating Apple from Orange" << endl;
    }
    virtual ~Apple () {}
};

void f(Apple){
    cout << "Calling f(Apple)" << endl;
}

int main(int argc, char* argv[]) {
    Orange o;
    //f(o);
    f(Apple(o));
    return 0;
}
