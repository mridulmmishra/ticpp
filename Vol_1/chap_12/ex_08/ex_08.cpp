#include <iostream>

using namespace std;

class DataObj {
    private:
    int i;

    public:
    DataObj (int ii = 0) : i(ii) {}
    virtual ~DataObj () {}
    DataObj operator+(const DataObj& d2){
        return DataObj(i + d2.i);
    }
    DataObj operator-(){
        return DataObj(-i);
    }
    ostream& print(ostream& os){
        return os << i;
    }
};

int main(int argc, char* argv[]) {
    DataObj a(33), b(41);
    a.print(cout << "a: " ) << endl;
    b.print(cout << "b: ") << endl;
    (a+b).print(cout << "a + b: ") << endl;
    (-a).print(cout<< "-a: ") << endl;
    return 0;
}
