#include <iostream>

using namespace std;

class DataObj {
    private:
    int i;

    public:
    DataObj (int ii = 0) : i(ii) {}
    virtual ~DataObj () {}
    DataObj operator+(const DataObj& d2){
        return DataObj(i + d2.i);
    }
    DataObj operator-(const DataObj& d2){
        return DataObj(i - d2.i);
    }
    DataObj& operator++(){
        ++i;
        return *this;
    }
    const DataObj operator++(int){
        DataObj tmpObj = *this;
        ++i;
        return tmpObj;
    }
    friend ostream& operator<<(ostream& os, const DataObj& d) {
        return os << d.i;
    }
};

int main(int argc, char* argv[]) {
    DataObj a(33), b(41), c(25);
    cout << "a: " << a << endl;
    cout << "b: " << b << endl;
    cout << "c: " << c << endl;
    cout << "(a + b - c): " << (a + b - c) << endl;
    cout << "(a++): " << (a++) << endl;
    cout << "a: " << a << endl;
    cout << "(++a): " << (++a) << endl;
    cout << "a: " << a << endl;
    return 0;
}
