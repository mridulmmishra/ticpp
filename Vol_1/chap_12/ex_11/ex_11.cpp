#include <iostream>

using namespace std;

class Number {
    private:
        double d;

    public:
        explicit Number(double dd = 0) : d(dd) {}
        virtual ~Number () {}
        const Number operator+(const Number& n2) const{
            return Number(d + n2.d);
        }
        const Number operator-(const Number& n2) const{
            return Number(d - n2.d);
        }
        const Number operator*(const Number& n2) const{
            return Number(d * n2.d);
        }
        const Number operator/(const Number& n2) const{
            return Number(d / n2.d);
        }
        const Number& operator=(const Number& n2) {
            if(&(n2) != this){
                d = n2.d;
            }
            return *this;
        }
        operator double() const { return d; }
};

int main(int argc, char* argv[]) {
    Number a(2.3), b(3.2), c(1.4), d(9.2);
    cout << 2.5 + ((a + b - c)*d)/Number(1.5) << endl;
    return 0;
}
