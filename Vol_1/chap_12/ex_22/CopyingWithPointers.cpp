#include "../require.h"
#include <iostream>
#include <string>

using namespace std;

class Dog {
    private:
        string nm;
        
    public:
        Dog (const string& name) : nm(name) {
            cout << "Creating Dog: " << *this << endl;
        }
        Dog (const Dog* dp, const string& msg) : nm(dp->nm + msg) {
            cout << "Copied Dog: " << *this << " from " << *dp << endl;
        }
        virtual ~Dog () {
            cout << "Deleting Dog: " << *this << endl;
        }
        void rename(const string& nname) {
            nm = nname;
            cout << "Dog renamed to: " << *this << endl;
        }
        friend ostream& operator<<(ostream& os, const Dog& dg) {
            return os << " [" << dg.nm << "] ";
        }
};

class DogHouse {
    private:
        Dog* p;
        string houseName;
        
    public:
        DogHouse (Dog* dp, const string& house) : p(dp), houseName(house) {}
        DogHouse (const DogHouse& dh) 
            : p(new Dog(dh.p, " copy-constructed"))
              , houseName(dh.houseName + " copy-constructed") {}
        virtual ~DogHouse () { delete p; }
        /*
        DogHouse& operator=(const DogHouse& dh) {
            if(p != dh.p){
                p = new Dog(dh.p, " assigned");
                houseName = dh.houseName + " assigned";
            }
            return *this;
        }
        */
        void renameHouse(const string& nname) {
            houseName = nname;
        }
        Dog* getDog() const { return p; }
        friend ostream& operator<<(ostream& os, const DogHouse& dh){
            return os << "[" << dh.houseName << "] contains " << *(dh.p);
        }
};

int main(int argc, char* argv[]) {
    DogHouse fidos(new Dog("Fido"), "FidoHouse");
    cout << fidos << endl;
    DogHouse fidos2 = fidos;
    cout << fidos2 << endl;
    fidos2.getDog()->rename("Spot");
    fidos2.renameHouse("SpotHouse");
    cout << fidos2 << endl;
    fidos = fidos2;
    cout << fidos << endl;
    fidos.getDog()->rename("Max");
    fidos.renameHouse("MaxHouse");
    return 0;
}
