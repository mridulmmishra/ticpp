#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

using namespace std;

ofstream out("ex_09.out");

class DataObj {
    private:
    char c;

    public:
    DataObj (const char& cc = 0) : c(cc) {}
    virtual ~DataObj () {}
    friend istream& operator>>(istream& is, DataObj& d){
        return is >> d.c;
    }
    friend ostream& operator<<(ostream& os, DataObj& d){
        return os << d.c;
    }
};

int main(int argc, char* argv[]) {
    stringstream ss("abcdefgh");
    DataObj dat;
    while(ss >> dat){
        out << dat << endl;
    }
    ifstream in("ex_09.out");
    while(in >> dat)
        cout << dat << endl;
    cin >> dat;
    cout << dat << endl;
    return 0;
}
