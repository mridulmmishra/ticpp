#include "../require.h"
#include <iostream>
#include <string>

using namespace std;

class Dog {
    private:
        string nm;
        int refc;
        static int dCount;
        int dNum;

        Dog (const string& name) : nm(name), refc(1), dNum(++dCount) {
            cout << "Creating Dog: " << *this << endl;
        }
        Dog& operator=(const Dog&);
    public:
        static Dog* make(const string& name) {
            return new Dog(name);
        }
        Dog (const Dog& dp) : nm(dp.nm + " copy"), refc(1), dNum(++dCount) {
            cout << "Dog copy constructor: " << *this << endl;
        }
        virtual ~Dog () {
            cout << "Deleting Dog: " << *this << endl;
            dCount--;
        }
        void attach(){
            refc++;
            cout << "Attached Dog: " << *this << endl;
        }
        void detach() {
            require(refc != 0);
            cout << "Detaching dog: " << *this << endl;
            if(--refc == 0) delete this;
        }
        Dog* unalias(){
            cout <<"Unaliasing dog: " << *this << endl;
            if(refc == 1) return this;
            refc--;
            return new Dog(*this);
        }
        void rename(const string& nname) {
            nm = nname;
            cout << "Dog renamed to: " << *this << endl;
        }
        friend ostream& operator<<(ostream& os, const Dog& dg) {
            return os << " [" << dg.dNum << ": " << dg.nm << "], rc = " << dg.refc;
        }
};
int Dog::dCount = 0;

class DogHouse {
    private:
        Dog* p;
        string houseName;
        static int dHCount;
        int dHNum;

    public:
        DogHouse (Dog* dp, const string& house) 
            : p(dp), houseName(house), dHNum(++dHCount) {
            cout << "Created DogHouse: " << *this << endl;
        }
        DogHouse (const DogHouse& dh) 
            : p(dh.p), houseName(dh.houseName + " copy-constructed"), dHNum(++dHCount) {
                p->attach();
                cout << "DogHouse copy constructed " << *this << endl;
            }
        virtual ~DogHouse () { 
            cout << "DogHouse destructor(): " << *this << endl;
            p->detach();
            --dHCount;
        }
        DogHouse& operator=(const DogHouse& dh) {
            if(&dh != this){
                p->detach();
                p = dh.p;
                p->attach();
                houseName = dh.houseName + " assigned";
            }
            cout << "DogHouse operator= : " << *this << endl;
            return *this;
        }
        void renameHouse(const string& nname) {
            houseName = nname;
        }
        void renameDog(const string& nname) {
            unalias();
            p->rename(nname);
        }
        void unalias() { p = p->unalias(); }
        Dog* getDog() { 
            unalias();
            return p;
        }
        friend ostream& operator<<(ostream& os, const DogHouse& dh){
            return os << "[" << dh.dHNum << ": " << dh.houseName << "] contains " << *(dh.p);
        }
};
int DogHouse::dHCount = 0;

int main(int argc, char* argv[]) {
    DogHouse fidos(Dog::make("Fido"), "FidoHouse"),
             spots(Dog::make("Spot"), "SpotHouse");
    cout << "Entering copy construction" << endl;
    DogHouse bobs(fidos);
    cout << "After copy constructing bobs" << endl;
    cout << "fidos: " << fidos << endl;
    cout << "spots: " << spots << endl;
    cout << "bobs: " << bobs << endl;
    cout << "Entering spots = fidos" << endl;
    spots = fidos;
    cout << "After spots = fidos" << endl;
    cout << "spots: " << spots << endl;
    cout << "Entering self-assignment" << endl;
    bobs = bobs;
    cout << "After self-assignment" << endl;
    cout << "bobs: " << bobs << endl;
    cout << "Entering rename(\"Bob\")" << endl;
    bobs.getDog()->rename("Bob");
    cout << "After rename(\"Bob\")" << endl;
    return 0;
}
