#include <iostream>

using namespace std;

class ex_03 {
    private:
    int i;

    public:
    ex_03 (int ii = 0) : i(ii) {}
    virtual ~ex_03 () {}
    ex_03 operator+(const ex_03& d2){
        return ex_03(i + d2.i);
    }
    ex_03 operator-(const ex_03& d2){
        return ex_03(i - d2.i);
    }
    void print(ostream& os){
        os << i << endl;
    }
};

int main(int argc, char* argv[]) {
    ex_03 e1(33), e2(41), e3(25);
    e1.print(cout);
    e2.print(cout);
    e3.print(cout);
    (e1 + e2 - e3).print(cout);
    return 0;
}
