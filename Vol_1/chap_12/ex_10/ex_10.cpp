#include <iostream>

using namespace std;

class DataObj {
    private:

    public:
    DataObj (){}
    virtual ~DataObj (){}
    DataObj& operator++(int i){
        cout << "operator++ int i: " << i << endl;
        return *this;
    }
    DataObj& operator--(int i){
        cout << "operator-- int i: " << i << endl;
        return *this;
    }
};

int main(int argc, char* argv[]) {
    DataObj dat;
    dat++;
    dat--;
    return 0;
}
