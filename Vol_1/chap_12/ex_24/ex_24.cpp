#include <string>
#include <iostream>

using namespace std;

class StrD1 {
    private:
    string s;

    public:
    StrD1 (const string& ss = "") : s(ss) {}
    virtual ~StrD1 () {}
    void print(){
        cout << s << endl;
    }
};

class StrD2 {
    private:
    StrD1 S1;

    public:
    StrD2 (const string& ss = "") : S1(ss) {}
    virtual ~StrD2 () {}
    void print(){
        S1.print();
    }
};

int main(int argc, char* argv[]) {
    StrD2 s1("Hello");
    StrD2 s2(s1);
    StrD2 s3("there!");
    s1.print();
    s2.print();
    s3.print();
    s3 = s1;
    s1.print();
    s2.print();
    s3.print();
    return 0;
}
