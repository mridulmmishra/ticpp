#include <iostream>

using namespace std;

class Value {
    private:
        int a, b;
        float c;
        
    public:
        Value (int aa = 0, int bb = 0, float cc = 0) : a(aa), b(bb), c(cc){}
        virtual ~Value () {}
        Value& operator=(const Value& v1){
            a = v1.a;
            b = v1.b;
            c = v1.c;
            return *this;
        }
        friend ostream& operator<<(ostream& os, const Value& rv) {
            return os << "a = " << rv.a << ", b = " << rv.b << ", c = " << rv.c;
        }
};

int main(int argc, char* argv[]){
    Value a, b(1, 2, 3.3);
    cout << "a: " << a << endl;
    cout << "b: " << b << endl;
    a = b;
    cout << "a after assignment: " << a << endl;
    return 0;
}
