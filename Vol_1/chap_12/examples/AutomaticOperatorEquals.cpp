#include <iostream>

using namespace std;

class Cargo {
    private:
                
    public:
        Cargo () {}
        virtual ~Cargo (){}
        Cargo& operator=(const Cargo&){
            cout << "Cargo::operator=" << endl;
            return *this;
        }
};

class Truck {
    private:
        Cargo b;
        
    public:
        Truck (){}
        virtual ~Truck () {}
};
int main(int argc, char* argv[]) {
    Truck a, b;
    a = b;
    return 0;
}
