class Fi { };

class Fee {
    private:
        
    public:
        Fee (int) {}
        virtual ~Fee () {}
        Fee(const Fi&) {}
};

class Fo {
    private:
        int i;
        
    public:
        Fo (int x = 0) : i(x) {}
        virtual ~Fo () {}
        operator Fee() const { return Fee(i); }
};

int main(int argc, char* argv[]) {
    Fo fo;
    Fee fee = fo; 
    // Convert fo to temp of Fee type
    // Create a copy constructor of Fee type
    // Invoke copy constructor for fee
    return 0;
}
