#include "../require.h"
#include <iostream>
#include <sstream>
#include <cstring>

using namespace std;

class IntArray {
    private:
        /* data */
        enum { sz = 5 };
        int i[sz];
    public:
        IntArray () {memset(i, 0, sz*sizeof(*i));}
        virtual ~IntArray (){}
        int& operator[](int idx){
            require(idx >= 0 && idx < sz, "IntArray::operator[] out of bound index");
            return i[idx];
        }
        friend ostream& operator<<(ostream& os, const IntArray& ia);
        friend istream& operator>>(istream& is, IntArray& ia);
};

ostream& operator<<(ostream& os, const IntArray& ia){
    for(int j = 0; j < ia.sz; j++){
        os << ia.i[j];
        if(j != ia.sz - 1)
            os << ", ";
    }
    os << endl;
    return os;
}

istream& operator>>(istream& is, IntArray& ia){
    for(int j = 0; j < ia.sz; j++)
        is >> ia.i[j];
    return is;
}

int main(int argc, char* argv[]) {
    stringstream input("47 34 56 92 103");
    IntArray I;
    input >> I;
    I[4] = -1;
    cout << I;
    return 0;
}
