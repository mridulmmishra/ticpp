#ifndef GUARD_BYTE_H
#define GUARD_BYTE_H 
#include <iostream>
#include "../require.h"

// Non member functions
class Byte {
    private:
        /* data */
        unsigned char b;
    public:
        Byte (unsigned char bb = 0) : b(bb) {}
        virtual ~Byte (){}
        // No side effect: const member function
        const Byte operator+(const Byte& right) const{
                return Byte(b + right.b);
            }
        const Byte operator-(const Byte& right) const{
                return Byte(b - right.b);
            }
        const Byte operator*(const Byte& right) const{
                return Byte(b * right.b);
            }
        const Byte operator/(const Byte& right) const{
                require(right.b != 0, "divide by zero");
                return Byte(b / right.b);
            }
        const Byte operator%(const Byte& right) const{
                require(right.b != 0, "modulo by zero");
                return Byte(b % right.b);
            }
        const Byte operator^(const Byte& right) const{
                return Byte(b ^ right.b);
            }
        const Byte operator&(const Byte& right) const{
                return Byte(b & right.b);
            }
        const Byte operator|(const Byte& right) const{
                return Byte(b | right.b);
            }
        const Byte operator<<(const Byte& right) const{
                return Byte(b << right.b);
            }
        const Byte operator>>(const Byte& right) const{
                return Byte(b >> right.b);
            }
        // Operators that modify & return lvalue
        // Operator = can only be a member function
        Byte& operator=(const Byte& right){
                if(this == &right) return *this;
                b = right.b;
                return *this;
            }
        Byte& operator+=(const Byte& right){
                if(this == &right) {/* self assignment*/ }
                b += right.b;
                return *this;
            }
        Byte& operator-=(const Byte& right){
                if(this == &right) {/* self assignment*/ }
                b -= right.b;
                return *this;
            }
        Byte& operator*=(const Byte& right){
                if(this == &right) {/* self assignment*/ }
                b *= right.b;
                return *this;
            }
        Byte& operator/=(const Byte& right){
                if(this == &right) {/* self assignment*/ }
                require(right.b != 0, "divide by zero");
                b /= right.b;
                return *this;
            }
        Byte& operator%=(const Byte& right){
                if(this == &right) {/* self assignment*/ }
                require(right.b != 0, "modulo by zero");
                b %= right.b;
                return *this;
            }
        Byte& operator^=(const Byte& right){
                if(this == &right) {/* self assignment*/ }
                b ^= right.b;
                return *this;
            }
        Byte& operator&=(const Byte& right){
                if(this == &right) {/* self assignment*/ }
                b &= right.b;
                return *this;
            }
        Byte& operator|=(const Byte& right){
                if(this == &right) {/* self assignment*/ }
                b |= right.b;
                return *this;
            }
        Byte& operator<<=(const Byte& right){
                if(this == &right) {/* self assignment*/ }
                b <<= right.b;
                return *this;
            }
        Byte& operator>>=(const Byte& right){
                if(this == &right) {/* self assignment*/ }
                b >>= right.b;
                return *this;
            }
        // Conditional operators which return true/false
        int operator==(const Byte& right){
                return b == right.b;
            }
        int operator!=(const Byte& right){
                return b != right.b;
            }
        int operator<(const Byte& right){
                return b < right.b;
            }
        int operator>(const Byte& right){
                return b > right.b;
            }
        int operator<=(const Byte& right){
                return b <= right.b;
            }
        int operator>=(const Byte& right){
                return b >= right.b;
            }
        int operator&&(const Byte& right){
                return b && right.b;
            }
        int operator||(const Byte& right){
                return b || right.b;
            }
        void print(std::ostream& os) const { 
            os << "0x" << std::hex << int(b) << std::dec;
        }
};
#endif /* BYTE_H */
