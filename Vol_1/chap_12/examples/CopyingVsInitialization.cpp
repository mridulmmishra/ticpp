class Fi {
    private:
        /* data */
        
    public:
        Fi () {}
        virtual ~Fi () {}
};

class Fee {
    private:
        /* data */
        
    public:
        Fee (int) {}
        virtual ~Fee () {}
        Fee(const Fi&) {}
};

int main(int argc, char* argv[]) {
    Fee fee = 1; //Fee(int)
    Fi fi;
    Fee fum = fi; // Fee(Fi)
    return 0;
}
