#include <iostream>

using namespace std;

class Integer {
    private:
        /* data */
        long i;
        Integer* This() { return this; }
    public:
        Integer (long ii = 0) : i(ii) {}
        virtual ~Integer (){}
        const Integer operator+(const Integer& rv) const {
            cout << "operator +" << endl;
            return Integer(i + rv.i);
        }

        Integer& operator+=(const Integer& rv){
            cout << "operator +=" << endl;
            i += rv.i;
            return *this;
        }
};

int main(int argc, char* argv[]) {
    cout <<"built-in types:" << endl;
    long i = 1, j = 2, k = 3;
    k += i + j;
    cout << "user defined types" << endl;
    Integer ii(1), jj(2), kk(3);
    kk += ii + jj;
    return 0;
}
