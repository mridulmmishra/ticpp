#include "../require.h"
#include <string>
#include <cstdlib>
#include <cstring>

using namespace std;

class Stringc {
    private:
        string s;
        
    public:
        Stringc (const string& str = "") : s(str) {}
        virtual ~Stringc () {}
        int strcmp(const Stringc& S) const{
            return ::strcmp(s.c_str(), S.s.c_str());
        }
};

int main(int argc, char* argv[]) {
    Stringc s1("Hello"), s2("there");
    s1.strcmp(s2);
    return 0;
}
