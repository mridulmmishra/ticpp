#include "../require.h"
#include <string>
#include <cstdlib>
#include <cstring>

using namespace std;

class Stringc {
    private:
        string s;
        
    public:
        Stringc (const string& str = "") : s(str) {}
        virtual ~Stringc () {}
        operator const char*() const{
            return s.c_str();
        }
};

int main(int argc, char* argv[]) {
    Stringc s1("Hello"), s2("there");
    strcmp(s1,s2);
    strspn(s1,s2);
    return 0;
}
