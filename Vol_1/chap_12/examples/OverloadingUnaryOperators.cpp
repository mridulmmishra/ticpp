#include <iostream>

using namespace std;

// Non member functions
class Integer {
    private:
        /* data */
        long i;
        Integer* This() { return this; }
    public:
        Integer (long ii = 0) : i(ii) {}
        virtual ~Integer (){}
        //Unary operators
        // No Side effects, take const& argument
        friend const Integer& 
            operator+(const Integer& a);
        friend const Integer 
            operator-(const Integer& a);
        friend const Integer 
            operator~(const Integer& a);
        friend Integer* 
            operator&(Integer& a);
        friend const Integer 
            operator!(const Integer& a);
        // With side effect
        friend Integer& operator++(Integer& a);
        friend Integer operator++(Integer& a, int);
        friend Integer& operator--(Integer& a);
        friend Integer operator--(Integer& a, int);
};

const Integer& operator+(const Integer& a){
    cout << "+Integer\n";
    return a; // unary + has no effect
}
const Integer operator-(const Integer& a){
    cout << "-Integer\n";
    return Integer(-a.i);
}
const Integer operator~(const Integer& a) {
    cout << "~Integer\n";
    return Integer(~a.i);
}
const Integer operator!(const Integer& a) {
    cout << "!Integer\n";
    return Integer(!a.i);
}
Integer* operator&(Integer& a) {
    cout << "&Integer\n";
    return a.This();
}
Integer& operator++(Integer& a){
    cout << "++Integer\n";
    a.i++;
    return a;
}
Integer operator++(Integer& a, int){
    cout << "Integer++\n";
    Integer before(a.i);
    a.i++;
    return before;
}
Integer& operator--(Integer& a){
    cout << "--Integer\n";
    a.i--;
    return a;
}
Integer operator--(Integer& a, int){
    cout << "Integer--\n";
    Integer before(a.i);
    a.i--;
    return before;
}

void f(Integer a) {
    +a;
    -a;
    ~a;
    Integer *ip = &a;
    !a;
    ++a;
    a++;
    --a;
    a--;
}

//Member functions (implicit "this")
class Byte {
    private:
        /* data */
        unsigned char b;
    public:
        Byte (unsigned char bb = 0) : b(bb) {}
        virtual ~Byte () {};
        const Byte& operator+(){
            cout << "+Byte\n";
            return *this; // unary + has no effect
        }
        const Byte operator-(){
            cout << "-Byte\n";
            return Byte(-b);
        }
        const Byte operator~() {
            cout << "~Byte\n";
            return Byte(~b);
        }
        const Byte operator!() {
            cout << "!Byte\n";
            return Byte(!b);
        }
        Byte* operator&() {
            cout << "&Byte\n";
            return this;
        }
        Byte& operator++(){
            cout << "++Byte\n";
            b++;
            return *this;
        }
        Byte operator++(int){
            cout << "Byte++\n";
            Byte before(b);
            b++;
            return before;
        }
        Byte& operator--(){
            cout << "--Byte\n";
            b--;
            return *this;
        }
        Byte operator--(int){
            cout << "Byte--\n";
            Byte before(b);
            b--;
            return before;
        }
};

void g(Byte b) {
    +b;
    -b;
    ~b;
    Byte *bp = &b;
    !b;
    ++b;
    b++;
    --b;
    b--;
}

int main(int argc, char* argv[]) {
    Integer a;
    f(a);
    Byte b;
    g(b);
    return 0;
}
