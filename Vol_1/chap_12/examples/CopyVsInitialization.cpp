#include <iostream>

using namespace std;

class Fi {
    private:
    /* data */
    public:
    Fi () {
        cout << "Fi constuctor" << endl;
    }
    virtual ~Fi (){}
};

class Fee {
    private:
    /* data */
    public:
    Fee (int) { 
        cout << "Fee constuctor" << endl;
    }
    Fee(const Fi&) {
        cout << "Fee copy constuctor" << endl;
    }
    virtual ~Fee () {}
};

int main(int argc, char* argv[]) {
    Fee fee = 1;
    Fi fi;
    Fee fum = fi;
    return 0;
}
