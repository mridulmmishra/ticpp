#include <iostream>

using namespace std;

class Dog {
    private:
    /* data */
    public:
    Dog (){}
    virtual ~Dog (){}
    int run(int i) const {
        cout << "run\n";
        return i;
    }
    int sleep(int i) const {
        cout << "sleep\n";
        return i;
    }
    int eat(int i) const {
        cout << "eat\n";
        return i;
    }
    typedef int (Dog::*PMF)(int)const;
    class FunctionObject {
        private:
        /* data */
            Dog* dg;
            PMF pf;
        public:
        FunctionObject (Dog* dog, PMF pmf) : dg(dog), pf(pmf) {
            cout << "FunctionObject::FunctionObject()\n";
        }
        virtual ~FunctionObject (){}
        int operator()(int i){
            cout << "FunctionObject::operator()\n";
            return (dg->*pf)(i);
        }
    };
    FunctionObject operator->*(PMF pm){
        cout << "Dog::operator->*\n";
        return FunctionObject(this, pm);
    }
};

int main(int argc, char* argv[]) {
    Dog dm;
    Dog::PMF pmf = &Dog::run;
    cout << (dm->*pmf)(1) << endl;
    pmf = &Dog::sleep;
    cout << (dm->*pmf)(2) << endl;
    pmf = &Dog::eat;
    cout << (dm->*pmf)(3) << endl;
    return 0;
}
