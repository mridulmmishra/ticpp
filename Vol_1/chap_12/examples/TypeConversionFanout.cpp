class Orange;
class Pear;

class Apple {
    private:
        
    public:
        Apple () {}
        virtual ~Apple (){}
        operator Orange() const;
        operator Pear() const;
};

void eat(Orange);
void eat(Pear);

int main(int argc, char* argv[]) {
    Apple a1;
//    eat(a1); // Error, Apple->Orange or Apple->Pear ??
    return 0;
}
