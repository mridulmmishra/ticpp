class One {
    private:
        
        
    public:
        One (){}
        virtual ~One () {}
};

class Two {
    private:
        
        
    public:
        explicit Two (const One&) {}
        virtual ~Two () {}
};

void f(Two) {}

int main(int argc, char* argv[]) {
    One one;
    //f(one);
    f(Two(one));
    return 0;
}
