#include <iostream>

using namespace std;

class Class2;

class Class1 {
    private:

    public:
    Class1 () {}
    Class1 (const Class2& c){
        cout << "Converting from Class2 to Class1" << endl;
    }
    virtual ~Class1 (){}
    friend Class1 operator+(const Class1& c1, const Class1& c2);
};

Class1 operator+(const Class1& c1, const Class1& c2){
    cout << "Adding two class1 objects" << endl;
}

class Class2 {
    private:

    public:
    Class2 () {}
    virtual ~Class2 (){}
};

int main(int argc, char* argv[]) {
    Class1 c1, c2;
    Class2 d1;
    c1 + c2;
    c1 + d1;
    d1 + c1;
    return 0;
}
