class Orange{
    public:
        Orange() {}
        virtual ~Orange() {}
};
class Pear{
    public:
        Pear() {}
        virtual ~Pear() {}
};

class Apple {
    private:
        
    public:
        Apple () {}
        virtual ~Apple (){}
        operator Orange() const;
        operator Pear() const;
};

void eat(Orange);
void eat(Pear);

int main(int argc, char* argv[]) {
    Apple a1;
    eat(Orange(a1));
    eat(Pear(a1));
    return 0;
}
