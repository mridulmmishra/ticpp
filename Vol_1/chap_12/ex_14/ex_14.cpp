#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class Bird {
    private:
        string s;

    public:
        static int bc;
        Bird (const string& ss = "Bird") : s(ss){
            ostringstream os;
            os << ++bc;
            s = s + " #" + os.str();
            cout << "Created: " << *this << endl;
        }
        virtual ~Bird () {
            if(bc != 0){
                cout << "Destroying: " << *this << " bird number " << bc-- << endl;
            }
        }
        Bird(const Bird& bd) {
            ostringstream os;
            os << ++bc;
            s = bd.s + " #" + os.str() + " copied";
            cout << "Copy created: " << *this << " from " << bd << endl;
        }
        Bird& operator=(const Bird& bd) {
            cout << "Assigning: " << bd << " to " << *this << endl;
            s = bd.s + " assigned";
            return *this;
        }
        friend ostream& operator<<(ostream& os, const Bird& bd){
            return os << bd.s;
        }
};

int Bird::bc = 0;

int main(int argc, char* argv[]) {
    Bird b1;
    Bird b2 = b1;
    Bird b3("Happy");
    b1 = b3;
    return 0;
}
