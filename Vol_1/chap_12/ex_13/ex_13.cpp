#include <iostream>

using namespace std;

class DataObj {
    private:
    int* p;

    public:
    DataObj (int* const ip = 0) : p(ip) {}
    virtual ~DataObj () {}
    void print(){
        cout << "*p: " << *p << " at address = " << p << endl;
    }
    // /* Uncomment this line to disable following operator=
    DataObj& operator=(const DataObj& d2){
        p = new int(*d2.p);
        return *this;
    }
    //*/
};

int main(int argc, char* argv[]) {
    int i = 20;
    DataObj d1(&i), d2;
    d2 = d1;
    d1.print();
    d2.print();
    return 0;
}
