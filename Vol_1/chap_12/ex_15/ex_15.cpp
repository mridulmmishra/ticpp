#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class Bird {
    private:
        string s;

    public:
        static int bc;
        Bird (const string& ss = "Bird") : s(ss){
            ostringstream os;
            os << ++bc;
            s = s + " #" + os.str();
            cout << "Created: " << *this << endl;
        }
        virtual ~Bird () {
            if(bc != 0){
                cout << "Destroying: " << *this << " bird number " << bc-- << endl;
            }
        }
        Bird(const Bird& bd) {
            ostringstream os;
            os << ++bc;
            s = bd.s + " #" + os.str() + " copied";
            cout << "Copy created: " << *this << " from " << bd << endl;
        }
        Bird& operator=(const Bird& bd) {
            cout << "Assigning: " << bd << " to " << *this << endl;
            s = bd.s + " assigned";
            return *this;
        }
        friend ostream& operator<<(ostream& os, const Bird& bd){
            return os << bd.s;
        }
};

class BirdHouse {
    private:
        Bird bo;
        Bird& br;
        Bird* bp;

        BirdHouse(const BirdHouse&);
        BirdHouse& operator=(const BirdHouse&);

    public:
        BirdHouse (Bird b1, Bird& b2, Bird* b3) : bo(b1), br(b2), bp(b3) {}
        virtual ~BirdHouse () {}
        friend ostream& operator<<(ostream& os, const BirdHouse& bh) {
            return os << bh.bo << ", " << bh.br << ", " << *bh.bp;
        }
};

int Bird::bc = 0;

int main(int argc, char* argv[]) {
    Bird b1;
    Bird b2 = b1;
    Bird b3("Happy");
    BirdHouse bh(b1, b2, &b3);
    cout << bh << endl;
//    BirdHouse bh2 = bh;
    Bird b4, b5, b6;
    BirdHouse bh3(b4, b5, &b6);
//    bh2 = bh;
    return 0;
}
