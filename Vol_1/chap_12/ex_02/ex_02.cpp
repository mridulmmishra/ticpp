#include <iostream>

using namespace std;

class ex_02 {
    private:
    int i;

    public:
    ex_02 (int ii = 0) : i(ii) {}
    virtual ~ex_02 () {}
    ex_02 operator+(const ex_02& d2){
        return ex_02(i + d2.i);
    }
    void print(ostream& os){
        os << i << endl;
    }
};

int main(int argc, char* argv[]) {
    ex_02 e1(33), e2(41);
    e1.print(cout);
    e2.print(cout);
    (e1+e2).print(cout);
    return 0;
}
