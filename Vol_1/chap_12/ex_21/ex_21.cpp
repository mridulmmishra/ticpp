#include <iostream>
#include <string>

using namespace std;

class DataObj {
    private:

    public:
    DataObj () {}
    virtual ~DataObj () {}
    DataObj& operator=(const DataObj& d2, string str = "Op=call"){
        cout << str << endl;
        return *this;
    }
};

int main(int argc, char* argv[]) {
    DataObj d1, d2;
    d2 = d1;
    return 0;
}
