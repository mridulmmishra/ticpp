#include <iostream>

using namespace std;

class DataObj {
    private:
    int i;

    public:
    DataObj (int ii = 0) : i(ii) {}
    virtual ~DataObj () {}
    friend DataObj operator+(const DataObj& d1, const DataObj& d2);
    friend DataObj operator-(const DataObj& d1, const DataObj& d2);
    void print(ostream& os){
        os << i << endl;
    }
};

    DataObj operator+(const DataObj& d1, const DataObj& d2){
        return DataObj(d1.i + d2.i);
    }
    DataObj operator-(const DataObj& d1, const DataObj& d2){
        return DataObj(d1.i - d2.i);
    }
int main(int argc, char* argv[]) {
    DataObj a(33), b(41), c(25);
    a.print(cout);
    b.print(cout);
    c.print(cout);
    (a + b - c).print(cout);
    return 0;
}
