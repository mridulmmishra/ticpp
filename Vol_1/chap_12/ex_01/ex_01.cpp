#include <iostream>

using namespace std;

class ex_01 {
    private:

    public:
    ex_01 () {}
    virtual ~ex_01 () {}
    const ex_01& operator++(){
        cout << "ex_01::operator++" << endl;
    }
};

int main(int argc, char* argv[]) {
    ex_01 e1;
    e1++;
    ++e1;
    return 0;
}
