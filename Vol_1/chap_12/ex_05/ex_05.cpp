#include <iostream>

using namespace std;

class DataObj {
    private:
    int i;

    public:
    DataObj (int ii = 0) : i(ii) {}
    virtual ~DataObj () {}
    DataObj operator+(const DataObj& d2){
        return DataObj(i + d2.i);
    }
    DataObj operator-(const DataObj& d2){
        return DataObj(i - d2.i);
    }
    DataObj& operator++(){
        ++i;
        return *this;
    }
    const DataObj operator++(int){
        DataObj tmpObj = *this;
        ++i;
        return tmpObj;
    }
    void print(ostream& os) const{
        os << i << endl;
    }
};

int main(int argc, char* argv[]) {
    DataObj a(33), b(41), c(25);
    a.print(cout);
    b.print(cout);
    c.print(cout);
    (a + b - c).print(cout);
    (a++).print(cout);
    a.print(cout);
    (++a).print(cout);
    a.print(cout);
    return 0;
}
