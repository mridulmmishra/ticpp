#include <iostream>
#include <vector>
#include "../require.h"

using namespace std;

class Obj {
    private:
    /* data */
        static int i, j;
    public:
    Obj (){}
    virtual ~Obj (){}
    void f() const { cout << i++ << endl; }
    void g() const { cout << j++ << endl; }
};

int Obj::i = 47;
int Obj::j = 11;

class ObjContainer {
    private:
        /* data */
        vector<Obj*> a;
    public:
        ObjContainer (){}
        virtual ~ObjContainer (){}
        void add(Obj* obj) { a.push_back(obj); }
        class SmartPointer;
        friend SmartPointer;
        class SmartPointer {
            private:
                /* data */
                ObjContainer& oc;
                int index;
            public:
                SmartPointer (ObjContainer& objc) : oc(objc) {
                    index = 0;
                }
                virtual ~SmartPointer (){}

                bool operator++(){ // Prefix
                    if(index >= oc.a.size()) return false;
                    if(oc.a[++index] == 0) return false;
                    return true;
                }

                bool operator++(int){ // Postfix
                    return operator++();
                }

                bool operator--(){ // Prefix
                    if(index <= 0) return false;
                    if(oc.a[--index] == 0) return false;
                    return true;
                }

                bool operator--(int){ // Postfix
                    return operator--();
                }

                Obj* operator->() const {
                    require(oc.a[index] != 0, "Zero value"
                            "returned by SmartPointer::operator->()");
                    return oc.a[index];
                }
        };
        SmartPointer begin(){
            return SmartPointer(*this);
        }
};


int main(int argc, char* argv[]) {
    const int sz = 10;
    Obj o[sz];
    ObjContainer oc;
    for(int i = 0; i < sz; i++)
        oc.add(&o[i]);
    ObjContainer::SmartPointer sp = oc.begin();
    do{
        sp->f();
        sp->g();
    } while(sp++);
    while(sp--){
        sp->f();
        sp->g();
    }
    return 0;
}
