#include "Stack3.h"
#include "../require.h"

using namespace std;

Stack::Link::Link(void* dat, Link* nxt){
    data = dat;
    next = nxt;
}

Stack::Link::~Link(){}

Stack::Stack() { head = NULL; }

void Stack::push(void* dat){
    Link* newlnk = new Link(dat, head);
    head = newlnk;
}

void* Stack::peek(){
    require(head != NULL, "Stack Empty.");
    return head->data;
}

void* Stack::pop(){
    if(head == NULL){
	return 0;
    }
    void* result=head->data;
    Link* oldHead = head;
    head = head->next;
    delete oldHead;
    return result;
}

Stack::~Stack(){
    require(head == NULL, "Stack not Empty.");
}
