#include "Simple.h"
#include <iostream>

using namespace std;

Simple::Simple(){
    cout << "Constructor for \"Simple\" class has been called." << endl;
}

Simple::~Simple(){
    cout << "Destructor for \"Simple\" class has been called." << endl;
}
