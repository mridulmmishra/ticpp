#include "Simple.h"
#include <iostream>

using namespace std;

Simple::Simple(int val){
    i = val;
    cout << "Constructor for \"Simple\" class has been called with member value : "<< i << endl;
}

Simple::~Simple(){
    cout << "Destructor for \"Simple\" class has been called with member value : " << i << endl;
}
