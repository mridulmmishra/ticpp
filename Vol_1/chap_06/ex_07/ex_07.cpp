#include <iostream>

using namespace std;

int main(){
    double arr_1[6]={0.0, 0.1, 0.2};
    for(int i=0; i<(sizeof arr_1/sizeof *arr_1); i++)
	cout << arr_1[i] << " ";
    cout << endl;

    double arr_2[]={0, 1, 2, 3, 4, 5};

    for(int i=0; i<(sizeof arr_2/sizeof *arr_2); i++)
	cout << arr_2[i] << " ";
    cout << endl;

    return 0;
}
