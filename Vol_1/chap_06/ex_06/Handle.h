#ifndef GUARD_HANDLE_H
#define GUARD_HANDLE_H 
class Handle{
    private:
	struct Cheshire;
	Cheshire* cs;
    public:
	Handle();
	~Handle();
	int read();
	void change(int);
};
#endif /* HANDLE_H */
