#include "Handle.h"
#include <iostream>

using namespace std;


int main(){
    Handle u;
    cout << "Current value in hidden cat is : " << u.read() << endl;
    u.change(6);
    cout << "New value in hidden cat is : " << u.read() << endl;
    return 0;
}
