#include "Handle.h"

struct Handle::Cheshire{
    int i;
};

Handle::Handle(){
    cs = new Cheshire;
    cs->i=0;
}

Handle::~Handle(){
    delete cs;
}

int Handle::read(){
    return cs->i;
}

void Handle::change(int val){
    cs->i = val;
}
