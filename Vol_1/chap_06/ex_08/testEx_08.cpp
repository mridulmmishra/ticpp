#include "Stack3.h"
#include <iostream>
#include <string>

using namespace std;

int main(){
    string st[]={"Mridul", "Manohar", "Mishra"};
    Stack s;
    for(int i; i< (sizeof st/sizeof *st); i++){
	s.push(st+i);
    }
    for(int i; i< (sizeof st/sizeof *st); i++){
	cout << *(string*)s.pop() << " ";
    }
    cout << endl;
    return 0;
}
