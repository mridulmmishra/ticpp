#include <iostream>

using namespace std;

class X{
    public:
	X();
};

X::X(){}

void f(int i) {
    if(i < 0) {
	//! goto jump1; // Error: goto bypasses init
    }
    X x1;
jump1:
    switch(i) {
	case 1 :
	    X x2;
	    break;
	    //! case 2 : //Error: case bypasses init
	    X x3; //Constructor called here
	    break;
    }
}

int main() {
    f(9);
    f(11);
}
