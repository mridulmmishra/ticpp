#include <iostream>

using namespace std;

class Tree{
    private:
    	int height;
    public:
	Tree(int initialHeight); // Constructor, no return type
	~Tree(); // Destructor, no return type
	void grow(int years);
	void printsize();
};

Tree::Tree(int initialHeight) {
    height =  initialHeight;
}

Tree::~Tree(){
    cout << "inside Tree destructor" << endl;
    printsize();
}

void Tree::grow(int years){
    height += years;
}

void Tree::printsize(){
    cout << "Tree height is :" << height << endl;
}

int main(){
    cout << "Before opening brace" << endl;
    {
	Tree t(12);
	cout << "after tree creation" << endl;
	t.printsize();
	t.grow(4);
	cout << "before closing brace" << endl;
    }
    cout << "After opening brace" << endl;
}
