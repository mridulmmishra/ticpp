#ifndef GAURD_STASH_H
#define GAURD_STASH_H

struct Stash{
    private:
	int size;
	int quantity;
	int next;
	unsigned char* storage;
	void inflate(int increase);
    public:
	//Stash(int sz);
	Stash(int sz, int initQuantity=100);
	~Stash();
	int add(const void* element);
	void* fetch(int index);
	int count();
};
#endif
