#include "Stash.h"
#include "../require.h"
#include <iostream>
#include <cassert>

#define INFLATE_SIZE 100

void Stash::inflate(int increase){
    require( 0 < increase, "Stash::inflate zero or negative increase.");
    int newQuantity = size + increase;
    unsigned char* tmp_p = new unsigned char[newQuantity*size];
    for(int i =0; i < (next-1)*size; i++)
	*(tmp_p + i) = *(storage + i);
    delete [] storage;
    storage = tmp_p;
    quantity = newQuantity;
}
/*
Stash::Stash(int sz){
    size = sz;
    quantity = 0;
    next = 0;
    storage = 0;
}
*/
Stash::Stash(int sz, int initQuantity){
    size = sz;
    quantity = 0;
    next = 0;
    storage = 0;
    inflate(initQuantity);
}

Stash::~Stash(){
    if(quantity > 0){
	std::cout << "Clearing up storage." << std::endl;
	quantity = 0;
	next = 0;
	delete [] storage;
    }
}

int Stash::add(const void* element){
    if(next >= quantity)
	inflate(INFLATE_SIZE);
    for(int i=0; i < size; i++)
	*(storage + next*size + i) = *((unsigned char*)element + i);
    next++;
    return next - 1;
}

void* Stash::fetch(int index){
    require( 0 <= index, "Stash::fetch() (-)index.");
    if(index < next)
	return (void *)(storage + index*size) ;
    else
	return 0;
}

int Stash::count(){
    return next;
}
