#ifndef GUARD_EX_02_H
#define GUARD_EX_02_H 
#include <string>

class Message{
    private:
	std::string msg;
    public:
	Message(std::string ss): msg(ss) {}
	void print() const;
	void print(std::string s) const;
};
#endif /* EX_02_H */
