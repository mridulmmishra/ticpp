#include "ex_02.h"
#include <iostream>

void Message::print() const{
    std::cout << msg << std::endl;
}

void Message::print(std::string s) const{
    std::cout << s << ": " << msg << std::endl;
}
