#include "Mem.h"
#include <cstring>
#include <iostream>

using namespace std;

class myString { 
    private:
	Mem* buf;
    public:
	myString();
	myString(char* str);
	~myString();
	void concat(char* str);
	void print(ostream& os);
};

myString::myString(){ buf = 0; }

myString::myString(char* str) { 
    buf = new Mem(strlen(str) + 1);
    strcpy((char*)buf->pointer(), str);
}

myString::~myString(){ delete buf;}

void myString::concat(char* str){
    if(!buf)
	buf = new Mem;
    strcat((char*) buf->pointer(buf->msize() + strlen(str) + 1), str);
}

void myString::print(ostream& os){
    if(!buf) return;
    os << buf->pointer() << endl;
}

int main(){
    myString s("My test String.");
    s.print(cout);
    s.concat("Some additional stuff.");
    s.print(cout);
    myString s2;
    s2.concat("Using default constructor.");
    s2.print(cout);
    return 0;
}
