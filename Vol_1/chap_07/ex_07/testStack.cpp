#include "Stack3.h"
#include "../require.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main(int argc, char* argv[]){
  requireArgs(argc, 1);
  ifstream in(argv[1]);
  assure(in, argv[1]);
  //Stack textlines;
  void * arr[100];
  string line;
  int num_lines=0;
  while(getline(in, line)){
    //textlines.push(new string(line));
    if(num_lines >= 100)
	break;
    arr[num_lines++] = (void *) new string(line);
  }
  
  Stack textlines(arr, num_lines);

  string* s;
  while((s = (string*)textlines.pop()) != 0 ){
    cout << *s << endl;
    delete s;
  }

  return 0;
}
