#include "Stack3.h"
#include "../require.h"

Stack::Link::Link(void* dat, Link* nxt){
    data = dat;
    next = nxt;
}

Stack::Link::~Link(){ }

Stack::Stack(){
    head = NULL;
}

Stack::Stack(void* arr[], int size){
    head = NULL;
    for(int i=0; i < size; i++)
	push(arr[i]);
}

Stack::~Stack(){
	require(head == NULL,  "Stack not empty.");
}

void Stack::push(void* dat){
    struct Link* new_leaf = new struct Link(dat, head);
    head = new_leaf;
}

void* Stack::pop(){
    if(head == NULL)
	return 0;
    void* last_d=head->data;
    struct Link* oldHead = head;
    head = oldHead->next;
    delete oldHead;
    return last_d;
}

void* Stack::peek(){
    require(head != NULL, "Stack empty.");
    return head->next;
}
