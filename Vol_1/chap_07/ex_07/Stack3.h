#ifndef STACK3_H
#define STACK3_H

class Stack {
    private:
	struct Link {
	    void *data;
	    Link* next;
	    Link(void* dat, Link* nxt);
	    ~Link();
	}* head;
	// Interface
    public:
	Stack();
	Stack(void* arr[], int size);
	~Stack();
	void push(void* dat);
	void* peek();
	void* pop();
};

#endif
