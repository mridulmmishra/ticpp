#include <iostream>

//#define DEBUG

using namespace std;

class SuperVar{
    private:
	union{
	    char c;
	    int i;
	    float f;
	};
    public:
#ifdef DEBUG
	enum{
	    character=0,
	    integer=1,
	    floating=2
	}vartype;
#endif
	SuperVar(char ch);
	SuperVar(int ii);
	SuperVar(float ff);
	void print(int vartype);
};

SuperVar::SuperVar(char ch){
#ifdef DEBUG
    vartype = character;
#endif
    c = ch;
}

SuperVar::SuperVar(int ii){
#ifdef DEBUG
    vartype = integer;
#endif
    i = ii;
}

SuperVar::SuperVar(float ff){
#ifdef DEBUG
    vartype = floating;
#endif
    f = ff;
}

void SuperVar::print(int vartype){
    switch(vartype){
	case 0:
	    cout << "character: " << c << endl;
	    break;
	case 1:
	    cout << "integer: " << i << endl;
	    break;
	case 2:
	    cout << "floating: " << f << endl;
	    break;
    }
}

int main(){
    SuperVar A('c'), B(12), C(1.34F);
    A.print(0);
    B.print(1);
    C.print(2);
    return 0;
}
