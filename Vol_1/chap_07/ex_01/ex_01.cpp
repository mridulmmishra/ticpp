#include "ex_01.h"
#include <iostream>
#include <fstream>

using namespace std;

Text::Text() { text=""; }

Text::Text(string filename){
    ifstream myfile(filename.c_str(), ios::in);
    cout << myfile.is_open() << endl;
    if(myfile.is_open()){
	string line;
	while(std::getline(myfile, line)){
	    text += "\n";
	    text += line;
	}
    }
}

string Text::contents() const{
    if(text == ""){
	return " ";
    } else {
	return text;
    }
}
