#ifndef GUARD_EX_01_H
#define GUARD_EX_01_H
#include <string>

class Text{
    private:
	std::string text;
    public:
	Text();
	Text(std::string filename);
	std::string contents() const;
};
#endif /* EX_01_H */
