#include "ex_04.h"

int main(){
    ex_04 s1;
    s1.mem_f(1);
    s1.mem_f(1, 2);
    s1.mem_f(1, 2, 3);
    s1.mem_f(1, 2, 3, 4);
    return 0;
}
