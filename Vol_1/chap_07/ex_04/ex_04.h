#ifndef GUARD_EX_04_H
#define GUARD_EX_04_H
class ex_04{
    private:
	int i;
    public:
	/*
	void mem_f(int j1);
	void mem_f(int j1, int j2);
	void mem_f(int j1, int j2, int j3);
	void mem_f(int j1, int j2, int j3, int j4);
	*/
	void mem_f(int j1, int j2=0, int j3=0, int j4=0);
};
#endif
