#ifndef GUARD_EX_03_H
#define GUARD_EX_03_H
class Test{
    private:
	int ii;
    public:
	Test(int i = 0) : ii(i) {};
	int value() const {return ii;}
};
#endif
