#ifndef GUARD_VALUESTACK_H
#define GUARD_VALUESTACK_H 
#include "../require.h"

template<class T, int ssize=100>
class Stack {
    T stack[ssize];
    int top;
    public:
        Stack() : top(0) {}
        void push(const T& dat) {
            require( top < ssize, "Too many push()es");
            stack[top++] = dat;
        }
        T peek() const {
            return stack[top - 1];
        }
        T pop() {
            require( top > 0, "Too many pop()s");
            return stack[--top];
        }
};
#endif /* VALUESTACK_H */
