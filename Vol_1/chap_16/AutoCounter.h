#ifndef GUARD_AUTOCOUNTER_H
#define GUARD_AUTOCOUNTER_H 
#include "../require.h"
#include <iostream>
#include <set>
#include <string>

class AutoCounter {
    private:
        static int count;
        int id;
        class CleanupCheck {
            private:
                std::set<AutoCounter*> trace;
            public:
                void add(AutoCounter* ap) {
                    trace.insert(ap);
                }
                void remove(AutoCounter* ap) {
                    require(trace.erase(ap) == 1, "Attempt to delete the counter twice.");
                }
                virtual ~CleanupCheck() {
                    std::cout << "~CleanupCheck()" << std::endl;
                    require(trace.size() == 0, "All AutoCounter objects not cleaned up.");
                }
        };
        static CleanupCheck verifier;
        AutoCounter() : id(count++) {
            verifier.add(this);
            std::cout << "Created [" << id << "]" << std::endl;
        }
        AutoCounter(const AutoCounter &);
        void operator=(const AutoCounter &);
    public:
        static AutoCounter* create() {
            return new AutoCounter();
        }
        virtual ~AutoCounter() {
            std::cout << "Destroying [" << id << "]" << std::endl;
            verifier.remove(this);
        }

        friend std::ostream& operator<<(std::ostream& os, const AutoCounter &ac){
            os << "AutoCounter " << ac.id;
        }

        friend std::ostream& operator<<(std::ostream& os, const AutoCounter* ac){
            os << "AutoCounter " << ac->id;
        }
};
#endif /* AUTOCOUNTER_H */
