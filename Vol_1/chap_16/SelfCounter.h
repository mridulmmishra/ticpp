#ifndef GUARD_SELFCOUNTER_H
#define GUARD_SELFCOUNTER_H
#include "ValueStack.h"
#include <iostream>

class SelfCounter {
    private:
        static int counter;
        int id;
    public:
        SelfCounter() : id(counter++) {
            std::cout << "Created: " << id << std::endl;
        }
        SelfCounter(const SelfCounter& rv) : id(rv.id) {
            std::cout << "Copied " << id << std::endl;
        }
        
        SelfCounter operator=(const SelfCounter& rv){
            std::cout << "Assigned " << rv.id << " to " << id << std::endl;
            id = rv.id;
            return *this;
        }
        virtual ~SelfCounter() {
            std::cout << "Destroyed: " << id << std::endl;
        }
        friend std::ostream& operator<<(std::ostream& os, const SelfCounter& sc) {
            return os << "SelfCounter: " << sc.id;
        }
};

#endif /* SELFCOUNTER_H */
