#ifndef GUARD_TSTACK_H
#define GUARD_TSTACK_H 

template<class T>
class Stack {
        struct Link {
            T* data;
            Link* next;
            Link(T* dat, Link* nxt) : data(dat), next(nxt) {}
        }* head;
    public:
        Stack() : head(0) {}
        ~Stack(){
            while(head)
                delete pop();
        }
        void push(T* dat) {
            head = new Link(dat, head);
        }
        T* peek() {
            return head ? head->data : 0;
        }
        T* pop() {
            if(head == 0) return 0;
            Link* oldhead = head;
            T* result = oldhead->data;
            head = oldhead->next;
            delete oldhead;
            return result;
        }
};
#endif /* TSTACK_H */
