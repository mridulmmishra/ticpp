#include "TStack.h"
#include "../require.h"
#include <iostream>
#include <string>

using namespace std;

class X {
    public:
        ~X() { cout << "~X()" << endl; }
};

int main(int argc, char* argv[]){
    requireArgs(argc, 1);
    ifstream in(argv[1]);
    assure(in, argv[1]);
    Stack<string> textlines;
    string line;
    while(getline(in, line))
        textlines.push(new string(line));
    string* s;
    while((s = (string *)textlines.pop()) != 0) {
        cout << *s << endl;
        delete s;
    }
    Stack<X> xx;
    for(int j = 0; j < 10; j++)
        xx.push(new X);
    return 0;
}
