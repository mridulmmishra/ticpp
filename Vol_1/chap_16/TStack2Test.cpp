#include "TStack2.h"
#include "../require.h"
#include <fstream>
#include <string>
#include <iostream>

using namespace std;

int main() {
    ifstream in("TStack2Test.cpp");
    assure(in, "TStack2Test.cpp");
    Stack<string> textlines;
    string line;
    while(getline(in, line))
        textlines.push(new string(line));
    int i = 0;
    Stack<string>::iterator it = textlines.begin();
    Stack<string>::iterator* it2 = 0;

    while( it != textlines.end() ){
        cout << it->c_str() << endl;
        it++;
        if(++i == 10)
            it2 = new Stack<string>::iterator(it);
    }
    cout << (*it2)->c_str() << endl;
    delete it2;
    return 0;
}
