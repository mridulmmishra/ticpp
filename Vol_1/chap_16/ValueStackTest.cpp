#include "SelfCounter.h"
#include "ValueStack.h"
#include <iostream>

using namespace std;

int main() {
    cout << "Stack creation starts." << endl;
    Stack<SelfCounter> sc;
    cout << "Stack creation ends." << endl;
    for(int i = 0; i < 10; i++)
        sc.push(SelfCounter());
    cout << sc.peek() << endl;

    for(int i = 0; i < 10; i++)
        cout << sc.pop() << endl;
    return 0;
}
