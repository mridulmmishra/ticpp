#ifndef GUARD_TSTACK2_H
#define GUARD_TSTACK2_H
#include "../require.h"

template<class T> class Stack {
    private:
        struct Link {
            T *data;
            Link* next;
            Link(T* dat, Link* nxt) : data(dat),next(nxt) {}
        }* head;
        // Interface
    public:
        Stack() : head(0) {}
        void push(T* dat){
            head = new Link(dat, head);
        }
        T* peek(){
            return head ? head->data : 0;
        }
        T* pop() {
            if(head == 0) return 0;
            T* result = head->data;
            Link* oldhead = head;
            head = oldhead->next;
            delete oldhead;
            return result;
        }
        ~Stack() {
            while(head)
                delete pop();
        }
        class iterator;
        friend class iterator;
        class iterator {
            private:
                Stack::Link* p;
            public:
                iterator(const Stack<T>& tl) : p(tl.head) {}
                iterator() : p(0) {}
                iterator(const iterator &tl) : p(tl.p) {}
                bool operator++(){
                    if(p->next)
                        p = p->next;
                    else
                        p = 0;
                    return bool(p);
                }
                bool operator++(int) { return operator++(); }
                T* current() const {
                    if(p == 0) return 0;
                    return p->data;
                }
                T* operator->() const {
                    require(p != 0 , "PStack::iterator::operator-> returns 0");
                    return current();
                }
                T* operator*() { return current(); }
                operator bool() { return bool(p); }
                bool operator==(const iterator&) { return p == 0; }
                bool operator!=(const iterator&) { return p != 0; }
        };
        iterator begin() const { return iterator(*this); }
        iterator end() const { return iterator(); }
};

#endif  /* TSTACK2_H */
