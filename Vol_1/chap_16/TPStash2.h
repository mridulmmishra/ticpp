#ifndef GAURD_STASH_H
#define GAURD_STASH_H
#include "../require.h"
#include <cstring>

template<class T, int incr = 20>
struct PStash{
    private:
        int quantity;
        int next;
        T** storage;
        void inflate(int increase = incr);
    public:
        PStash() : quantity(0), storage(0), next(0) {}
        virtual ~PStash();
        int add(T* element);
        T* operator[](int index) const;
        T* remove(int index);
        int count() { return next; }
        class iterator;
        friend class iterator;
        class iterator{
            private:
                PStash& ps;
                int index;
            public:
                iterator() : ps(0),index(0){}
                iterator(PStash& p) : ps(p),index(0) {}
                iterator(PStash& p, bool) : ps(p),index(ps.next) {}
                iterator(const iterator& rv) : ps(rv.ps),index(rv.index) {}

                iterator& operator=(const iterator& rv) {
                    ps = rv.ps;
                    index = rv.index;
                    return *this;
                }

                iterator& operator++() {
                    require(++index <= ps.next, "PStash::iterator::operator++() index out of bound");
                    return *this;
                }

                iterator& operator++(int) { return operator++(); }

                iterator& operator--() {
                    require(--index >= 0, "PStash::iterator::operator++() index out of bound");
                    return *this;
                }

                iterator& operator--(int) { return operator--(); }

                iterator& operator+=(int amount) {
                    require(((index + amount) < ps.next) &&
                            ((index + amount) >= 0), "PStash::iterator::operator++() index out of bound");
                    index += amount;
                    return *this;
                }

                iterator& operator-=(int amount) {
                    require(((index - amount) < ps.next) &&
                            ((index - amount) >= 0), "PStash::iterator::operator++() index out of bound");
                    index -= amount;
                    return *this;
                }

                iterator operator+(int amount) {
                    iterator ret(*this);
                    ret += amount;
                    return ret;
                }

                T* current() const {
                    return ps.storage[index];
                }

                T* operator*() const { return current(); }
                T* operator->() const {
                    require(ps.storage[index] != 0 , "PStash::iterator::operator-> returns zero");
                    return current();
                }

                T* remove() {
                    return ps.remove(index);
                }

                bool operator==(const iterator& it) const {
                    return index == it.index;
                }

                bool operator!=(const iterator& it) const {
                    return index != it.index;
                }
        };
        iterator begin() { return iterator(*this); }
        iterator end() { return iterator(*this, true); }
};

template<class T, int incr>
void PStash<T, incr>::inflate(int increase) {
    int sz = sizeof(T*);
    T** new_storage = new T*[quantity + increase];
    std::memset(new_storage, 0 , (quantity+increase)*sz);
    std::memcpy(new_storage, storage, (quantity)*sz);
    quantity += increase;
    delete []storage;
    storage = new_storage;
}

template<class T, int incr>
int PStash<T, incr>::add(T* element) {
    if(next >= quantity)
        inflate(incr);
    storage[next++] = element;
    return next - 1;
}

template<class T, int incr>
T* PStash<T, incr>::operator[](int index) const {
    require(index >=0, "PStash::operator[] index can't be negative");
    if(index >= next)
        return 0;
    require(storage[index] != 0 , "PStash::operator[] returns null pointer");
    return storage[index];
}


template<class T, int incr>
T* PStash<T, incr>::remove(int index) {
    T* v =  operator[](index);
    storage[index] = 0;
    return v;
}

template<class T, int incr>
PStash<T, incr>::~PStash() {
    for(int i = 0; i < next; i++){
        delete storage[i];
        storage[i] = 0;
    }
    delete []storage;
}
#endif
