#ifndef GUARD_SHAPE_H
#define GUARD_SHAPE_H
#include <string>
#include <iostream>

class Shape{
    public:
        static int idx;
        static int increment() { return idx++;}
        virtual void draw() = 0;
        virtual void erase() = 0;
        virtual ~Shape() {}
};

class Circle : public Shape {
    private:
        int id;
    public:
        Circle() : id(Shape::increment()) { std::cout << "Circle::Circle: " << id << std::endl; }
        ~Circle() { std::cout << "Circle::~Circle: " << id << std::endl; }
        void draw() { std::cout << "Circle::draw: " << id << std::endl; }
        void erase() { std::cout << "Circle::erase: " << id << std::endl; }
};

class Square : public Shape {
    private:
        int id;
    public:
        Square() : id(Shape::increment()) { std::cout << "Square::Square: " << id << std::endl; }
        ~Square() { std::cout << "Square::~Square: " << id << std::endl; }
        void draw() { std::cout << "Square::draw: " << id << std::endl; }
        void erase() { std::cout << "Square::erase: " << id << std::endl; }
};

class Line : public Shape {
    private:
        int id;
    public:
        Line() : id(Shape::increment()) { std::cout << "Line::Line: " << id << std::endl; }
        ~Line() { std::cout << "Line::~Line: " << id << std::endl; }
        void draw() { std::cout << "Line::draw: " << id << std::endl; }
        void erase() { std::cout << "Line::erase: "<< id << std::endl; }
};
#endif /* SHAPE_H */
