#include "../require.h"
#include "AutoCounter.h"
#include "TPStash.h"
#include <fstream>
#include <iostream>

using namespace std;

int main() {
    PStash<AutoCounter> acStash;
    for(int i = 0; i < 10; i++)
        acStash.add(AutoCounter::create());
    for(int j = 0; j < 5; j++)
        delete acStash.remove(j);
    acStash.remove(5);
    acStash.remove(6);//*/
    cout << "The destructor cleans up the rest" << endl;
    ifstream in("TPStashTest.cpp");
    assure(in, "TPStashTest.cpp");
    PStash<string> stringStash;
    string line;
    while(getline(in, line))
        stringStash.add(new string(line));
    for(int u = 0; stringStash[u]; u++)
        cout << "stringStash[" << u << "] = " << *stringStash[u] << endl;
    return 0;
}
