#include "TPStash2.h"
#include "../require.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class Int {
    private:
        int i;
    public:
        Int(int ii = 0) : i(ii) {
            cout << " > " << i << endl;
        }
        ~Int() { cout << " ~ " << i << endl; }
        operator int() const { return i; }
        friend ostream& operator<<(ostream& os, const Int& x) {
            return os << "Int: " << x.i;
        }
        friend ostream& operator<<(ostream& os, const Int* x) {
            return os << "Int: " << x->i;
        }
};

int main() {
    {
        PStash<Int> ints;
        for(int i = 0; i < 30; i++)
            ints.add(new Int(i));
        cout << endl;

        PStash<Int>::iterator it = ints.begin();
        it += 5;
        PStash<Int>::iterator it2 = it + 10;
        for(;it != it2; it++)
            delete it.remove();
        cout << endl;
        for(it = ints.begin(); it != ints.end(); it++)
            if(*it)
                cout << *it << endl;
    }
    cout << "\n ~~~~~~~~~~~~~~~~~~~~~~ \n " ;
    ifstream in("TPStash2Test.cpp");
    assure(in, "TPStash2Test.cpp");
    PStash<string> lines;
    string line;
    while(getline(in, line))
        lines.add(new string(line));
    PStash<string>::iterator sit = lines.begin();
    for(; sit != lines.end(); sit++)
        cout << **sit << endl;

    sit = lines.begin();
    int n = 26;
    sit += n;
    for(; sit != lines.end(); sit++)
        cout << n++ << ": " << **sit << endl;
    return 0;
}
