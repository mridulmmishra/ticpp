#ifndef GUARD_ITERSTACKTEMPLATE_H
#define GUARD_ITERSTACKTEMPLATE_H
#include "../require.h"
#include <iostream>


template<class T, int ssize = 100>
class StackTemplate {
    private:
        T stack[ssize];
        int top;
    public:
        StackTemplate() : top(0) {}
        void push(const T& i) {
            require(top < ssize, "Too many push()es");
            stack[top++] = i;
        }
        T pop() {
            require(top > 0, "Too many pop()s");
            return stack[--top];
        }
        class iterator;
        friend class iterator;
        class iterator {
            private:
                StackTemplate& s;
                int index;
            public:
                iterator(StackTemplate& is) : s(is), index(0) {}
                iterator(StackTemplate& is, bool) : s(is), index(s.top) {}
                T operator*() const { return s.stack[index]; }
                T operator++(){
                    ++index;
                    require(index < s.top, "Index out of bound");
                    return s.stack[index];
                }
                T operator++(int){
                    require(index < s.top, "Index out of bound");
                    return s.stack[index++];
                }
                iterator& operator+=(int amount) {
                    index += amount;
                    require(index < s.top, "Index out of bound");
                    return *this;
                }
                bool operator==(const iterator& rv) const {
                    return index == rv.index;
                }
                bool operator!=(const iterator& rv) const {
                    return index != rv.index;
                }
                friend std::ostream& operator<<(std::ostream& os, const iterator& it) {
                    return os << *it;
                }
        };
        iterator begin() { return iterator(*this); }
        iterator end() { return iterator(*this, true); }
};
#endif /* ITERSTACKTEMPLATE_H */
