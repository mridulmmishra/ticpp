#ifndef GUARD_TPSTASH_H
#define GUARD_TPSTASH_H 
#include <cstring>

template<class T, int incr=10>
class PStash{
    private:
        T** storage;
        int quantity;
        int next;
        void inflate(int increase = incr);
    public:
        PStash() : storage(0), quantity(0), next(0) {}
        ~PStash();
        int add(T* element);
        T* operator[](int index) const;
        T* remove(int index);
        int count() { return next; }
};

template<class T, int incr>
PStash<T, incr>::~PStash() {
    for(int i = 0; i< next; i++) {
        delete remove(i);
        storage[i] = 0;
    }
    delete []storage;
}

template<class T, int incr>
int PStash<T, incr>::add(T* element) {
    if(next >= quantity){
        inflate(incr);
    }
    storage[next] = element;
    return next++;
}

template<class T, int incr>
void PStash<T, incr>::inflate(int increase) {
    const int psz = sizeof(T*);
    int oldsz = next;
    int newsz = (quantity+increase);
    T** st = new T*[newsz];
    std::memset(st, 0, newsz*psz);
    std::memcpy(st, storage, oldsz*psz);
    quantity = newsz;
    delete []storage;
    storage = st;
}

template<class T, int incr>
T* PStash<T, incr>::operator[](int index) const {
    require(index >= 0 , "PStash::operator[] index negative.");
    if(index >= next) return 0;
    //require(storage[index] != 0, "PStash::operator[] returned null pointer.");
    return storage[index];
}

template<class T, int incr>
T* PStash<T, incr>::remove(int index) {
    T* v = operator[](index);
    if (v != 0 ) storage[index] = 0;
    return v;
}
#endif /* TPSTASH_H */
