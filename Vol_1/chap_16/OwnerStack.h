#ifndef GUARD_OWNERSTACK_H
#define GUARD_OWNERSTACK_H 

template<class T>
class Stack {
        struct Link {
            T* data;
            Link* next;
            Link(T* dat, Link* nxt) : data(dat), next(nxt) {}
        }* head;
        bool own;
    public:
        Stack(bool own=true) : head(0), own(own) {}
        ~Stack();
        void push(T* dat) {
            head = new Link(dat, head);
        }
        T* peek() {
            return head ? head->data : 0;
        }
        T* pop();
        bool owns() const { return own; }
        void owns(bool newOwnership) { own = newOwnership; }
        operator bool() const { return head != 0; }
};

template<class T>
Stack<T>::~Stack(){
    if(!own) return;
    while(head)
        delete pop();
}

template<class T>
T* Stack<T>::pop() {
    if(head == 0) return 0;
    Link* oldhead = head;
    T* result = oldhead->data;
    head = oldhead->next;
    delete oldhead;
    return result;
}
#endif /* OWNERSTACK_H */
