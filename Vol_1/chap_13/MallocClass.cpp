#include "../require.h"
#include <cstdlib>
#include <cstring>
#include <iostream>

using namespace std;

class Obj {
private:
    /* data */
    int i, j, k;
    enum {size = 100};
    char buf[size];

public:
    void initialize(){
        cout << "Initializing Obj" << endl;
        i = j = k = 0;
        memset(buf, 0, size);
    }
    void destroy() {
        cout << "Destroying Obj" << endl;
    }
};

int main(int argc, char* argv[]) {
    Obj* obj = (Obj*) malloc(sizeof(Obj));
    require(obj != 0);
    obj->initialize();
    obj->destroy();
    free(obj);
    return 0;
}
