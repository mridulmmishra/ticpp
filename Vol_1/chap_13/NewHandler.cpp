#include <iostream>
#include <cstdlib>
#include <new>

using namespace std;

int count = 0;

void out_of_memory(){
    cerr << "Out of memory after " << count << " allocations!" << endl;
    exit(1);
}

int main(int argc, char* argv[]) {
    set_new_handler(out_of_memory);
    while(1){
        count++;
        new int[1000];
    }
    return 0;
}
