#include "PStash.h"
#include "../require.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main(int argc, char* argv[]) {
    PStash intStash;
    for(int i = 0; i < 25; i++)
        intStash.add(new int(i));
    for(int i = 0; i < intStash.count(); i++)
        cout << "intStash[" << i << "] = " << *(int*)intStash[i] << endl;
    for(int i = 0; i < intStash.count(); i++)
        delete (int*)intStash.remove(i);
    ifstream in("PStashTest.cpp");
    assure(in, "PStashTest.cpp");
    PStash lines;
    string line;
    while(getline(in, line))
        lines.add(new string(line));
    for(int i = 0; i < lines.count(); i++)
        cout << "stringStash[" << i << "] = " << *(string*)lines[i] << endl;
    for(int i = 0; i < lines.count(); i++)
        delete (string*)lines.remove(i);
    return 0;
}
