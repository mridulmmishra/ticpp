#include <cstdio>
#include <cstdlib>

using namespace std;

void* operator new(size_t sz) {
    printf("operator new: %d Byres\n", sz);
    void* m = malloc(sz);
    if(!m) puts("Out of memory");
    return m;
}

void operator delete(void* m) {
    printf("operator delete");
    free(m);
}

class S
{
public:
    S () { puts("S::S()"); }
    virtual ~S () { puts("S::~S()"); }

private:
    /* data */
    int i[100];
};
int main(int argc, char *argv[]){
    puts("Creating andd destroying and int");
    int* p = new int(47);
    delete p;
    puts("Creating and destroying s");
    S* s = new S;
    delete s;
    puts("Creating and destroying S[3]");
    S* sa = new S[3];
    delete []sa;
    return 0;
}
