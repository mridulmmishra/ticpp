#ifndef GUARD_TREE_H
#define GUARD_TREE_H
#include <iostream>

class Tree {
private:
    /* data */
    int height;
public:
    Tree (int th) : height(th){}
    virtual ~Tree () { std::cout << "*"; }
    friend std::ostream& operator<<(std::ostream& os, const Tree* t) {
        return os << "Tree height is: " << t->height << std::endl;
    }
};
#endif /* TREE_H */
