#include <new>
#include <fstream>

using namespace std;
ofstream trace("ArrayOperatorNew.out");

class Widget {
    private:
    /* data */
        enum { sz = 10 };
        int i[sz];

    public:
    Widget () { trace << "*"; }
    virtual ~Widget () { trace << "~"; }
    void* operator new(size_t sz) {
        trace << "Widget::new: " << sz << " bytes" << endl;
        return ::new char[sz];
    }
    void operator delete(void* p) {
        trace << "\nWidget::delete" << endl;
        ::delete []p;
    }
    void* operator new[](size_t sz) {
        trace << "Widget::new[]: " << sz << " bytes" << endl;
        return ::new char[sz];
    }
    void operator delete[](void* p) {
        trace << "\nWidget::delete[]" << endl;
        ::delete []p;
    }
};

int main(int argc, char* argv[]) {
    trace << "New Widget" << endl;
    Widget* w = new Widget;
    trace << "\ndelete Widget" << endl;
    delete w;
    trace << "New Widget[25]" << endl;
    Widget* wa = new Widget[25];
    trace << "\ndelete Widget[25]" << endl;
    delete []wa;
    return 0;
}
