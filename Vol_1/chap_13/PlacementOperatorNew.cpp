#include <cstddef>
#include <iostream>

using namespace std;

class X {
    private:
    /* data */
        int i;
    public:
    X (int ii = 0) : i(ii) {}
    virtual ~X () {
        cout << "X::~X(): " << this << endl;
    }
    void* operator new(size_t, void* loc) {  // have additional arguments in operator new() definition
        return loc;
    }
};

int main(int argc, char* argv[]) {
    int l[10];
    cout << "l = " << l << endl;
    X* xp = new(l) X(47);  // Provide values only for additional arguments
    xp->X::~X();    // Explicitly call destructor
                    // Destroy the object, don't release the memory 
                    // As it was not allocated during call to new()
                    // Explicitly call destructor only when memory placement has been provided to new
    return 0;
}
