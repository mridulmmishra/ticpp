#include "PStash.h"
#include "../require.h"
#include <iostream>
#include <cstring>

using namespace std;

int PStash::add(void* element) {
    int inflateSize = 10;
    if(next >= quantity)
        inflate(inflateSize);
    storage[next++] = element;
    return (next - 1);
}

void PStash::inflate(int increase) {
    int psz = sizeof(void*);
    int newsz = quantity + increase;
    void** newstorage = new void*[newsz];
    memset(newstorage, 0, newsz*psz);
    memcpy(newstorage, storage, (next)*psz);
    quantity = newsz;
    delete []storage;
    storage = newstorage;
}

void* PStash::operator[](int index) const {
    require(((index >=0 ) && (index < next)), "Index out of bound");
    return storage[index];
}

void* PStash::remove(int index) {
    require(((index >=0 ) && (index < next)), "Index out of bound");
    void* v = storage[index];
    if(v != 0 ) storage[index] = 0;
    return v;
}

PStash::~PStash() {
    for(int i = 0; i < next; i++) {
        require(storage[i] == 0, "PStash not cleaned up");
    }
    delete []storage;
}
