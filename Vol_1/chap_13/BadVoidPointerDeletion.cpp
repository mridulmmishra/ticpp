#include <iostream>

using namespace std;

class Object {
private:
    /* data */
    void* data;
    const int size;
    const char id;
public:
    Object (int sz, char c) : size(sz),id(c) {
        data = new char[size];
        cout << "Constructing object " << id << ", size " << size << endl;
    }
    virtual ~Object (){
        cout << "Destructing object " << id << endl;
        delete []data;
    }
};

int main(int argc, char* argv[]) {
    Object* a = new Object(40, 'a');
    delete a;
    void *b = new Object(40, 'b');
    // delete b; //Doesn't call destructor
    Object* c = (Object*) b;
    delete c; // Calls the destructor
    return 0;
}
