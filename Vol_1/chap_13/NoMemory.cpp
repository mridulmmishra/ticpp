#include <iostream>
#include <new>

using namespace std;

class NoMemory {
    private:
    /* data */
    public:
    NoMemory (){
        cout << "NoMemory::NoMemory()" << endl;
    }
    void* operator new(size_t sz) throw(bad_alloc){
        cout << "NoMemory::operator new" << endl;
        throw bad_alloc();
    }
};

int main(int argc, char* argv[]) {
    NoMemory* nm = 0;
    try{
        nm = new NoMemory;
    }catch(bad_alloc){
        cerr << "Out of Memory exception" << endl;
    }
    cout << "nm = " << nm << endl;
    return 0;
}
