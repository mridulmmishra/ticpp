#include <iostream>

using namespace std;

class NoVirtual {
    private:
        //int a;
    public:
        void x() const {}
        int i() const { return 1; }
};

class OneVirtual {
    private:
        //int a;
    public:
        virtual void x() const {}
        int i() const { return 1; }
};

class TwoVirtual {
    private:
        //int a;
    public:
        virtual void x() const {}
        virtual int i() const { return 1; }
};

int main() {
    cout << "int: " << sizeof(int) << endl;
    cout << "NoVirtual: " << sizeof(NoVirtual) << endl;
    cout << "void* : "  << sizeof(void*) << endl;
    cout << "OneVirtual: " << sizeof(OneVirtual) << endl;
    cout << "TwoVirtual: " << sizeof(TwoVirtual) << endl;
}
