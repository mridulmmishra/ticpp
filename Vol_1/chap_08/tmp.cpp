#include <iostream>

class X{
    private:
	int i;
    public:
	X(int ii = 0);
	void modify();
};

X::X(int ii){ i = ii; }

void X::modify(){ i++; }

X f5() {
    return X();
}

const X f6(){
    return X();
}

void f7(X& x){
    x.modify();
}

int main(){
    f5() = X(1);
    f5().modify();
    f7(f5());
//    f6().modify();
    return 0;
}
/*
int main(){
//    const int i;
    const int j=0;
    int const k=0;
    int i=5;
    int const * ip;
    int * jp;
//    jp = &j;
    ip = &k;
    ip = &i;
//    *ip = 4;
    i = 4;
    char *cp = "howdy";
    *(cp + 3) = 'y';
    return 0;
}
*/
