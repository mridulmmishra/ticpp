#include "ex_6.h"
#include <iostream>

using namespace std;

int main(){
    Hen h;
    Hen::Nest n;
    Hen::Nest::Egg e;
    h.display();
    n.display();
    e.display();
    return 0;
}
