#ifndef GUARD_EX_6_H
#define GUARD_EX_6_H 
class Hen{
    public:
	class Nest{
	    public:
		class Egg{
		    public:
			void display();
		};
		void display();
	};
	void display();
};
#endif /* EX_6_H */
