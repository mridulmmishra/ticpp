#include "ex_6.h"
#include <iostream>

using namespace std;

void Hen::display(){
    cout << "This is Hen." << endl;
}

void Hen::Nest::display(){
    cout << "This is Hen's Nest." << endl;
}

void Hen::Nest::Egg::display(){
    cout << "This is Hen's egg in it's Nest." << endl;
}
