#include "ex_6.h"
#include "Stack2.h"
#include <iostream>

using namespace std;

int main(){
    Stack h_s;
    Hen h1, h2, *h_p;
    h_s.initialize();
    h_s.push(&h1);
    h_s.push(&h2);
    h_p = (Hen*) h_s.pop();
    h_p->display();
    h_p = (Hen*) h_s.pop();
    h_p->display();
    return 0;
}
