#ifndef GAURD_CPPLIB_H
#define GAURD_CPPLIB_H

class Stash{
	int size;
	int quantity;
	int next;
	unsigned char* storage;
	void inflate(int increase);
    public:
	void initialize(int sz);
	void cleanup();
	int add(const void* element);
	void* fetch(int index);
	int count();
};
#endif
