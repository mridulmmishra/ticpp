#ifndef GUARD_EX_5_H
#define GUARD_EX_5_H 

class cl_1;

class cl_2{
    public:
	int j;
	void set_j(int);
	int get_j();
	int get_mem(cl_1*);
};

class cl_3{
    public:
	int k;
	void set_k(int);
	int get_k();
	int get_mem(cl_1*);
};

class cl_1{
    private:
	int i;
    public:
	void set_i(int);
	int get_i();
	friend class cl_2;
	friend int cl_3::get_mem(cl_1*);
};

#endif /* EX_5_H */
