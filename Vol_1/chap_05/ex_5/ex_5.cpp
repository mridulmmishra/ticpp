#include "ex_5.h"

void cl_1::set_i(int in){
    i = in;
}

int cl_1::get_i(){
    return i;
}

void cl_2::set_j(int in){
    j = in;
}

int cl_2::get_j(){
    return j;
}

int cl_2::get_mem(cl_1* x){
    return x->i;
}

void cl_3::set_k(int in){
    k = in;
}

int cl_3::get_k(){
    return k;
}

int cl_3::get_mem(cl_1* x){
    return x->i;
}
