#include "ex_5.h"
#include <iostream>

using namespace std;

int main(){
    cl_1 x;
    cl_2 y;
    cl_3 z;

    x.set_i(5);
    cout << "Cl_1 int : " << x.get_i() << endl;
    cout << "Cl_1 int from Cl_2: " << y.get_mem(&x) << endl;
    cout << "Cl_1 int from Cl_3: " << z.get_mem(&x) << endl;

    return 0;
}
