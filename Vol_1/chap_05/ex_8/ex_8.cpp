#include "ex_8.h"
#include <iostream>

using namespace std;

void ex_8::showMap(){
    cout << "Public member i is located at " << &i << endl;
    cout << "Private member f is located at " << &f << endl;
    cout << "Protected member c is located at " << &c << endl;
}
