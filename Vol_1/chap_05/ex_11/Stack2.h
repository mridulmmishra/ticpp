#ifndef STACK2_H
#define STACK2_H

class Stack{
    private:
	struct Link{
	    void* data;
	    Link* next;
	    void initialize(void* dat, Link* nxt);
	}* head;
    public:
	void initialize();
	void push(void* data);
	void* peek();
	void* pop();
	void cleanup();
};

#endif
