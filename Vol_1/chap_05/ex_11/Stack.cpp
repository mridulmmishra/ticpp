#include <iostream>
#include "Stack2.h"
#include "../require.h"

using namespace std;

void Stack::Link::initialize(void* dat, Link* nxt){
  data = dat;
  next = nxt;
}

void Stack::initialize(){
  head = 0;
}

void Stack::push(void* dt){
  Link* newlnk = new Link;
  newlnk->initialize(dt, head);
  head = newlnk;
}

void* Stack::peek(){
  require(head != 0, "Stack Empty");
  return head->data;
}

void* Stack::pop(){
  if(head == 0){
    return 0;
  }
  void* result = head->data;
  Link* oldlnk = head;
  head = head->next;
  delete oldlnk;
  return result;
}

void Stack::cleanup(){
  require(head == 0, "Stack not empty!");
}
