#include "ex_4.h"

void cl_1::set_i(int in){
    i = in;
}

int cl_1::get_i(){
    return i;
}

int cl_1::get_cl_j(cl_2* y){
    return y->j;
}

void cl_2::set_j(int in){
    j = in;
}

int cl_2::get_j(){
    return j;
}

int cl_2::get_cl_i(cl_1* x){
    return x->i;
}
