#ifndef GUARD_EX_4_H
#define GUARD_EX_4_H 

class cl_2;

class cl_1{
    public:
	int i;
	void set_i(int);
	int get_i();
	int get_cl_j(cl_2*);
};

class cl_2{
    public:
	int j;
	void set_j(int);
	int get_j();
	int get_cl_i(cl_1*);
};
#endif /* EX_4_H */
