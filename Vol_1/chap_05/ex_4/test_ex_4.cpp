#include "ex_4.h"
#include <iostream>

using namespace std;

int main(){
    cl_1 x;
    cl_2 y;
    x.set_i(5);
    y.set_j(6);
    cout << "Cl_1 int : " << x.get_i() << endl;
    cout << "Cl_2 int : " << y.get_j() << endl;

    cout << "Cl_2 int from Cl_1 : " << x.get_cl_j(&y) << endl;
    cout << "Cl_1 int from Cl_2 : " << y.get_cl_i(&x) << endl;
    return 0;
}
