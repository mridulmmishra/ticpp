#include "Stack2.h"
#include "../require.h"

using namespace std;

void Stack::Link::initialize(void* dat, Link* nxt){
    data = dat;
    next = nxt;
}

void Stack::initialize(){
    head = NULL;
}

void Stack::push(void* dat){
    Link* newlnk = new Link;
    newlnk->initialize(dat, head);
    head = newlnk;
}

void* Stack::peek(){
    require(head != NULL, "Stack Empty.");
    return head->data;
}

void* Stack::pop(){
    if(head == NULL){
	return 0;
    }
    void* result=head->data;
    Link* oldHead = head;
    head = head->next;
    delete oldHead;
    return result;
}

void Stack::cleanup(){
    require(head == NULL, "Stack not Empty.");
}
