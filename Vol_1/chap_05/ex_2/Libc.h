#ifndef GUARD_LIBC_H
#define GUARD_LIBC_H
#include <string>
using std::string;

class Libc{
    private:
	//string a, b, c;
	string s[3];
    public:
	void seta(string);
	string geta();
	void setb(string);
	string getb();
	void setc(string);
	string getc();
};

#endif
