#include "Libc.h"
#include <iostream>

using namespace std;

int main(){
    Libc x;
    
    x.seta("Hello there! ");
    x.setb("How are you? ");
    x.setc("Its a nice day today. ");

    cout << x.geta() << x.getb() << x.getc() << endl;
    return 0;
}
