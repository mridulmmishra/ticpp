#include "ex_1.h"

int ex_1::read_int(){
    return in;
}

float ex_1::read_float(){
    return fl;
}

int ex_1::write_int(int i){
    in = i;
    return 0;
}

int ex_1::write_float(float f){
    fl = f;
    return 0;
}
