#ifndef GUARD_EX_1_H
#define GUARD_EX_1_H
class ex_1{
    private:
	int in;
    protected:
	float fl;
    public:
	int read_int();
	int write_int(int);
	float read_float();
	int write_float(float);
};
#endif
