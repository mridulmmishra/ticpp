#include "ex_1.h"
#include <iostream>

using namespace std;

int main(){
    ex_1 var;
    cout << "Int : " << var.read_int() << endl;
    cout << "Float : " << var.read_float() << endl;
    var.write_int(5);
    var.write_float(5.1);
    cout << "Int : " << var.read_int() << endl;
    cout << "Float: " << var.read_float() << endl;
    var.in = 5;
    var.fl = 5.1;
    return 0;
}
