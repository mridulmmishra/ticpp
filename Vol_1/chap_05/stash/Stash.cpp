#include "Stash.h"
#include <iostream>
#include <cassert>

using std::cout;		using std::endl;

const int increment = 20;

void Stash::initialize(int sz){
	size = sz;
	quantity = 0;
	next = 0;
	storage = 0;
}

void Stash::cleanup(){
	if(quantity > 0){
		cout << "Cleaning up storage." << endl;
		delete [](storage);
	}
}

int Stash::add(const void* element){
	if(quantity <= next){
		inflate(increment);
	}
	unsigned char* c = (unsigned char*) element;
	for(int i =0; i < size; i++){
		storage[(next)*(size) + i] = c[i];
	}
	(next)++;
	return (next - 1);
}

void* Stash::fetch(int index){
	assert(0 <= index);
	if(index >= next){
		return 0;
	}
	return &(storage[index*size]);
}

int Stash::count(){
	return next;
}

void Stash::inflate(int increase){
	assert(increase > 0);
	int oldSize = (quantity)*(size);
	unsigned char* c = new unsigned char[(quantity + increase)*size];
	for(int i =0; i < oldSize; i++){
		c[i] = storage[i];
	}
	delete []storage;
	storage = c;
	quantity += increase;
}
