#include "ex_6.h"
#include "CppLib.h"
#include <iostream>

using namespace std;

int main(){
    Stash h_s;
    Hen h1, h2, *h_p;
    h_s.initialize(sizeof(Hen));
    h_s.add(&h1);
    h_s.add(&h2);
    h_p = (Hen*) h_s.fetch(1);
    h_p->display();
    h_p = (Hen*) h_s.fetch(2);
    h_p->display();
    return 0;
}
