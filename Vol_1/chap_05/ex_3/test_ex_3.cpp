#include "ex_3.h"
#include <iostream>

using namespace std;

int get_int(ex_3);

int main(){
    ex_3 x;
    x.set_mem(5);
    cout << "Int : " << x.get_mem() << endl;
    cout << "From friend : " << get_int(x) << endl;
}

int get_int(ex_3 y){
    return y.i;
}
