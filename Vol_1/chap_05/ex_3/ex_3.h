#ifndef GUARD_EX_3_H
#define GUARD_EX_3_H
class ex_3{
    private:
	int i;
    public:
	int set_mem(int);
	int get_mem();
	friend int get_int(ex_3);
};
#endif /* EX_3_H */
