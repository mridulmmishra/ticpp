#include "ex_7.h"
#include <iostream>

using namespace std;

void Hen::display(){
    cout << "This is Hen." << endl;
}

void Hen::Nest::display(){
    cout << "This is Hen's Nest." << endl;
}

void Hen::Nest::Egg::display(){
    cout << "This is Hen's egg in it's Nest." << endl;
}

int Hen::get_Nest(Nest* n){
    return n->n_num;
}

int Hen::get_Egg(Nest::Egg* e){
    return e->e_num;
}

int Hen::Nest::get_Egg(Egg* e){
    return e->e_num;
}

void Hen::Nest::set_Nest(int val){
    n_num = val;
}

void Hen::Nest::Egg::set_Egg(int val){
    e_num = val;
}
