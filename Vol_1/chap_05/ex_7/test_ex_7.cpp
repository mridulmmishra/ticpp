#include "ex_7.h"
#include <iostream>

using namespace std;

int main(){
    Hen h;
    Hen::Nest n;
    Hen::Nest::Egg e;
    h.display();
    n.display();
    e.display();
    n.set_Nest(5);
    e.set_Egg(6);
    cout << "Nest from Hen : " << h.get_Nest(&n) << endl;
    cout << "Egg from Hen : " << h.get_Egg(&e) << endl;
    cout << "Egg from Nest : " << n.get_Egg(&e) << endl;
    return 0;
}
