#ifndef GUARD_EX_7_H
#define GUARD_EX_7_H 
class Hen{
    public:
	class Nest{
	    private:
		int n_num;
	    public:
		class Egg{
		    private:
			int e_num;
		    public:
			void display();
			void set_Egg(int);
			friend class Nest;
			friend class Hen;
		};
		void display();
		void set_Nest(int);
		friend class Hen;
		int get_Egg(Egg *);
	};
	void display();
	int get_Nest(Nest *);
	int get_Egg(Nest::Egg *);
};
#endif /* EX_7_H */
