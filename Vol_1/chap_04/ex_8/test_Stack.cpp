#include <iostream>
#include "Stack.h"

using namespace std;

int main(){
  Stack dStack;
  const int stackSize = 100;

  dStack.initialize();

  for(int i = 0; i < stackSize; i++){
    double d = i/3.0;
    dStack.push(new double(d));
  }

  cout << "Doubles in reverse order are : " << endl;
  double* k;
  while(k = (double *)dStack.pop()){
    cout << *k << '\t';
  }
  cout << endl;

  return 0;
}
