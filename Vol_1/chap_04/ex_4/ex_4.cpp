#include "ex_4.h"
#include <iostream>

using namespace std;

void f1(ex_4* st_p, int d){
  if(st_p !=0){
    st_p->dat = d;
  }
}

void f2(ex_4* st_p){
  if(st_p ==0){
    cout << "Undefined structure" << endl;
  }
    cout << "Integer value in struct is : " << st_p->dat << endl;
}
