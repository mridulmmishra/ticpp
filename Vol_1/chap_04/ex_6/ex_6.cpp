#include "ex_6.h"
#include <iostream>

using namespace std;

void ex_6::f1(int d){
    this->dat = d;
}

void ex_6::f2(){
    cout << "Integer value in struct is : " << this->dat << endl;
}
