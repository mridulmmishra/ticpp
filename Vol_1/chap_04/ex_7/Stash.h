#ifndef GAURD_STASH_H
#define GAURD_STASH_H

struct Stash{
	int quantity;
	int next;
	double* storage;

	void initialize();
	void cleanup();
	int add(const double* element);
	double* fetch(int index);
	int count();
	void inflate(int increase);
};
#endif
