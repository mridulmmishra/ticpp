#include "Stash.h"
#include <iostream>
#include <cassert>

using std::cout;		using std::endl;

const int increment = 20;

void Stash::initialize(){
	quantity = 0;
	next = 0;
	storage = 0;
}

void Stash::cleanup(){
	if(quantity > 0){
		cout << "Cleaning up storage." << endl;
		delete [](storage);
	}
}

int Stash::add(const double* element){
	if(quantity <= next){
		inflate(increment);
	}
	storage[next] = *element;
	(next)++;
	return (next - 1);
}

double* Stash::fetch(int index){
	assert(0 <= index);
	if(index >= next){
		return 0;
	}
	return &(storage[index]);
}

int Stash::count(){
	return next;
}

void Stash::inflate(int increase){
	assert(increase > 0);
	double* c = new double[(quantity + increase)*sizeof(double)];
	for(int i =0; i < quantity; i++){
		c[i] = storage[i];
	}
	delete []storage;
	storage = c;
	quantity += increase;
}
