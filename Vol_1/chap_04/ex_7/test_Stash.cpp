#include <iostream>
#include "Stash.h"

using namespace std;

int main(){
  Stash dStash;
  const int stashSize = 100;

  dStash.initialize();

  for(int i = 0; i < stashSize; i++){
    double d = i/3.0;
    dStash.add(&d);
  }

  cout << "Doubles in reverse order are : " << endl;
  for(int j = dStash.count() - 1; j >=0 ; j--){
    double k = *(dStash.fetch(j));
    cout << k << '\t';
  }
  cout << endl;

  return 0;
}
