#include <iostream>

using namespace std;

struct ex_2{
  void print_something();
};

int main(){
  ex_2 ex2_obj;
  ex2_obj.print_something();
  return 0;
}

void ex_2::print_something(){
  cout << "Hey there!" << endl;
}
