#include "CLib.h"
#include <iostream>
#include <cassert>

using std::cout;		using std::endl;

const int increment = 20;

void initialize(CStash* s, int size){
	s->size = size;
	s->quantity = 0;
	s->next = 0;
	s->storage = 0;
}

void cleanup(CStash* s){
	if(s->quantity > 0){
		cout << "Cleaning up storage." << endl;
		delete [](s->storage);
	}
}

int add(CStash* s, const void* element){
	if(s->quantity <= s->next){
		inflate(s, increment);
	}
	unsigned char* c = (unsigned char*) element;
	for(int i =0; i < s->size; i++){
		s->storage[(s->next)*(s->size) + i] = c[i];
	}
	(s->next)++;
	return (s->next - 1);
}

void* fetch(CStash* s, int index){
	assert(0 <= index);
	if(index >= s->next){
		return 0;
	}
	return &(s->storage[index*s->size]);
}

int count(CStash* s){
	return s->next;
}

void inflate(CStash* s, int increase){
	assert(increase > 0);
	int oldSize = (s->quantity)*(s->size);
	unsigned char* c = new unsigned char[(s->quantity + increase)*s->size];
	for(int i =0; i < oldSize; i++){
		c[i] = s->storage[i];
	}
	delete []s->storage;
	s->storage = c;
	s->quantity += increase;
}
