#include <iostream>
#include "CLib.h"

using namespace std;

int main(){
	CStash iStash;
	CStash* intStash = &(iStash);
	const int stashSize = 100;

	initialize(intStash, sizeof(int));

	for(int i = 0; i < stashSize; i++){
		add(intStash, &i);
	}

	cout << "Integers in reverse order are : " << endl;
	for(int j = count(intStash) - 1; j >=0 ; j--){
		int k = *(int*) fetch(intStash, j);
		cout << k << '\t';
	}
	cout << endl;
	return 0;
}
