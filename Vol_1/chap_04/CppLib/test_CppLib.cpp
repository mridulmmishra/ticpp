#include <iostream>
#include <fstream>
#include "../require.h"
#include "CppLib.h"

using namespace std;

int main(){
  Stash iStash;
  const int stashSize = 100;

  iStash.initialize(sizeof(int));

  for(int i = 0; i < stashSize; i++){
    iStash.add(&i);
  }

  cout << "Integers in reverse order are : " << endl;
  for(int j = iStash.count() - 1; j >=0 ; j--){
    int k = *(int*) iStash.fetch(j);
    cout << k << '\t';
  }
  cout << endl;

  Stash strStash;
  const int bufSize=80;

  strStash.initialize(bufSize*sizeof(char));

  ifstream in("test_CppLib.cpp");
  assure(in);

  string line;
  while(getline(in, line)){
    strStash.add(line.c_str());
  }

  cout << endl << "File lines in reverse order are: " << endl ;
  for(int j = strStash.count() - 1; j >=0; j--){
    char* k = (char*) strStash.fetch(j);
    cout << k << endl;
  }

  return 0;
}
